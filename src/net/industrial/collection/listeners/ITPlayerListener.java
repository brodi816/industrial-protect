package net.industrial.collection.listeners;

import net.industrial.collection.ITPlugin;
import net.industrial.collection.mp3.ITPlayedSaySound;
import net.industrial.collection.mp3.ITSaySound;
import net.industrial.collection.player.ITPlayer;

import net.industrial.collection.player.ITPlayerData;
import net.industrial.collection.world.ITChunk;
import org.bukkit.ChatColor;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.*;
import org.bukkit.inventory.ItemStack;

public class ITPlayerListener implements Listener {
	public final ITPlugin plugin;
    public final boolean saySoundsEnabled;
    public final int saySoundsTicksInBetween;

    private Integer tickLastPlayed = 0;

	public ITPlayerListener(ITPlugin plugin) {
		this.plugin = plugin;
        this.saySoundsTicksInBetween= plugin.config.saySoundsTicksInBetween;
        this.saySoundsEnabled = plugin.config.saySoundsEnabled;
	}

    public void setTickLastPlayed(int tick) {
        synchronized (tickLastPlayed) {
            tickLastPlayed = tick;
        }
    }

    public int getTickLastPlayed() {
        synchronized (tickLastPlayed) {
            return tickLastPlayed;
        }
    }

	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        ITPlayerData playerData = plugin.getOrCreatePlayerData(player.getName());
        ITPlayer itPlayer = new ITPlayer(plugin, playerData, player);

        ITPlayer.setPlayer(itPlayer);
        ITPlayer.getPlayer(plugin, player);
        player.getPlayer().sendMessage(ChatColor.GREEN + "Successfully joined, test 1-2-3-4-5");
	}

	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent event) {
		ITPlayer player = ITPlayer.getPlayer(plugin, event.getPlayer());

		player.onQuit(event);
	}

    @EventHandler
    public void onPlayerRespawn(PlayerRespawnEvent event) {
        ITPlayer player = ITPlayer.getPlayer(plugin, event.getPlayer());

        player.cacheLatestPlayer();
    }

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent event) {
        ITPlayer player = ITPlayer.getPlayer(plugin, event.getPlayer());
        player.onMove(event);
    }

    @EventHandler
    public void onPlayerCommand(PlayerCommandPreprocessEvent event) {
        ITPlayer player = ITPlayer.getPlayer(plugin, event.getPlayer());

        if(plugin.commands.aSyncCommandCheck(player, event.getMessage())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onPlayerAsyncChat(AsyncPlayerChatEvent event) {
        String newFormat = plugin.config.chatFormat.replace('$','\u00A7').replace("%player%", "%1$s").replace("%message%", "%2$s");
        event.setFormat(newFormat);

        //say sounds
        if(plugin.getServerTicks() > (getTickLastPlayed() + saySoundsTicksInBetween) || event.getPlayer().isOp()) {
            //see if a say sound was played
            ITSaySound sound = plugin.saySounds.getSound(event.getMessage());
            if(sound != null) {
                plugin.addSaySoundToPlay(new ITPlayedSaySound(event.getPlayer(), sound));
                setTickLastPlayed(plugin.getServerTicks());
            }
        }
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        ITPlayer player = ITPlayer.getPlayer(plugin, event.getPlayer());
        Block block = event.getClickedBlock();
        ITChunk chunk = null;
        ItemStack itemInHand = player.getPlayer().getItemInHand();

        if(block != null) {
            chunk = ITChunk.getChunk(block.getLocation().getChunk());
        } else {
            chunk = ITChunk.getChunk(event.getPlayer().getLocation().getChunk());
        }


        if(chunk == null || !chunk.isLoaded()) {
            player.getPlayer().sendMessage("Chunk data still loading");
            event.setCancelled(true);
        }

        //make sure the item in hand isn't disabled
        if(!event.isCancelled() && itemInHand != null && chunk.world.itemDisabled((short)itemInHand.getTypeId(), itemInHand.getDurability())) {
            player.getPlayer().sendMessage(ChatColor.DARK_RED + "Item disabled in this world, try another world");
            event.setCancelled(true);
        }

        //get the clicked block clicked and make sure they aren't interacting, aka right clicking with them
        if(!event.isCancelled() &&
                block!= null &&
                event.getAction() != Action.LEFT_CLICK_AIR &&
                event.getAction() != Action.LEFT_CLICK_BLOCK) {

            if(!chunk.canInteract(player.data, block)) {
                player.getPlayer().sendMessage("Protected");
                event.setCancelled(true);
            }
        }

        //action
        if(!event.isCancelled() && player.timedAction != null) {
            player.timedAction.onPlayerInteraction(event);
        }
    }
}
