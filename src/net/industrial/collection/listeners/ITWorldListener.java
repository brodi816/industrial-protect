package net.industrial.collection.listeners;

import net.industrial.collection.ITPlugin;
import net.industrial.collection.sql.ITMySQLQueue;
import net.industrial.collection.sql.ITMySQLQueueItem;
import net.industrial.collection.world.ITChunk;
import net.industrial.collection.world.ITWorld;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.world.*;

/**
 * Created with IntelliJ IDEA.
 * User: Brodi
 * Date: 6/9/13
 * Time: 12:40 AM
 * To change this template use File | Settings | File Templates.
 */
public class ITWorldListener implements Listener {
    public final ITPlugin plugin;

    public ITWorldListener(ITPlugin plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onWorldInit(WorldInitEvent event) {
        ITWorld world = ITWorld.loadWorld(plugin, event.getWorld());
        plugin.logInfo("Loaded world: " + event.getWorld().getName());
    }

    @EventHandler
    public void onWorldLoad(WorldLoadEvent event) {
        ITWorld world = ITWorld.loadWorld(plugin, event.getWorld());
        plugin.logInfo("Loaded world: " + event.getWorld().getName());
    }

    @EventHandler
    public void onWorldUnload(WorldUnloadEvent event) {
        ITWorld world = ITWorld.getWorld(plugin, event.getWorld());
        plugin.addQueueItem(new ITMySQLQueueItem(world, ITMySQLQueue.QueueOperation.UPDATE));

        ITWorld.removeWorld(world);
    }

    @EventHandler
    public void onChunkLoad(ChunkLoadEvent event) {
        ITWorld world = ITWorld.getWorld(plugin, event.getWorld());
    }

    @EventHandler
    public void onChunkUnload(ChunkUnloadEvent event) {
        ITChunk chunk = ITChunk.getChunk(event.getChunk());

        if(chunk != null) {
            plugin.addQueueItem(new ITMySQLQueueItem(chunk, ITMySQLQueue.QueueOperation.UPDATE));
            ITChunk.removeChunk(chunk);
            chunk.unload();
        }
    }
}
