package net.industrial.collection.listeners;

import net.industrial.collection.ITPlugin;
import net.industrial.collection.player.ITPlayer;
import net.industrial.collection.protection.ITProtectedBlock;
import net.industrial.collection.world.ITChunk;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.*;
import org.bukkit.inventory.ItemStack;

/**
 * Created with IntelliJ IDEA.
 * User: Brodi
 * Date: 6/9/13
 * Time: 9:26 AM
 * To change this template use File | Settings | File Templates.
 */
public class ITBlockListener implements Listener {
    public final ITPlugin plugin;

    public ITBlockListener(ITPlugin plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent event) {
        ITPlayer player = ITPlayer.getPlayer(plugin, event.getPlayer());
        ITChunk chunk = ITChunk.getChunk(event.getBlock().getLocation().getChunk());
        Block block = event.getBlock();



        if(chunk == null || !chunk.isLoaded()) {
            player.getPlayer().sendMessage("Chunk data still loading");
            event.setCancelled(true);
        }



        //make sure there wasn't a protected block in the air
        if(!event.isCancelled()) {
            ITProtectedBlock itBlock = chunk.getProtectedBlock(block.getX(), (short)block.getY(), block.getZ());
            if(itBlock != null) {
                chunk.removeProtectedBlock(itBlock);
                player.getPlayer().sendMessage("Removed protected block");
            }
        }

        //see if it's a disabled block in the world
        if(!event.isCancelled() && chunk.world.itemDisabled((short)block.getTypeId(), block.getData())) {
            player.getPlayer().sendMessage(ChatColor.DARK_RED + "Item disabled in this world, try another world");
            event.setCancelled(true);
        }

        //see if it's a limited range block
        if(!event.isCancelled()) {
            boolean limited = true;
            if(block.getX() < plugin.config.limitedBlockRange && block.getX() > -plugin.config.limitedBlockRange &&
                block.getZ() < plugin.config.limitedBlockRange && block.getZ() > -plugin.config.limitedBlockRange) {
                limited = false;
            }
            if(limited) {
                for(ItemStack lb : plugin.config.limitedBlocks) {
                    if(lb.getTypeId() == block.getTypeId() && lb.getDurability() == block.getData()) {
                        player.getPlayer().sendMessage(ChatColor.RED + "This block is limited to within a range of " + plugin.config.limitedBlockRange + " blocks square shaped, not circle");
                        event.setCancelled(true);
                    }
                }
            }
        }

        //see if it's in a protected area
        if(!event.isCancelled() && !chunk.canEdit(player.data, event.getBlock())) {
            player.getPlayer().sendMessage("Protected");
            event.setCancelled(true);
        }


        //see if it's a default protected block
        if(!event.isCancelled()) {
            if(plugin.config.protectedBlocks.contains(event.getBlock().getTypeId())) {
                chunk.addProtectedBlock(new ITProtectedBlock(chunk, block.getX(), (short)block.getY(), block.getZ(), player.data));
                player.getPlayer().sendMessage("Protected block added");
            }
        }

        //action
        if(!event.isCancelled() && player.timedAction != null) {
            player.timedAction.onBlockPlace(event);
        }
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent event) {
        ITPlayer player = ITPlayer.getPlayer(plugin, event.getPlayer());
        ITChunk chunk = ITChunk.getChunk(event.getBlock().getLocation().getChunk());
        Block block = event.getBlock();

        if(chunk == null || !chunk.isLoaded()) {
            player.getPlayer().sendMessage("Chunk data still loading");
            event.setCancelled(true);
        }
        else if(!chunk.canEdit(player.data, event.getBlock())) {
            player.getPlayer().sendMessage("Protected");
            event.setCancelled(true);
        }

        if(!event.isCancelled()) {
            ITProtectedBlock pBlock = chunk.getProtectedBlock(block.getX(), (short) block.getY(), block.getZ());
            if(pBlock != null) {
                chunk.removeProtectedBlock(pBlock);
                player.getPlayer().sendMessage("Removed protected block");
            }
        }

        //action
        if(!event.isCancelled() && player.timedAction != null) {
            player.timedAction.onBlockBreak(event);
        }
    }

    @EventHandler
    public void onPistonRetractEvent(BlockPistonRetractEvent event) {
        Location loc = event.getBlock().getRelative(event.getDirection()).getLocation();
        ITChunk chunk = ITChunk.getChunk(loc.getChunk());

        if(chunk == null) {
            event.setCancelled(true);
            return;
        }

        //make sure the piston isn't trying to retract a protected block
        if(chunk.getProtectedBlock(loc.getBlockX(), (short)loc.getBlockY(), loc.getBlockZ()) != null) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onPistonExtendEvent(BlockPistonExtendEvent event) {
        Location loc = event.getBlock().getRelative(event.getDirection()).getLocation();
        ITChunk chunk = ITChunk.getChunk(loc.getChunk());

        if(chunk == null) {
            event.setCancelled(true);
            return;
        }

        //make sure the piston isn't trying to extend a protected block
        if(chunk.getProtectedBlock(loc.getBlockX(), (short)loc.getBlockY(), loc.getBlockZ()) != null) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onBlockBurn(BlockBurnEvent event) {
        Block block = event.getBlock();
        Location loc = block.getLocation();
        ITChunk chunk = ITChunk.getChunk(loc.getChunk());

        //make sure the chunk data is loaded
        if(chunk == null || !chunk.isLoaded()) {
            event.setCancelled(true);
        }

        //cancel fire if block is protected
        if(!event.isCancelled()) {
            ITProtectedBlock pBlock = chunk.getProtectedBlock(block.getX(), (short) block.getY(), block.getZ());
            if(pBlock != null) {
                event.setCancelled(true);
            }
        }
    }
}
