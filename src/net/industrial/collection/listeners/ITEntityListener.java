package net.industrial.collection.listeners;

import net.industrial.collection.ITPlugin;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityExplodeEvent;

/**
 * Created with IntelliJ IDEA.
 * User: Brodi
 * Date: 6/9/13
 * Time: 9:29 AM
 * To change this template use File | Settings | File Templates.
 */
public class ITEntityListener implements Listener {
    public final ITPlugin plugin;

    public ITEntityListener(ITPlugin plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onEntityExplode(EntityExplodeEvent event) {
        event.setCancelled(true);
    }
}
