package net.industrial.collection.commands;

import net.industrial.collection.player.ITPlayer;
import net.industrial.collection.world.ITWorld;
import org.bukkit.ChatColor;
import org.bukkit.inventory.ItemStack;

/**
 * Created with IntelliJ IDEA.
 * User: brodi
 * Date: 6/17/13
 * Time: 4:57 PM
 * To change this template use File | Settings | File Templates.
 */
public class ITCommandAddDisabledItem extends ITCommand {
    public ITCommandAddDisabledItem(ITCommands commands, String name, short rankNeeded) {
        super(commands, name, rankNeeded);
        arguments.add(new Argument("help", "Disables the item in your in the world you're in"));
    }

    @Override
    public void onPlayerUse(ITPlayer player, String[] args) {
        ItemStack itemInHand = player.getPlayer().getItemInHand();

        if(itemInHand == null || itemInHand.getTypeId() == 0) {
            player.getPlayer().sendMessage(ChatColor.DARK_RED + "No item in your hand");
            return;
        }

        ITWorld world = ITWorld.getWorld(plugin, player.getPlayer().getWorld());

        //make sure it's not already disabled
        if(world.itemDisabled((short)itemInHand.getTypeId(),itemInHand.getDurability())) {
            player.getPlayer().sendMessage(ChatColor.DARK_RED + "Item already disabled");
            return;
        }

        world.addDisabledItem((short)itemInHand.getTypeId(),itemInHand.getDurability());
        player.getPlayer().sendMessage(ChatColor.GREEN + String.format("Disabled block %d:%d in world %s",
                (short)itemInHand.getTypeId(), itemInHand.getDurability(), world.world.getName()));
    }
}
