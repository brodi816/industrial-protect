package net.industrial.collection.commands.relationships;

import net.industrial.collection.ITPageBuilder;
import net.industrial.collection.commands.ITCommand;
import net.industrial.collection.commands.ITCommands;
import net.industrial.collection.player.ITPlayer;
import net.industrial.collection.relationships.ITPlayerFriend;
import org.bukkit.ChatColor;

/**
 * Created with IntelliJ IDEA.
 * User: brodi
 * Date: 6/17/13
 * Time: 7:09 AM
 * To change this template use File | Settings | File Templates.
 */
public class ITCommandFriendList extends ITCommand {
    public ITCommandFriendList(ITCommands commands, String name, short rankNeeded) {
        super(commands, name, rankNeeded);

        arguments.add(new Argument("help", "Lists all friends on your friends list in page format"));
        arguments.add(new Argument("[page", "The page to view"));
    }

    @Override
    public void onPlayerUse(ITPlayer player, String[] args) {
        Object[] friends = player.data.getFriends();

        int page = 1;
        if(args.length > 1) {
            try {
                page = Integer.parseInt(args[1]);
            } catch (Exception ex) {}
        }


        if(friends.length > 0) {
            ITPageBuilder pageBuilder = new ITPageBuilder();

            for(Object friendObject : friends) {
                ITPlayerFriend friend = (ITPlayerFriend)friendObject;
                pageBuilder.lines.add(ChatColor.GRAY + friend.friendPlayer.name + ChatColor.WHITE + " with rank: " + ChatColor.GRAY + friend.friendPlayer.getRank());
            }

            pageBuilder.printPage(player.getPlayer(), page, 8);

        } else {
            player.getPlayer().sendMessage(ChatColor.DARK_RED + "You have no friends");
            printArguments(player);
        }
    }
}
