package net.industrial.collection.commands.relationships;

import net.industrial.collection.commands.ITCommand;
import net.industrial.collection.commands.ITCommands;
import net.industrial.collection.player.ITPlayer;
import net.industrial.collection.player.ITPlayerData;
import net.industrial.collection.relationships.ITPlayerFriend;
import org.bukkit.ChatColor;

/**
 * Created with IntelliJ IDEA.
 * User: brodi
 * Date: 6/17/13
 * Time: 8:48 AM
 * To change this template use File | Settings | File Templates.
 */
public class ITCommandFriendSetRank extends ITCommand {
    public ITCommandFriendSetRank(ITCommands commands, String name, short rankNeeded) {
        super(commands, name, rankNeeded);

        arguments.add(new Argument("help", "Set the rank of your friendship"));
        arguments.add(new Argument("friend", "The friend to set the friendship rank"));
        arguments.add(new Argument("rank", "The rank to set the friendship at"));
    }

    @Override
    public void onPlayerUse(ITPlayer player, String[] args) {
        if(args.length > 2) {
            ITPlayerData friend = plugin.getPlayerData(args[1]);
            if(friend == null) {
                player.getPlayer().sendMessage(ChatColor.RED + "Player: " + args[1] + " doesn't exist");
                return;
            }

            ITPlayerFriend friendship = player.data.getFriend(friend);
            if(friendship == null) {
                player.getPlayer().sendMessage(ChatColor.RED + "Player: " + friend.name + " isn't your friend");
                return;
            }

            //fetch the rank to set the friendship at
            short rank = 1;

            try {
                rank = Short.valueOf(args[2]);
            } catch (Exception ex) {
                player.getPlayer().sendMessage(ChatColor.DARK_RED +
                        String.format("Invalid number rank(0-32767): %s", args[3]));
                return;
            }

            if(rank < 0) {
                player.getPlayer().sendMessage(ChatColor.DARK_RED +
                        String.format("Invalid number rank(0-32767): %s", args[2]));
            }

            friendship.setRank(rank);

            player.getPlayer().sendMessage(ChatColor.GREEN + "Set friendship with: " + friend.name + " to rank " + rank);

        } else {
            player.getPlayer().sendMessage(ChatColor.DARK_RED + "Not enough arguments");
            printArguments(player);
        }
    }
}
