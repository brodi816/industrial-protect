package net.industrial.collection.commands.relationships;

import net.industrial.collection.commands.ITCommand;
import net.industrial.collection.commands.ITCommands;
import net.industrial.collection.player.ITPlayer;
import org.bukkit.ChatColor;

/**
 * Created with IntelliJ IDEA.
 * User: Brodi
 * Date: 6/10/13
 * Time: 1:06 AM
 * To change this template use File | Settings | File Templates.
 */
public class ITCommandFriend extends ITCommand {
    public ITCommandFriend(ITCommands commands, String name, short rankNeeded) {
        super(commands, name, rankNeeded);

        arguments.add(new Argument("help", "Allows for you to manage your friends"));
        arguments.add(new Argument("{view,add,remove,setrank}", "The friend command to run"));
        arguments.add(new Argument("[player]", "The player to act on"));
        arguments.add(new Argument("[rank]", "The rank to set the friend at"));
    }

    @Override
    public void onPlayerUse(ITPlayer player, String[] args) {
        if(args.length == 1) {
            player.getPlayer().sendMessage(ChatColor.RED + "Must specify subcommand view,add,remove,setrank");
            return;
        }


        if(args[1].equals("view")) {
        }else if(args[1].equals("setrank")) {
        } else {
            player.getPlayer().sendMessage(ChatColor.RED + "Valid subcommands are view,add,remove,setrank");
        }
    }
}
