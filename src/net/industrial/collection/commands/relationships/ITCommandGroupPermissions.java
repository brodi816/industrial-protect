package net.industrial.collection.commands.relationships;

import net.industrial.collection.commands.ITCommand;
import net.industrial.collection.commands.ITCommands;

/**
 * Created with IntelliJ IDEA.
 * User: Brodi
 * Date: 6/29/13
 * Time: 9:03 PM
 * To change this template use File | Settings | File Templates.
 */
public class ITCommandGroupPermissions extends ITCommand {
    public ITCommandGroupPermissions(ITCommands commands, String name, short rankNeeded) {
        super(commands, name, rankNeeded);
        arguments.add(new Argument("help", "Manages the permissions of the specified group"));
    }
}
