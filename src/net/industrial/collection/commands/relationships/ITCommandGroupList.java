package net.industrial.collection.commands.relationships;

import net.industrial.collection.commands.ITCommand;
import net.industrial.collection.commands.ITCommands;
import net.industrial.collection.player.ITPlayer;
import net.industrial.collection.relationships.ITGroupMember;
import org.bukkit.ChatColor;

/**
 * Created with IntelliJ IDEA.
 * User: Brodi
 * Date: 6/29/13
 * Time: 9:10 PM
 * To change this template use File | Settings | File Templates.
 */
public class ITCommandGroupList extends ITCommand {
    public ITCommandGroupList(ITCommands commands, String name, short rankNeeded) {
        super(commands, name, rankNeeded);
        arguments.add(new Argument("help", "Lists all the groups your in"));
    }

    @Override
    public void onPlayerUse(ITPlayer player, String[] args) {
        StringBuilder groupsMessage = new StringBuilder();

        groupsMessage.append(ChatColor.GREEN);
        groupsMessage.append("Groups: ");
        groupsMessage.append(ChatColor.GRAY);

        for(ITGroupMember membership : player.data.getGroupMemberships()) {
            groupsMessage.append(", ");
            groupsMessage.append(membership.group.name);
        }

        player.getPlayer().sendMessage(groupsMessage.toString().replaceFirst(" , ", " "));
    }
}
