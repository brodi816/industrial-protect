package net.industrial.collection.commands.relationships;

import net.industrial.collection.commands.ITCommand;
import net.industrial.collection.commands.ITCommands;
import net.industrial.collection.player.ITPlayer;
import net.industrial.collection.relationships.ITGroup;
import net.industrial.collection.relationships.ITGroupMember;
import org.bukkit.ChatColor;

/**
 * Created with IntelliJ IDEA.
 * User: Brodi
 * Date: 6/29/13
 * Time: 9:07 PM
 * To change this template use File | Settings | File Templates.
 */
public class ITCommandGroupPermissionsSet extends ITCommand {
    public ITCommandGroupPermissionsSet(ITCommands commands, String name, short rankNeeded) {
        super(commands, name, rankNeeded);
        arguments.add(new Argument("help", "Sets the specified permission rank of the specified group to the specified rank"));
        arguments.add(new Argument("group", "The group to set the permission of"));
        arguments.add(new Argument("permission", "The permission of the group to set"));
        arguments.add(new Argument("rank", "The rank needed for that permission"));
    }

    @Override
    public void onPlayerUse(ITPlayer player, String[] args) {
        if(args.length > 3) {
            ITGroup group = plugin.getGroup(args[1]);
            String permission = args[2];
            short rankNeeded = -1;

            if(group == null) {
                player.getPlayer().sendMessage(ChatColor.DARK_RED + "Group: " + args[1] + " doesn't exist");
                return;
            }

            if(!group.permissionExists(permission)) {
                player.getPlayer().sendMessage(ChatColor.DARK_RED + "Group permission: " + args[2] + " doesn't exist");
                return;
            }

            ITGroupMember membership = group.getMember(player.data);

            if(membership == null) {
                player.getPlayer().sendMessage(ChatColor.DARK_RED + "You're not in group: " + group.name);
                return;
            }

            if(group.rankNeeded("permissions.set") > membership.getRank()) {
                player.getPlayer().sendMessage(ChatColor.DARK_RED +
                        "Your group rank is not high enough to edit permissions for this group");
                return;
            }

            try {
                rankNeeded = Short.parseShort(args[3]);
            } catch (Exception ex) {
                player.getPlayer().sendMessage(ChatColor.DARK_RED + "Invalid number rank(0-32767): " + args[3]);
                return;
            }

            if(rankNeeded < 0) {
                player.getPlayer().sendMessage(ChatColor.DARK_RED + "Invalid number rank(0-32767): " + args[3]);
                return;
            }

            group.setRankNeeded(permission, rankNeeded);
            player.getPlayer().sendMessage(
                    String.format("%sSet group permission %s rank to %d", ChatColor.GRAY, permission, rankNeeded));
        } else {
            player.getPlayer().sendMessage(ChatColor.DARK_RED + "Not enough arguments");
            printArguments(player);
        }
    }
}
