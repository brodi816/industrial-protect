package net.industrial.collection.commands.relationships;

import net.industrial.collection.commands.ITCommand;
import net.industrial.collection.commands.ITCommands;
import net.industrial.collection.player.ITPlayer;
import net.industrial.collection.relationships.ITGroup;
import org.bukkit.ChatColor;

/**
 * Created with IntelliJ IDEA.
 * User: Brodi
 * Date: 6/29/13
 * Time: 9:05 PM
 * To change this template use File | Settings | File Templates.
 */
public class ITCommandGroupPermissionsList extends ITCommand {
    public ITCommandGroupPermissionsList(ITCommands commands, String name, short rankNeeded) {
        super(commands, name, rankNeeded);
        arguments.add(new Argument("help", "Lists all the permissions and ranks associated with them of the specified group"));
        arguments.add(new Argument("group", "The group to list the permissions of"));
    }

    @Override
    public void onPlayerUse(ITPlayer player, String[] args) {
        if(args.length > 1) {
            ITGroup group = plugin.getGroup(args[1]);
            if(group == null) {
                player.getPlayer().sendMessage(ChatColor.DARK_RED + "Group: " + args[1] + " doesn't exist");
                return;
            }

            StringBuilder permissionsList = new StringBuilder();

            permissionsList.append("Group Permissions List");
            for(ITGroup.ITGroupPermission permission : group.getPermissions()) {
                permissionsList.append("\n");

                permissionsList.append(ChatColor.GRAY);
                permissionsList.append("Name: ");
                permissionsList.append(ChatColor.WHITE);
                permissionsList.append(permission.name);

                permissionsList.append(ChatColor.GRAY);
                permissionsList.append(" Rank: ");
                permissionsList.append(ChatColor.WHITE);
                permissionsList.append(permission.getRankNeeded());
            }

            player.getPlayer().sendMessage(permissionsList.toString());
        } else {
            player.getPlayer().sendMessage(ChatColor.DARK_RED + "Not enough arguments");
            printArguments(player);
        }
    }
}
