package net.industrial.collection.commands.relationships;

import net.industrial.collection.commands.ITCommand;
import net.industrial.collection.commands.ITCommands;
import net.industrial.collection.player.ITPlayer;
import net.industrial.collection.player.ITPlayerData;
import net.industrial.collection.relationships.ITGroup;
import net.industrial.collection.relationships.ITGroupMember;
import org.bukkit.ChatColor;

/**
 * Created with IntelliJ IDEA.
 * User: Brodi
 * Date: 7/6/13
 * Time: 2:02 PM
 * To change this template use File | Settings | File Templates.
 */
public class ITCommandGroupRemove extends ITCommand {
    public ITCommandGroupRemove(ITCommands commands, String name, short rankNeeded) {
        super(commands, name, rankNeeded);
        arguments.add(new Argument("help", "Removes the specified player from the specified group"));
        arguments.add(new Argument("group", "The group to remove the player from"));
        arguments.add(new Argument("player", "The player to remove from the group"));
    }

    @Override
    public void onPlayerUse(ITPlayer player, String[] args) {
        if(args.length > 2) {
            ITGroup group = plugin.getGroup(args[1]);

            if(group == null) {
                player.getPlayer().sendMessage(ChatColor.DARK_RED + "Group: " + args[1] + " doesn't exist");
                return;
            }

            ITGroupMember playerMembership = group.getMember(player.data);

            if(playerMembership == null) {
                player.getPlayer().sendMessage(ChatColor.DARK_RED + "You're not in group: " + group.name);
                return;
            }

            if(playerMembership.getRank() < group.rankNeeded("member.remove")) {
                player.getPlayer().sendMessage(ChatColor.DARK_RED +
                        "You're not a high enough rank in group: " + group.name + " to remove members");
                return;
            }

            ITPlayerData member = plugin.getPlayerData(args[2]);

            if(member == null) {
                player.getPlayer().sendMessage(ChatColor.DARK_RED + "Player: " + args[2] + " doesn't exist");
                return;
            }

            ITGroupMember membership = group.getMember(member);

            if(membership == null) {
                player.getPlayer().sendMessage(ChatColor.DARK_RED +
                        "Player: " + member.name + " not in group: " + group.name);
                return;
            }

            group.removeFromGroup(membership);
            player.getPlayer().sendMessage(ChatColor.GRAY + "Removed: " + member.name + " from group: " + group.name);

        } else {
            player.getPlayer().sendMessage(ChatColor.DARK_RED + "Not enough arguments");
            printArguments(player);
        }
    }
}
