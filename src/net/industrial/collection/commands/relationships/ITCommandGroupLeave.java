package net.industrial.collection.commands.relationships;

import net.industrial.collection.commands.ITCommand;
import net.industrial.collection.commands.ITCommands;
import net.industrial.collection.player.ITPlayer;
import net.industrial.collection.relationships.ITGroup;
import net.industrial.collection.relationships.ITGroupMember;
import org.bukkit.ChatColor;

/**
 * Created with IntelliJ IDEA.
 * User: Brodi
 * Date: 6/29/13
 * Time: 9:01 PM
 * To change this template use File | Settings | File Templates.
 */
public class ITCommandGroupLeave extends ITCommand {
    public ITCommandGroupLeave(ITCommands commands, String name, short rankNeeded) {
        super(commands, name, rankNeeded);
        arguments.add(new Argument("help", "Leaves the group specificed"));
        arguments.add(new Argument("group", "The group to leave"));
    }


    @Override
    public void onPlayerUse(ITPlayer player, String[] args) {
        if(args.length > 1) {
            ITGroup group = plugin.getGroup(args[1]);

            if(group == null) {
                player.getPlayer().sendMessage(ChatColor.DARK_RED + "Group: " + args[1] + " doesn't exist");
                return;
            }

            ITGroupMember membership = player.data.getGroupMembership(group);

            if(player.data.inGroup(group)) {
                group.removeFromGroup(membership);
                player.getPlayer().sendMessage(ChatColor.GREEN + "You have left group: " + group.name);
            } else {
                player.getPlayer().sendMessage(ChatColor.DARK_RED + "You're not in group: " + group.name);
            }

        } else {
            player.getPlayer().sendMessage(ChatColor.DARK_RED + "Not enough arguments");
            printArguments(player);
        }
    }
}
