package net.industrial.collection.commands.relationships;

import net.industrial.collection.commands.ITCommand;
import net.industrial.collection.commands.ITCommands;
import net.industrial.collection.player.ITPlayer;
import net.industrial.collection.player.ITPlayerData;
import net.industrial.collection.relationships.ITGroup;
import net.industrial.collection.relationships.ITGroupMember;
import org.bukkit.ChatColor;

/**
 * Created with IntelliJ IDEA.
 * User: Brodi
 * Date: 6/29/13
 * Time: 9:04 PM
 * To change this template use File | Settings | File Templates.
 */
public class ITCommandGroupAdd extends ITCommand {
    public ITCommandGroupAdd(ITCommands commands, String name, short rankNeeded) {
        super(commands, name, rankNeeded);
        arguments.add(new Argument("help", "Adds the specified player to the specified group"));
        arguments.add(new Argument("group", "The group to add the player to"));
        arguments.add(new Argument("player", "The player to add to the group"));
        arguments.add(new Argument("[rank]", "The group rank to set the player at"));
    }

    @Override
    public void onPlayerUse(ITPlayer player, String[] args) {
        if(args.length > 2) {
            ITGroup group = plugin.getGroup(args[1]);
            ITPlayerData playerData = plugin.getPlayerData(args[2]);
            short rank = 1;

            if(args.length > 3) {
                try {
                    rank = Short.parseShort(args[3]);
                } catch (Exception ex) {
                    player.getPlayer().sendMessage(ChatColor.DARK_RED + "Invalid rank(0-32767): " + args[3]);
                    return;
                }
            }

            if(rank < 0) {
                player.getPlayer().sendMessage(ChatColor.DARK_RED + "Invalid rank(0-32767): " + args[3]);
                return;
            }

            if(group == null) {
                player.getPlayer().sendMessage(ChatColor.DARK_RED + "Group: " + args[1] + " doesn't exist");
                return;
            }

            if(playerData == null) {
                player.getPlayer().sendMessage(ChatColor.DARK_RED + "Player: " + args[2] + " doesn't exist");
                return;
            }

            ITGroupMember membership = group.getMember(player.data);

            if(membership == null) {
                player.getPlayer().sendMessage(ChatColor.DARK_RED + "You're not a member of group: " + group.name);
                return;
            }

            if(membership.getRank() <= rank) {
                player.getPlayer().sendMessage(ChatColor.DARK_RED + "Cannot add member with a higher rank then you");
                return;
            }

            if(group.rankNeeded("member.add") > membership.getRank()) {
                player.getPlayer().sendMessage(ChatColor.DARK_RED + "You're not a high enough rank to add members to group: " + group.name);
                return;
            }

            group.addToGroup(new ITGroupMember(group, playerData, rank));
            player.getPlayer().sendMessage(ChatColor.GREEN + "Added player: " + playerData.name +
                    " to group " + group.name + " with rank " + rank);

        } else {
            player.getPlayer().sendMessage(ChatColor.DARK_RED + "Not enough arguments");
            printArguments(player);
        }
    }
}
