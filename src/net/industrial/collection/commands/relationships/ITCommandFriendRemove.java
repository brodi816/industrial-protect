package net.industrial.collection.commands.relationships;

import net.industrial.collection.commands.ITCommand;
import net.industrial.collection.commands.ITCommands;
import net.industrial.collection.player.ITPlayer;
import net.industrial.collection.player.ITPlayerData;
import net.industrial.collection.relationships.ITPlayerFriend;
import org.bukkit.ChatColor;

/**
 * Created with IntelliJ IDEA.
 * User: brodi
 * Date: 6/12/13
 * Time: 10:18 AM
 * To change this template use File | Settings | File Templates.
 */
public class ITCommandFriendRemove extends ITCommand {
    public ITCommandFriendRemove(ITCommands commands, String name, short rankNeeded) {
        super(commands, name, rankNeeded);

        arguments.add(new Argument("help", "Adds a player to your friends list"));
        arguments.add(new Argument("player", "The player to remove from your friends list"));
    }

    @Override
    public void onPlayerUse(ITPlayer player, String[] args) {
        if(args.length > 1) {
            ITPlayerData friend = plugin.getPlayerData(args[1]);

            //make sure the friend exists
            if(friend == null) {
                player.getPlayer().sendMessage("Player " + args[1] + " not found");
                return;
            }

            ITPlayerFriend friendship = player.data.getFriend(friend);

            //make sure they are friends
            if(friendship == null) {
                player.getPlayer().sendMessage(ChatColor.DARK_RED + "Player " + args[1] + " not on your friends list");
                return;
            }

            //remove the friendship
            player.data.removeFriend(friend);

            player.getPlayer().sendMessage(ChatColor.GREEN + "Removed " + friend.name + " from your friend list " + ChatColor.RED + ":(");
        } else {
            player.getPlayer().sendMessage(ChatColor.DARK_RED + "Must specify a player");
            printArguments(player);
        }
    }
}
