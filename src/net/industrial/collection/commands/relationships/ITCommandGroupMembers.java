package net.industrial.collection.commands.relationships;

import net.industrial.collection.commands.ITCommand;
import net.industrial.collection.commands.ITCommands;
import net.industrial.collection.player.ITPlayer;
import net.industrial.collection.relationships.ITGroup;
import net.industrial.collection.relationships.ITGroupMember;
import org.bukkit.ChatColor;

/**
 * Created with IntelliJ IDEA.
 * User: Brodi
 * Date: 6/29/13
 * Time: 9:10 PM
 * To change this template use File | Settings | File Templates.
 */
public class ITCommandGroupMembers extends ITCommand {
    public ITCommandGroupMembers(ITCommands commands, String name, short rankNeeded) {
        super(commands, name, rankNeeded);
        arguments.add(new Argument("help", "View the players of the specified group"));
        arguments.add(new Argument("group", "The group to view the members of"));
    }

    @Override
    public void onPlayerUse(ITPlayer player, String[] args) {
        if(args.length > 1) {
            ITGroup group = plugin.getGroup(args[1]);

            if(group == null) {
                player.getPlayer().sendMessage(ChatColor.DARK_RED + "Group: " + args[1] + " doesn't exist");
                return;
            }

            StringBuilder membersMessage = new StringBuilder();

            membersMessage.append(ChatColor.GREEN);
            membersMessage.append(group.name);
            membersMessage.append(" members: ");
            membersMessage.append(ChatColor.GRAY);

            for(ITGroupMember membership : group.getMembers()) {
                membersMessage.append(", ");
                membersMessage.append(membership.player.name);
            }

            player.getPlayer().sendMessage(membersMessage.toString().replaceFirst(" , ", " "));

        } else {
            player.getPlayer().sendMessage(ChatColor.DARK_RED + "Not enough arguments");
            printArguments(player);
        }
    }
}
