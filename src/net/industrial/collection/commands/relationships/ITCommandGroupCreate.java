package net.industrial.collection.commands.relationships;

import net.industrial.collection.commands.ITCommand;
import net.industrial.collection.commands.ITCommands;
import net.industrial.collection.player.ITPlayer;
import net.industrial.collection.relationships.ITGroup;
import net.industrial.collection.relationships.ITGroupMember;
import net.industrial.collection.sql.ITMySQLQueue;
import net.industrial.collection.sql.ITMySQLQueueItem;
import net.industrial.collection.world.ITChunk;
import org.bukkit.ChatColor;

/**
 * Created with IntelliJ IDEA.
 * User: Brodi
 * Date: 6/29/13
 * Time: 8:59 PM
 * To change this template use File | Settings | File Templates.
 */
public class ITCommandGroupCreate extends ITCommand {
    public ITCommandGroupCreate(ITCommands commands, String name, short rankNeeded) {
        super(commands, name, rankNeeded);
        arguments.add(new Argument("help", "Creates a group"));
        arguments.add(new Argument("group", "The name to use for the group"));
    }

    @Override
    public void onPlayerUse(ITPlayer player, String[] args) {
        if(args.length > 1) {
            if(plugin.groupExists(args[1])) {
                player.getPlayer().sendMessage(ChatColor.DARK_RED + "Group " + args[1] + " already exists");
                return;
            }

            ITGroup group = new ITGroup(plugin, args[1]);
            plugin.addQueueItem(new ITMySQLQueueItem(group, ITMySQLQueue.QueueOperation.UPDATE));
            plugin.setGroup(args[1], group);
            group.addToGroup(new ITGroupMember(group, player.data, (short) 32767));

            player.getPlayer().sendMessage(ChatColor.GRAY + "Created group: " + group.name);

        } else {
            player.getPlayer().sendMessage(ChatColor.DARK_RED + "Not enough arguments");
            printArguments(player);
        }
    }
}
