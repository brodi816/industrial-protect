package net.industrial.collection.commands.relationships;

import net.industrial.collection.commands.ITCommand;
import net.industrial.collection.commands.ITCommands;
import net.industrial.collection.player.ITPlayer;
import net.industrial.collection.player.ITPlayerData;
import org.bukkit.ChatColor;

/**
 * Created with IntelliJ IDEA.
 * User: brodi
 * Date: 6/12/13
 * Time: 5:31 AM
 * To change this template use File | Settings | File Templates.
 */
public class ITCommandFriendAdd extends ITCommand {
    public ITCommandFriendAdd(ITCommands commands, String name, short rankNeeded) {
        super(commands, name, rankNeeded);
        arguments.add(new Argument("help", "Adds a player to your friends list"));
        arguments.add(new Argument("player", "The player to add to your friends list"));
        arguments.add(new Argument("[rank]", "The rank to set the player at"));

    }

    @Override
    public void onPlayerUse(ITPlayer player, String[] args) {
        if(args.length > 1) {

            ITPlayerData friend = plugin.getPlayerData(args[1]);

            if(friend == null) {
                player.getPlayer().sendMessage(ChatColor.DARK_RED + String.format("Player %s doesn't exist", args[1]));
                return;
            }


            if(player.data.getFriend(friend) != null) {
                player.getPlayer().sendMessage(ChatColor.DARK_RED + String.format("You're already friends with %s", args[1]));
                return;
            }

            //player specified a rank with the player
            if(args.length > 2) {
                short rank = -1;

                //try to get the rank
                try {
                    rank = Short.valueOf(args[2]);
                } catch (Exception ex) {
                    player.getPlayer().sendMessage(ChatColor.DARK_RED +
                            String.format("Invalid number rank(0-32767): %s", args[3]));
                    return;
                }

                //make sure it's a valid rank
                if(rank < 0) {
                    player.getPlayer().sendMessage(ChatColor.DARK_RED +
                            String.format("Invalid number rank(0-32767): %s", args[3]));
                }

                player.data.addFriend(friend, rank);
                player.getPlayer().sendMessage(ChatColor.GREEN  +
                        String.format("Added %s to friends with rank %d", friend.name, rank));

            } else {
                //add the friendship with default rank
                player.data.addFriend(friend);
                player.getPlayer().sendMessage(ChatColor.GREEN  +
                        String.format("Added %s to friends with rank %d", friend.name, player.data.getFriend(friend).getRank()));
            }

        } else {
            player.getPlayer().sendMessage(ChatColor.DARK_RED + "Must specify a player");
            printArguments(player);
        }
    }
}
