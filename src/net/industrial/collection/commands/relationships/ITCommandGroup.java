package net.industrial.collection.commands.relationships;

import net.industrial.collection.commands.ITCommand;
import net.industrial.collection.commands.ITCommands;

/**
 * Created with IntelliJ IDEA.
 * User: Brodi
 * Date: 6/29/13
 * Time: 8:57 PM
 * To change this template use File | Settings | File Templates.
 */
public class ITCommandGroup extends ITCommand {
    public ITCommandGroup(ITCommands commands, String name, short rankNeeded) {
        super(commands, name, rankNeeded);
        arguments.add(new Argument("help", "Base command for group management"));
    }
}
