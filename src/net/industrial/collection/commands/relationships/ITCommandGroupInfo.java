package net.industrial.collection.commands.relationships;

import net.industrial.collection.commands.ITCommand;
import net.industrial.collection.commands.ITCommands;
import net.industrial.collection.player.ITPlayer;
import net.industrial.collection.relationships.ITGroup;
import org.bukkit.ChatColor;

/**
 * Created with IntelliJ IDEA.
 * User: Brodi
 * Date: 6/29/13
 * Time: 9:11 PM
 * To change this template use File | Settings | File Templates.
 */
public class ITCommandGroupInfo extends ITCommand {
    public ITCommandGroupInfo(ITCommands commands, String name, short rankNeeded) {
        super(commands, name, rankNeeded);
        arguments.add(new Argument("help", "View the information about the specified group"));
        arguments.add(new Argument("group", "The group information to view"));
    }

    @Override
    public void onPlayerUse(ITPlayer player, String[] args) {
        if(args.length > 1) {
            ITGroup group = plugin.getGroup(args[1]);
            if(group == null) {
                player.getPlayer().sendMessage(ChatColor.DARK_RED + "Group " + args[1] + " doesn't exist");
                return;
            }

            StringBuilder infoMessage = new StringBuilder();
            infoMessage.append(ChatColor.GRAY);
            infoMessage.append("Group name: ");
            infoMessage.append(ChatColor.WHITE);
            infoMessage.append(group.name);
            infoMessage.append("\n");

            infoMessage.append(ChatColor.GRAY);
            infoMessage.append("Date created: ");
            infoMessage.append(group.getCreationDate().toString());
            infoMessage.append(ChatColor.WHITE);
            infoMessage.append("\n");

            infoMessage.append(ChatColor.GRAY);
            infoMessage.append("Total members: ");
            infoMessage.append(ChatColor.WHITE);
            infoMessage.append(group.getMemberCount());

            player.getPlayer().sendMessage(infoMessage.toString());

        } else {
            player.getPlayer().sendMessage(ChatColor.DARK_RED + "Not enough arguments");
            printArguments(player);
        }
    }
}
