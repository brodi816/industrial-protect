package net.industrial.collection.commands;

import net.industrial.collection.ITPlugin;
import net.industrial.collection.player.ITPlayer;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.help.HelpTopic;

/**
 * Created with IntelliJ IDEA.
 * User: brodi
 * Date: 6/16/13
 * Time: 8:19 PM
 * To change this template use File | Settings | File Templates.
 */
public class ITHelpTopic extends HelpTopic {
    public final ITPlugin plugin;
    public final ITCommand command;

    public ITHelpTopic(ITCommand command, String prefix) {
        this.plugin = command.plugin;
        this.command = command;

        if(prefix.isEmpty()) {
            this.name = "/" + command.name;
        } else {
            this.name = prefix + command.name;
        }

        if(command.arguments.size() > 0) {
            this.shortText = command.arguments.get(0).help;
        } else {
            plugin.logWarning("Command: " + command.name + " has no description/help");
        }

        StringBuffer descSb = new StringBuffer();

        descSb.append(ChatColor.GOLD + "Description: " + ChatColor.WHITE + this.shortText +"\n");

        StringBuffer args = new StringBuffer();
        descSb.append(ChatColor.GOLD + "Usage: " + ChatColor.WHITE + this.name);

        for(int i=1; i < command.arguments.size(); i++) {
            ITCommand.Argument arg = command.arguments.get(i);
            descSb.append(" " + arg.name);

            args.append("\n");
            args.append(" " + ChatColor.GOLD + arg.name + ChatColor.WHITE + " " + arg.help);
        }
        descSb.append(args.toString());

        this.fullText = descSb.toString();
    }


    @Override
    public boolean canSee(CommandSender commandSender) {
        if(commandSender instanceof Player) {
            if(((Player)commandSender).isOp()) {
                return true;
            }

            ITPlayer player = ITPlayer.getPlayer(plugin, (Player)commandSender);
            if(player.data.getRank() >= command.rankNeeded) {
                return true;
            } else {
                return false;
            }
        }
        return  true;
    }
}
