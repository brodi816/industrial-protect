package net.industrial.collection.commands;

import cpw.mods.fml.relauncher.FMLRelauncher;
import cpw.mods.fml.relauncher.RelaunchClassLoader;
import net.industrial.collection.player.ITPlayer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * Created with IntelliJ IDEA.
 * User: brodi
 * Date: 6/10/13
 * Time: 2:52 PM
 * To change this template use File | Settings | File Templates.
 */
public class ITCommandForceReload extends ITCommand {

    public ITCommandForceReload(ITCommands commands, String name, short rankNeeded) {
        super(commands, name, rankNeeded);

        arguments.add(new Argument("help", "Froce reloads all the plugins ignoring anything trying to stop reload, A.K.A. mcpc"));
    }

    public void onPlayerUse(ITPlayer player, String[] args) {
        plugin.getServer().broadcastMessage(ChatColor.GREEN + "Reloading plugins");


        try {
            Class craftingManagerClazz = Class.forName("net.minecraft.item.crafting.CraftingManager");
            Class furnaceManagerClazz = Class.forName("net.minecraft.item.crafting.FurnaceRecipes");

            /*for(Field f : craftingManagerClazz.getDeclaredFields()) {
                plugin.getServer().broadcastMessage("C: " + f.getName() + " t: " + f.getType().getName());
            }

            for(Field f : furnaceManagerClazz.getDeclaredFields()) {
                plugin.getServer().broadcastMessage("F: " + f.getName() + " t: " + f.getType().getName());
            }

            for(Method m : craftingManagerClazz.getDeclaredMethods()) {
                plugin.getServer().broadcastMessage("C: " + m.getName() + " r: " + m.getReturnType().getName());
            }

            for(Method m : furnaceManagerClazz.getDeclaredMethods()) {
                plugin.getServer().broadcastMessage("F: " + m.getName() + " r: " + m.getReturnType().getName());
            }*/

            Method craftingManagerMethod = craftingManagerClazz.getDeclaredMethod("func_77594_a");
            Method furnaceManagerMethod = furnaceManagerClazz.getDeclaredMethod("func_77602_a");



            craftingManagerMethod.setAccessible(true);
            furnaceManagerMethod.setAccessible(true);

            Object craftingManager = craftingManagerMethod.invoke(null);
            Object furnaceManager = furnaceManagerMethod.invoke(null);

            //crafting table fields
            Field recipeListField = craftingManager.getClass().getDeclaredField("field_77597_b"); //recipes
            //furnace fields
            Field furnaceRecipesField = furnaceManager.getClass().getDeclaredField("field_77604_b"); //smelting list
            Field furnaceRecipesExpField = furnaceManager.getClass().getDeclaredField("field_77605_c"); //experience list
            Field metaFurnaceRecipesField = furnaceManager.getClass().getDeclaredField("metaSmeltingList");
            Field metaFurnaceRecipesExpField = furnaceManager.getClass().getDeclaredField("metaExperience");

            //make all fields accessible
            recipeListField.setAccessible(true);
            furnaceRecipesField.setAccessible(true);
            furnaceRecipesExpField.setAccessible(true);
            metaFurnaceRecipesField.setAccessible(true);
            metaFurnaceRecipesExpField.setAccessible(true);


            //
            ///Copy the current references
            //

            //fetch crafting manager
            Object crafting = recipeListField.get(craftingManager);

            //fetch furnace recipes
            Object furnaceRecipes = furnaceRecipesField.get(furnaceManager);
            Object furnaceRecipesExp = furnaceRecipesExpField.get(furnaceManager);
            Object metaFurnaceRecipes = metaFurnaceRecipesField.get(furnaceManager);
            Object metaFurnaceRecipesExp = metaFurnaceRecipesExpField.get(furnaceManager);

            //reload the server
            plugin.getServer().reload();

            //fetch the latest managers
            craftingManager = craftingManagerMethod.invoke(null);
            furnaceManager = furnaceManagerMethod.invoke(null);


            //set the references to old recipes
            recipeListField.set(craftingManager, crafting);
            furnaceRecipesField.set(furnaceManager, furnaceRecipes);
            furnaceRecipesExpField.set(furnaceManager, furnaceRecipesExp);
            metaFurnaceRecipesField.set(furnaceManager, metaFurnaceRecipes);
            metaFurnaceRecipesExpField.set(furnaceManager, metaFurnaceRecipesExp);


            plugin.getServer().broadcastMessage(ChatColor.GREEN + "Reload Complete");

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void onPlayerUse1(ITPlayer player, String[] args) {
        String[] prefixes = new String[]{"", "net.minecraft.", "net.minecraft.src.", "net.minecraft.server.", "net.minecraft.item.crafting."};

        try {
            Method relauncherMethod = FMLRelauncher.class.getDeclaredMethod("instance");
            relauncherMethod.setAccessible(true);
            FMLRelauncher relauncher = (FMLRelauncher)relauncherMethod.invoke(null);
            Field classLoaderField = FMLRelauncher.class.getDeclaredField("classLoader");
            classLoaderField.setAccessible(true);
            RelaunchClassLoader classLoader = (RelaunchClassLoader) classLoaderField.get(relauncher);

            //loop through all the prefixes
            plugin.getServer().broadcastMessage("Beginning test group 1");

            for(int index=0; index<prefixes.length; index++) {
                plugin.getServer().broadcastMessage("Trying test " + index);
                try {
                    Class.forName(prefixes[index]+"CraftingManager");
                    plugin.getServer().broadcastMessage("Good 1");
                } catch (Exception ex) { }
                try {
                    Class craftClazz = Class.forName(prefixes[index] + "yk");
                    plugin.getServer().broadcastMessage("Good 2");
                } catch (Exception ex) { }
                try {
                    Class furnaceClazz = Class.forName(prefixes[index] + "yg");
                    plugin.getServer().broadcastMessage("Good 3");
                } catch (Exception ex) { }
            }

            plugin.getServer().broadcastMessage("Beginning test group 2");
            for(int index=0; index<prefixes.length; index++)  {
                plugin.getServer().broadcastMessage("Trying test " + index);
                try {
                    classLoader.findClass(prefixes[index]+"CraftingManager");
                    plugin.getServer().broadcastMessage("Good 1");
                } catch (Exception ex) { }
                try {
                    Class craftClazz = classLoader.findClass(prefixes[index] + "yk");
                    plugin.getServer().broadcastMessage("Good 2");
                } catch (Exception ex) { }
                try {
                    Class furnaceClazz = classLoader.findClass(prefixes[index] + "yg");
                    plugin.getServer().broadcastMessage("Good 3");
                } catch (Exception ex) { }
            }

            plugin.getServer().broadcastMessage("Beginning test group 3");
            for(int index=0; index<prefixes.length; index++)  {
                plugin.getServer().broadcastMessage("Trying test " + index);
                try {
                    Class craftClazz = Class.forName(prefixes[index] + "CraftingManager", false, classLoader);
                    plugin.getServer().broadcastMessage("Good 1");
                } catch (Exception ex) { }
                try {
                    Class craftClazz = Class.forName(prefixes[index] + "yk", false, classLoader);
                    plugin.getServer().broadcastMessage("Good 2");
                } catch (Exception ex) { }
                try {
                    Class furnaceClazz = Class.forName(prefixes[index] + "yg", false, classLoader);
                    plugin.getServer().broadcastMessage("Good 3");
                } catch (Exception ex) { }
            }
        } catch (Exception ex) {
            plugin.getServer().broadcastMessage("Severe exception");
            ex.printStackTrace();
        }
    }

    public void onPlayerUse2(ITPlayer player, String[] args) {
        try {



            plugin.getServer().broadcastMessage(ChatColor.GREEN + "Reloading plugins, test 1");

            //fetch forges private class loader ;)
            Method relauncherMethod = FMLRelauncher.class.getDeclaredMethod("instance");
            relauncherMethod.setAccessible(true);
            FMLRelauncher relauncher = (FMLRelauncher)relauncherMethod.invoke(null);
            Field classLoaderField = FMLRelauncher.class.getDeclaredField("classLoader");
            classLoaderField.setAccessible(true);
            RelaunchClassLoader classLoader = (RelaunchClassLoader) classLoaderField.get(relauncher);

            //clazzes
            Class craftClazz = Class.forName("net.minecraft.src.yk");
            Class furnaceClazz = Class.forName("net.minecraft.src.yg");



            //crafting table fields
            Field recipeListField = craftClazz.getDeclaredField("b");
            //furnace fields
            Field furnaceRecipesField = furnaceClazz.getDeclaredField("b");
            Field furnaceRecipesExpField = furnaceClazz.getDeclaredField("c");
            Field metaFurnaceRecipesField = furnaceClazz.getDeclaredField("metaSmeltingList");
            Field metaFurnaceRecipesExpField = furnaceClazz.getDeclaredField("metaExperience");

            //make all fields accessible
            recipeListField.setAccessible(true);
            furnaceRecipesField.setAccessible(true);
            furnaceRecipesExpField.setAccessible(true);
            metaFurnaceRecipesField.setAccessible(true);
            metaFurnaceRecipesExpField.setAccessible(true);

            //fetch crafting manager
            Object craftingManager = craftClazz.getDeclaredMethod("a").invoke(null);
            Object crafting = recipeListField.get(craftingManager);

            //fetch furnace recipes
            Object furnaceManager = furnaceClazz.getDeclaredMethod("a").invoke(null);
            Object furnaceRecipes = furnaceRecipesField.get(furnaceManager);
            Object furnaceRecipesExp = furnaceRecipesExpField.get(furnaceManager);
            Object metaFurnaceRecipes = metaFurnaceRecipesField.get(furnaceManager);
            Object metaFurnaceRecipesExp = metaFurnaceRecipesExpField.get(furnaceManager);

            //reload the server
            plugin.getServer().reload();

            //set crafting manager
            craftingManager = craftClazz.getDeclaredMethod("a").invoke(null);
            recipeListField.set(craftingManager, crafting);

            //set furnace recipes
            furnaceManager = furnaceClazz.getDeclaredMethod("a").invoke(null);
            furnaceRecipesField.set(furnaceManager, furnaceRecipes);
            furnaceRecipesExpField.set(furnaceManager, furnaceRecipesExp);
            metaFurnaceRecipesField.set(furnaceManager, metaFurnaceRecipes);
            metaFurnaceRecipesExpField.set(furnaceManager, metaFurnaceRecipesExp);

            plugin.getServer().broadcastMessage(ChatColor.GREEN + "Reload Complete");
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        try {
            plugin.getServer().broadcastMessage(ChatColor.GREEN + "Reloading plugins, test 2");

            //fetch forges private class loader ;)
            Method relauncherMethod = FMLRelauncher.class.getDeclaredMethod("instance");
            relauncherMethod.setAccessible(true);
            FMLRelauncher relauncher = (FMLRelauncher)relauncherMethod.invoke(null);
            Field classLoaderField = FMLRelauncher.class.getDeclaredField("classLoader");
            classLoaderField.setAccessible(true);
            RelaunchClassLoader classLoader = (RelaunchClassLoader) classLoaderField.get(relauncher);

            //clazzes
            Class craftClazz = classLoader.findClass("net.minecraft.src.yk");
            Class furnaceClazz = classLoader.findClass("net.minecraft.src.yg");



            //crafting table fields
            Field recipeListField = craftClazz.getDeclaredField("b");
            //furnace fields
            Field furnaceRecipesField = furnaceClazz.getDeclaredField("b");
            Field furnaceRecipesExpField = furnaceClazz.getDeclaredField("c");
            Field metaFurnaceRecipesField = furnaceClazz.getDeclaredField("metaSmeltingList");
            Field metaFurnaceRecipesExpField = furnaceClazz.getDeclaredField("metaExperience");

            //make all fields accessible
            recipeListField.setAccessible(true);
            furnaceRecipesField.setAccessible(true);
            furnaceRecipesExpField.setAccessible(true);
            metaFurnaceRecipesField.setAccessible(true);
            metaFurnaceRecipesExpField.setAccessible(true);

            //fetch crafting manager
            Object craftingManager = craftClazz.getDeclaredMethod("a").invoke(null);
            Object crafting = recipeListField.get(craftingManager);

            //fetch furnace recipes
            Object furnaceManager = furnaceClazz.getDeclaredMethod("a").invoke(null);
            Object furnaceRecipes = furnaceRecipesField.get(furnaceManager);
            Object furnaceRecipesExp = furnaceRecipesExpField.get(furnaceManager);
            Object metaFurnaceRecipes = metaFurnaceRecipesField.get(furnaceManager);
            Object metaFurnaceRecipesExp = metaFurnaceRecipesExpField.get(furnaceManager);

            //reload the server
            plugin.getServer().reload();

            //set crafting manager
            craftingManager = craftClazz.getDeclaredMethod("a").invoke(null);
            recipeListField.set(craftingManager, crafting);

            //set furnace recipes
            furnaceManager = furnaceClazz.getDeclaredMethod("a").invoke(null);
            furnaceRecipesField.set(furnaceManager, furnaceRecipes);
            furnaceRecipesExpField.set(furnaceManager, furnaceRecipesExp);
            metaFurnaceRecipesField.set(furnaceManager, metaFurnaceRecipes);
            metaFurnaceRecipesExpField.set(furnaceManager, metaFurnaceRecipesExp);

            plugin.getServer().broadcastMessage(ChatColor.GREEN + "Reload Complete");
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }


        try {
            plugin.getServer().broadcastMessage(ChatColor.GREEN + "Reloading plugins, test 3");

            /*Reflections reflections = new Reflections(ClasspathHelper.forClass(Object.class),
                    new SubTypesScanner(false));*/



            /*Set<Class<? extends Object>> clazzez = reflections.getSubTypesOf(Object.class);

            for(Class<? extends Object> clazz : clazzez) {
                System.out.println(clazz.getPackage().getName() + "." + clazz.getName());
            }

                        /*Set<String> allClasses =
                    reflections.getStore().getSubTypesOf(Object.class.getName());


            /*Map<String, Multimap<String,String>> a = reflections.getStore().getStoreMap();

            for(Multimap<String, String> map : a.values()) {
                for(String className : map.values()) {
                    System.out.println(className);
                }
            }*/

            //fetch forges private class loader ;)
            Method relauncherMethod = FMLRelauncher.class.getDeclaredMethod("instance");
            relauncherMethod.setAccessible(true);
            FMLRelauncher relauncher = (FMLRelauncher)relauncherMethod.invoke(null);
            Field classLoaderField = FMLRelauncher.class.getDeclaredField("classLoader");
            classLoaderField.setAccessible(true);
            RelaunchClassLoader classLoader = (RelaunchClassLoader) classLoaderField.get(relauncher);

            //clazzes
            Class craftClazz = Class.forName("net.minecraft.yk", false, classLoader);//classLoader.findClass("net.minecraft.src.yk");
            Class furnaceClazz = Class.forName("net.minecraft.yg", false, classLoader); //classLoader.findClass("net.minecraft.src.yg");



            //crafting table fields
            Field recipeListField = craftClazz.getDeclaredField("b");
            //furnace fields
            Field furnaceRecipesField = furnaceClazz.getDeclaredField("b");
            Field furnaceRecipesExpField = furnaceClazz.getDeclaredField("c");
            Field metaFurnaceRecipesField = furnaceClazz.getDeclaredField("metaSmeltingList");
            Field metaFurnaceRecipesExpField = furnaceClazz.getDeclaredField("metaExperience");

            //make all fields accessible
            recipeListField.setAccessible(true);
            furnaceRecipesField.setAccessible(true);
            furnaceRecipesExpField.setAccessible(true);
            metaFurnaceRecipesField.setAccessible(true);
            metaFurnaceRecipesExpField.setAccessible(true);

            //fetch crafting manager
            Object craftingManager = craftClazz.getDeclaredMethod("a").invoke(null);
            Object crafting = recipeListField.get(craftingManager);

            //fetch furnace recipes
            Object furnaceManager = furnaceClazz.getDeclaredMethod("a").invoke(null);
            Object furnaceRecipes = furnaceRecipesField.get(furnaceManager);
            Object furnaceRecipesExp = furnaceRecipesExpField.get(furnaceManager);
            Object metaFurnaceRecipes = metaFurnaceRecipesField.get(furnaceManager);
            Object metaFurnaceRecipesExp = metaFurnaceRecipesExpField.get(furnaceManager);

            //reload the server
            Bukkit.reload();
            //plugin.getServer().reload();

            //set crafting manager
            craftingManager = craftClazz.getDeclaredMethod("a").invoke(null);
            recipeListField.set(craftingManager, crafting);

            //set furnace recipes
            furnaceManager = furnaceClazz.getDeclaredMethod("a").invoke(null);
            furnaceRecipesField.set(furnaceManager, furnaceRecipes);
            furnaceRecipesExpField.set(furnaceManager, furnaceRecipesExp);
            metaFurnaceRecipesField.set(furnaceManager, metaFurnaceRecipes);
            metaFurnaceRecipesExpField.set(furnaceManager, metaFurnaceRecipesExp);

            plugin.getServer().broadcastMessage(ChatColor.GREEN + "Reload Complete");
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }


    }
}
