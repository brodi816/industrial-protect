package net.industrial.collection.commands;

import net.industrial.collection.player.ITPlayer;

/**
 * Created with IntelliJ IDEA.
 * User: brodi
 * Date: 6/12/13
 * Time: 3:09 AM
 * To change this template use File | Settings | File Templates.
 */
public class ITCommandRunner implements Runnable {
    private final ITCommand command;
    private final ITPlayer player;
    private final String[] args;

    public ITCommandRunner(ITCommand command, ITPlayer player, String[] args) {
        this.command = command;
        this.player = player;
        this.args = args;
    }

    @Override
    public void run() {
        command.onPlayerUse(player, args);
    }
}
