package net.industrial.collection.commands.testing;

import net.industrial.collection.commands.ITCommand;
import net.industrial.collection.commands.ITCommands;
import net.industrial.collection.packets.mp3.PacketHelper;
import net.industrial.collection.player.ITPlayer;
import org.bukkit.ChatColor;

/**
 * Created with IntelliJ IDEA.
 * User: Brodi
 * Date: 7/10/13
 * Time: 2:00 PM
 * To change this template use File | Settings | File Templates.
 */
public class ITCommandPlayCachedMP3 extends ITCommand {
    public ITCommandPlayCachedMP3(ITCommands commands, String name, short rankNeeded) {
        super(commands, name, rankNeeded);
        arguments.add(new Argument("help", "Plays a cached mp3 on your client"));
        arguments.add(new Argument("name", "The name of the cached mp3 to play"));
        arguments.add(new Argument("volume", "The volume(0-100) to play the cached mp3 at"));
    }

    @Override
    public void onPlayerUse(ITPlayer player, String[] args) {
        if(args.length > 2) {
            float volume = 100;

            try {
                volume = Float.parseFloat(args[2]);
            } catch (Exception ex) { }

            PacketHelper.sendMP3PlayCachedMP3msg(player, "test", args[1], volume);

        } else {
            player.getPlayer().sendMessage(ChatColor.DARK_RED + "Not enough arguments");
            printArguments(player);
        }
    }

}
