package net.industrial.collection.commands.testing;

import net.industrial.collection.commands.ITCommand;
import net.industrial.collection.commands.ITCommands;
import net.industrial.collection.packets.mp3.PacketHelper;
import net.industrial.collection.player.ITPlayer;
import org.bukkit.ChatColor;

/**
 * Created with IntelliJ IDEA.
 * User: Brodi
 * Date: 7/10/13
 * Time: 1:54 PM
 * To change this template use File | Settings | File Templates.
 */
public class ITCommandCacheMP3 extends ITCommand {
    public ITCommandCacheMP3(ITCommands commands, String name, short rankNeeded) {
        super(commands, name, rankNeeded);
        arguments.add(new Argument("help", "Caches a mp3 file to your client"));
        arguments.add(new Argument("name", "The name of the mp3 to cache"));
        arguments.add(new Argument("url", "The url of the mp3 to cache"));
    }

    @Override
    public void onPlayerUse(ITPlayer player, String[] args) {
        if(args.length > 2) {
            PacketHelper.sendMP3CacheMP3msg(player, args[1], args[2], 0);
        } else {
            player.getPlayer().sendMessage(ChatColor.DARK_RED + "Not enough arguments");
            printArguments(player);
        }
    }
}
