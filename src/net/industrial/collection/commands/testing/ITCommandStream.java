package net.industrial.collection.commands.testing;

import net.industrial.collection.commands.ITCommand;
import net.industrial.collection.commands.ITCommands;
import net.industrial.collection.packets.mp3.PacketHelper;
import net.industrial.collection.player.ITPlayer;
import org.bukkit.ChatColor;

/**
 * Created with IntelliJ IDEA.
 * User: Brodi
 * Date: 7/9/13
 * Time: 10:18 PM
 * To change this template use File | Settings | File Templates.
 */
public class ITCommandStream extends ITCommand {
    public ITCommandStream(ITCommands commands, String name, short rankNeeded) {
        super(commands, name, rankNeeded);
        arguments.add(new Argument("help", "Send a packet to stream an mp3 if you have the mp3 streaming mod installed"));
        arguments.add(new Argument("url", "The url to stream"));
        arguments.add(new Argument("volume", "The volume to set it at"));
    }

    @Override
    public void onPlayerUse(ITPlayer player, String[] args) {
        if(args.length > 2) {
            String url = args[1];
            float volume = 100.0f;
            try {
                volume = Float.parseFloat(args[2]);
            } catch (Exception ex) { }

            PacketHelper.sendMP3StreamToChannelMsg(player, "test", url, volume);

        } else {
            player.getPlayer().sendMessage(ChatColor.DARK_RED + "Not enough arguments");
            printArguments(player);
        }
    }
}
