package net.industrial.collection.commands;

import net.industrial.collection.player.ITPlayer;

/**
 * Created with IntelliJ IDEA.
 * User: Brodi
 * Date: 6/10/13
 * Time: 12:22 AM
 * To change this template use File | Settings | File Templates.
 */
public class ITCommandInvalidCommand extends ITCommand {

    public ITCommandInvalidCommand(ITCommands commands) {
        super(commands, "LOL NO", (short)0);
    }

    @Override
    public void onPlayerUse(ITPlayer player, String[] args) {
        player.getPlayer().sendMessage("Invalid Command " + args[0]);
    }
}
