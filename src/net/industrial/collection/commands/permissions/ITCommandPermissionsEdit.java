package net.industrial.collection.commands.permissions;

import net.industrial.collection.commands.ITCommand;
import net.industrial.collection.commands.ITCommands;
import net.industrial.collection.player.ITPlayer;
import org.bukkit.ChatColor;

/**
 * Created with IntelliJ IDEA.
 * User: Brodi
 * Date: 6/24/13
 * Time: 6:20 PM
 * To change this template use File | Settings | File Templates.
 */
public class ITCommandPermissionsEdit extends ITCommand {
    public ITCommandPermissionsEdit(ITCommands commands, String name, short rankNeeded) {
        super(commands, name, rankNeeded);
        arguments.add(new Argument("help", "Edits the permission at the specified index"));
    }

    @Override
    public void onPlayerUse(ITPlayer player, String[] args) {
        player.getPlayer().sendMessage(ChatColor.DARK_RED + "Not yet implemented");
    }
}
