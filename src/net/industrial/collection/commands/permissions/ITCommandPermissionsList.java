package net.industrial.collection.commands.permissions;

import net.industrial.collection.ITPageBuilder;
import net.industrial.collection.commands.ITCommand;
import net.industrial.collection.commands.ITCommands;
import net.industrial.collection.permissions.ITPermission;
import net.industrial.collection.player.ITPlayer;

/**
 * Created with IntelliJ IDEA.
 * User: Brodi
 * Date: 6/24/13
 * Time: 5:39 PM
 * To change this template use File | Settings | File Templates.
 */
public class ITCommandPermissionsList extends ITCommand {
    public ITCommandPermissionsList(ITCommands commands, String name, short rankNeeded) {
        super(commands, name, rankNeeded);
        arguments.add(new Argument("help", "List all the permissions in page format"));
        arguments.add(new Argument("[page]", "The page of permissions to view"));
    }

    @Override
    public void onPlayerUse(ITPlayer player, String[] args) {
        ITPermission[] permissions = plugin.permissions.getPermissions();
        ITPageBuilder pageBuilder = new ITPageBuilder();

        int page = 1;
        if(args.length > 1) {
            try {
                page = Integer.parseInt(args[1]);
            } catch (Exception ex) {}
        }

        pageBuilder.lines.add("Total Permissions: " + permissions.length);
        pageBuilder.lines.add("Index, Name, MinRank-MaxRank");
        for(int i = 0; i < permissions.length; i++) {
            pageBuilder.lines.add("" + i + ", " +  permissions[i].getName() + ", " + permissions[i].getMinRank() + "-" +permissions[i].getMaxRank());
        }

        pageBuilder.printPage(player.getPlayer(), page, 8);
    }
}
