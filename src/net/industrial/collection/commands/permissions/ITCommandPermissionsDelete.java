package net.industrial.collection.commands.permissions;

import net.industrial.collection.commands.ITCommand;
import net.industrial.collection.commands.ITCommands;
import net.industrial.collection.permissions.ITPermission;
import net.industrial.collection.player.ITPlayer;
import org.bukkit.ChatColor;

/**
 * Created with IntelliJ IDEA.
 * User: Brodi
 * Date: 6/24/13
 * Time: 6:38 PM
 * To change this template use File | Settings | File Templates.
 */
public class ITCommandPermissionsDelete extends ITCommand {
    public ITCommandPermissionsDelete(ITCommands commands, String name, short rankNeeded) {
        super(commands, name, rankNeeded);
        arguments.add(new Argument("help", "Removes the permission at the specified index"));
        arguments.add(new Argument("index", "The index of the permission to delete"));
    }

    @Override
    public void onPlayerUse(ITPlayer player, String[] args) {
        if(args.length > 1) {
            int index = -1;

            try {
                index = Integer.parseInt(args[1]);
            } catch (Exception ex) {
                player.getPlayer().sendMessage(ChatColor.DARK_RED + "Index: " + args[1] + " not valid");
            }

            if(index < 0 || index >= plugin.permissions.permissionCount()) {
                player.getPlayer().sendMessage(ChatColor.DARK_RED + "No permission at that index");
            }

            ITPermission permission = plugin.permissions.getPermission(index);
            plugin.permissions.deletePermission(index);
            player.getPlayer().sendMessage(ChatColor.GREEN + "Removed permission: " + permission.getName() + " " + permission.getMinRank() + "-" + permission.getMaxRank());

        } else {
            player.getPlayer().sendMessage(ChatColor.DARK_RED + "Not enough arguments");
            printArguments(player);
        }
    }
}
