package net.industrial.collection.commands.permissions;

import net.industrial.collection.commands.ITCommand;
import net.industrial.collection.commands.ITCommands;

/**
 * Created with IntelliJ IDEA.
 * User: Brodi
 * Date: 6/24/13
 * Time: 5:32 PM
 * To change this template use File | Settings | File Templates.
 */
public class ITCommandPermissions extends ITCommand {
    public ITCommandPermissions(ITCommands commands, String name, short rankNeeded) {
        super(commands, name, rankNeeded);

        arguments.add(new Argument("help", "The base function of permission management"));
    }
}
