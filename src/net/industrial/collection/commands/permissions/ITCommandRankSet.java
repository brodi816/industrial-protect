package net.industrial.collection.commands.permissions;

import net.industrial.collection.commands.ITCommand;
import net.industrial.collection.commands.ITCommands;
import net.industrial.collection.player.ITPlayer;
import net.industrial.collection.player.ITPlayerData;
import org.bukkit.ChatColor;

/**
 * Created with IntelliJ IDEA.
 * User: Brodi
 * Date: 6/25/13
 * Time: 2:11 PM
 * To change this template use File | Settings | File Templates.
 */
public class ITCommandRankSet extends ITCommand {
    public ITCommandRankSet(ITCommands commands, String name, short rankNeeded) {
        super(commands, name, rankNeeded);
        arguments.add(new Argument("help", "Sets the players rank to the specified rank"));
        arguments.add(new Argument("player", "The players rank to set"));
        arguments.add(new Argument("rank", "The rank to set the player at"));
    }

    @Override
    public void onPlayerUse(ITPlayer player, String[] args) {
        if(args.length > 2) {
            ITPlayerData playerData = plugin.getPlayerData(args[1]);
            short rank = -1;

            if(playerData == null) {
                player.getPlayer().sendMessage(ChatColor.DARK_RED + "Player " + args[1] + " not found");
                return;
            }

            try {
                rank = Short.parseShort(args[2]);
            } catch (Exception ex) {
                player.getPlayer().sendMessage(ChatColor.DARK_RED + "Specified rank: is not valid(0-32767): " + args[2]);
                return;
            }

            if(rank < -1) {
                player.getPlayer().sendMessage(ChatColor.DARK_RED + "Specified rank: is not valid(0-32767): " + args[2]);
                return;
            }

            if(rank >= player.data.getRank()) {
                player.getPlayer().sendMessage(ChatColor.DARK_RED + "Can not set a player rank(" + rank + ") above yours(" + player.data.getRank() + ")");
                return;
            }

            playerData.setRank(rank);

            player.getPlayer().sendMessage(ChatColor.GRAY + "Set player " + playerData.name + " rank to: " + playerData.getRank());

        } else {
            player.getPlayer().sendMessage(ChatColor.DARK_RED + "Not enough arguments");
            printArguments(player);
        }
    }
}
