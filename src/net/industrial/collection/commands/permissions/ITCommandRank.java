package net.industrial.collection.commands.permissions;

import net.industrial.collection.commands.ITCommand;
import net.industrial.collection.commands.ITCommands;

/**
 * Created with IntelliJ IDEA.
 * User: Brodi
 * Date: 6/25/13
 * Time: 2:08 PM
 * To change this template use File | Settings | File Templates.
 */
public class ITCommandRank extends ITCommand {
    public ITCommandRank(ITCommands commands, String name, short rankNeeded) {
        super(commands, name, rankNeeded);
        arguments.add(new Argument("help", "Base command for managing players ranks"));
    }
}
