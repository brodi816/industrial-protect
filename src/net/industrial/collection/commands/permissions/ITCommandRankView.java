package net.industrial.collection.commands.permissions;

import net.industrial.collection.commands.ITCommand;
import net.industrial.collection.commands.ITCommands;
import net.industrial.collection.player.ITPlayer;
import net.industrial.collection.player.ITPlayerData;
import org.bukkit.ChatColor;

/**
 * Created with IntelliJ IDEA.
 * User: Brodi
 * Date: 6/25/13
 * Time: 2:10 PM
 * To change this template use File | Settings | File Templates.
 */
public class ITCommandRankView extends ITCommand {
    public ITCommandRankView(ITCommands commands, String name, short rankNeeded) {
        super(commands, name, rankNeeded);
        arguments.add(new Argument("help", "View the specified players rank"));
        arguments.add(new Argument("player", "The player to view"));
    }

    @Override
    public void onPlayerUse(ITPlayer player, String[] args) {
        if(args.length > 1) {
            ITPlayerData playerData = plugin.getPlayerData(args[1]);

            if(playerData == null) {
                player.getPlayer().sendMessage(ChatColor.DARK_RED + "Player " + args[1] + " not found");
                return;
            }

            player.getPlayer().sendMessage(ChatColor.GRAY + "Player " + playerData.name + " rank is: " + playerData.getRank());

        } else {
            player.getPlayer().sendMessage(ChatColor.DARK_RED + "Not enough arguments");
            printArguments(player);
        }
    }
}
