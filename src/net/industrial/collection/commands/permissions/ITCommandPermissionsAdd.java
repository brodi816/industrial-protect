package net.industrial.collection.commands.permissions;

import net.industrial.collection.commands.ITCommand;
import net.industrial.collection.commands.ITCommands;
import net.industrial.collection.permissions.ITPermission;
import net.industrial.collection.player.ITPlayer;
import net.industrial.collection.sql.ITMySQLQueue;
import net.industrial.collection.sql.ITMySQLQueueItem;
import org.bukkit.ChatColor;

/**
 * Created with IntelliJ IDEA.
 * User: Brodi
 * Date: 6/24/13
 * Time: 6:20 PM
 * To change this template use File | Settings | File Templates.
 */
public class ITCommandPermissionsAdd extends ITCommand {
    public ITCommandPermissionsAdd(ITCommands commands, String name, short rankNeeded) {
        super(commands, name, rankNeeded);
        arguments.add(new Argument("help", "Adds a rank based permission"));
        arguments.add(new Argument("name", "The name of the permission"));
        arguments.add(new Argument("minRank", "The minimum rank required for the permission"));
        arguments.add(new Argument("maxRank", "The maximum allowed rank for the permission"));
    }

    @Override
    public void onPlayerUse(ITPlayer player, String[] args) {
        if(args.length > 3) {
            String name = args[1];
            short minRank = -1;
            short maxRank = -1;

            try {
                minRank = Short.parseShort(args[2]);
            } catch (Exception ex) {
                player.getPlayer().sendMessage(ChatColor.DARK_RED + "Invalid min rank(0-32767): " + args[2]);
                return;
            }

            try {
                maxRank = Short.parseShort(args[3]);
            } catch (Exception ex) {
                player.getPlayer().sendMessage(ChatColor.DARK_RED + "Invalid max rank(0-32767): " + args[3]);
                return;
            }

            if(minRank < 0) {
                player.getPlayer().sendMessage(ChatColor.DARK_RED + "Invalid min rank(0-32767): " + args[2]);
                return;
            }
            if(maxRank < 0) {
                player.getPlayer().sendMessage(ChatColor.DARK_RED + "Invalid max rank(0-32767): " + args[3]);
                return;
            }

            ITPermission permission = new ITPermission(plugin, name, minRank, maxRank);
            plugin.permissions.addPermission(permission);

            player.getPlayer().sendMessage(ChatColor.GREEN + String.format("Added permission %s \n index: %d \n minRank: %d \n maxRank %d", name, plugin.permissions.permissionCount(), minRank, maxRank));

            plugin.addQueueItem(new ITMySQLQueueItem(permission, ITMySQLQueue.QueueOperation.UPDATE));

        } else {
            player.getPlayer().sendMessage(ChatColor.DARK_RED + "Not enough arguments");
            printArguments(player);
        }
    }
}
