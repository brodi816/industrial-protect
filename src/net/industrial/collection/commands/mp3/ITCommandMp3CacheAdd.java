package net.industrial.collection.commands.mp3;

import net.industrial.collection.commands.ITCommand;
import net.industrial.collection.commands.ITCommands;
import net.industrial.collection.mp3.ITCachedMp3;
import net.industrial.collection.player.ITPlayer;
import org.bukkit.ChatColor;

/**
 * Created with IntelliJ IDEA.
 * User: Brodi
 * Date: 7/12/13
 * Time: 1:54 AM
 * To change this template use File | Settings | File Templates.
 */
public class ITCommandMp3CacheAdd extends ITCommand {
    public ITCommandMp3CacheAdd(ITCommands commands, String name, short rankNeeded) {
        super(commands, name, rankNeeded);
        arguments.add(new Argument("help","Adds a new cached mp3"));
        arguments.add(new Argument("name", "The name to use for the downloaded mp3 file"));
        arguments.add(new Argument("url", "The url to download the mp3 from"));
    }

    @Override
    public void onPlayerUse(ITPlayer player, String[] args) {
        if(args.length > 2) {
            ITCachedMp3 mp3 = new ITCachedMp3(plugin.getMp3Cache(), args[1], args[2]);
            plugin.getMp3Cache().addAndDownloadMp3(mp3);
            player.getPlayer().sendMessage("Added MP3: " + args[1] + " to cache");
        } else {
            player.getPlayer().sendMessage(ChatColor.DARK_RED + "Not enough arguments");
            printArguments(player);
        }
    }
}
