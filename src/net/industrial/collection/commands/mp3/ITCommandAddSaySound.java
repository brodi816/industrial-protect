package net.industrial.collection.commands.mp3;

import net.industrial.collection.commands.ITCommand;
import net.industrial.collection.commands.ITCommands;
import net.industrial.collection.mp3.ITSaySound;
import net.industrial.collection.player.ITPlayer;
import org.bukkit.ChatColor;

/**
 * Created with IntelliJ IDEA.
 * User: Brodi
 * Date: 7/12/13
 * Time: 5:37 PM
 * To change this template use File | Settings | File Templates.
 */
public class ITCommandAddSaySound extends ITCommand {
    public ITCommandAddSaySound(ITCommands commands, String name, short rankNeeded) {
        super(commands, name, rankNeeded);
        arguments.add(new Argument("help", "Adds a say sound"));
        arguments.add(new Argument("name...", "The name that players the say sound"));
        arguments.add(new Argument("mp3", "The mp3 to play"));
    }

    @Override
    public void onPlayerUse(ITPlayer player, String[] args) {
        if(args.length > 2) {
            String saySoundTrigger = args[1];
            //try to rebuild a name that has spaces in it
            for(int i = 2; i < args.length-1;i++) {
                saySoundTrigger += " " + args[i];
            }

            plugin.saySounds.addSound(new ITSaySound(plugin.saySounds, saySoundTrigger.toLowerCase(), args[args.length-1]));
            player.getPlayer().sendMessage(ChatColor.GRAY + "Added say sound: " + saySoundTrigger + " file: " + args[args.length-1]);

        } else {
            player.getPlayer().sendMessage(ChatColor.DARK_RED + "Not enough arguments");
            printArguments(player);
        }
    }
}
