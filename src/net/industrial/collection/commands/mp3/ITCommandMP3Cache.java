package net.industrial.collection.commands.mp3;

import net.industrial.collection.commands.ITCommand;
import net.industrial.collection.commands.ITCommands;

/**
 * Created with IntelliJ IDEA.
 * User: Brodi
 * Date: 7/15/13
 * Time: 3:07 AM
 * To change this template use File | Settings | File Templates.
 */
public class ITCommandMp3Cache extends ITCommand {
    public ITCommandMp3Cache(ITCommands commands, String name, short rankNeeded) {
        super(commands, name, rankNeeded);
        arguments.add(new Argument("help", "The base class for mp3 cache management"));
    }
}
