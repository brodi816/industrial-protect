package net.industrial.collection.commands.mp3;

import net.industrial.collection.commands.ITCommand;
import net.industrial.collection.commands.ITCommands;
import net.industrial.collection.mp3.ITCachedMp3;
import net.industrial.collection.player.ITPlayer;
import org.bukkit.ChatColor;

/**
 * Created with IntelliJ IDEA.
 * User: Brodi
 * Date: 7/15/13
 * Time: 3:10 AM
 * To change this template use File | Settings | File Templates.
 */
public class ITCommandMp3CacheUpdate extends ITCommand {
    public ITCommandMp3CacheUpdate(ITCommands commands, String name, short rankNeeded) {
        super(commands, name, rankNeeded);
        arguments.add(new Argument("help", "Redownloads the mp3 and updates the cache"));
        arguments.add(new Argument("mp3", "The mp3 to update"));
    }

    @Override
    public void onPlayerUse(ITPlayer player, String[] args) {
        if(args.length > 1) {
            ITCachedMp3 mp3 = plugin.getMp3Cache().getMp3(args[1]);

            if(mp3 == null) {
                player.getPlayer().sendMessage(ChatColor.DARK_RED + "Mp3: " + args[1] + " doesn't exist");
                return;
            }

            plugin.getMp3Cache().downloadMp3(mp3);
            player.getPlayer().sendMessage(ChatColor.GREEN + "Redownloading mp3: " + mp3.getName());

        } else {
            player.getPlayer().sendMessage(ChatColor.DARK_RED + "Not enough arguments");
            printArguments(player);
        }
    }
}
