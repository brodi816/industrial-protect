package net.industrial.collection.commands.mp3;

import net.industrial.collection.ITPageBuilder;
import net.industrial.collection.commands.ITCommand;
import net.industrial.collection.commands.ITCommands;
import net.industrial.collection.mp3.ITSaySound;
import net.industrial.collection.player.ITPlayer;

/**
 * Created with IntelliJ IDEA.
 * User: Brodi
 * Date: 7/12/13
 * Time: 9:52 PM
 * To change this template use File | Settings | File Templates.
 */
public class ITCommandSoundList extends ITCommand {
    public ITCommandSoundList(ITCommands commands, String name, short rankNeeded) {
        super(commands, name, rankNeeded);
        arguments.add(new Argument("help", "Lists all the sounds available to play"));
        arguments.add(new Argument("[page]", "The page to list"));
    }

    @Override
    public void onPlayerUse(ITPlayer player, String[] args) {
        int page = 1;
        if(args.length > 1) {
            try {
                page = Integer.parseInt(args[1]);
            } catch (Exception ex) {
            }
        }

        ITPageBuilder pageBuilder = new ITPageBuilder();

        for(ITSaySound sound : plugin.saySounds.getSounds()) {
            pageBuilder.lines.add(sound.getName());
        }

        pageBuilder.printPage(player.getPlayer() , page, 8);
    }
}
