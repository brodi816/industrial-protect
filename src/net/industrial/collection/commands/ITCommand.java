package net.industrial.collection.commands;

import net.industrial.collection.ITPlugin;
import net.industrial.collection.player.ITPlayer;
import org.bukkit.ChatColor;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Brodi
 * Date: 6/9/13
 * Time: 2:12 AM
 * To change this template use File | Settings | File Templates.
 */
public class ITCommand {
    public class Argument {
        public final String name, help;
        public Argument(String name, String help) {
            this.name = name;
            this.help = help;
        }
    }

    public final ITPlugin plugin;
    public final ITCommands commands;
    public final String name;
    public final short rankNeeded;
    public final List<Argument> arguments = new ArrayList<Argument>();
    private ITCommand parentCommand;
    private final List<ITCommand> subCommands = new ArrayList<ITCommand>();

    public ITCommand(ITCommands commands, String name, short rankNeeded) {
        this.plugin = commands.plugin;
        this.commands = commands;
        this.name = name;
        this.rankNeeded = rankNeeded;
    }

    public void onPlayerUse(ITPlayer player, String[] args) {

    }

    public ITCommand[] getSubCommands() {
        return subCommands.toArray(new ITCommand[]{});
    }

    public int getSubCommandsLength() {
        return subCommands.size();
    }

    public void addSubCommand(ITCommand command) {
        command.parentCommand = this;
        subCommands.add(command);
    }

    public String getParentName(String total) {
        if(parentCommand != null) {
            return parentCommand.getParentName(name + " " + total);
        } else {
            return name + " " + total;
        }
    }

    public void printArguments(ITPlayer player) {
        StringBuilder message = new StringBuilder();
        message.append("/");
        message.append(ChatColor.GOLD);
        message.append(getParentName(""));
        message.append(ChatColor.GRAY);
        for (int i = 1; i < arguments.size(); i++) {
            message.append(" ");
            message.append(arguments.get(i).name);
        }
        player.getPlayer().sendMessage(message.toString());
    }
}
