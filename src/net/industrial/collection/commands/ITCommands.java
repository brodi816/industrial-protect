package net.industrial.collection.commands;

import net.industrial.collection.ITPlugin;
import net.industrial.collection.commands.mp3.*;
import net.industrial.collection.commands.relationships.ITCommandFriend;
import net.industrial.collection.commands.permissions.*;
import net.industrial.collection.commands.protection.*;
import net.industrial.collection.commands.relationships.*;
import net.industrial.collection.commands.testing.ITCommandCacheMP3;
import net.industrial.collection.commands.testing.ITCommandPlayCachedMP3;
import net.industrial.collection.commands.testing.ITCommandStream;
import net.industrial.collection.player.ITPlayer;
import org.bukkit.help.HelpMap;
import org.bukkit.help.HelpTopic;
import org.bukkit.help.IndexHelpTopic;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Brodi
 * Date: 6/9/13
 * Time: 2:25 AM
 * To change this template use File | Settings | File Templates.
 */
public class ITCommands {
    public final ITPlugin plugin;

    public final List<ITCommand> commands = new ArrayList<ITCommand>();

    public ITCommands(ITPlugin plugin) {
        this.plugin = plugin;



    }

    public void addCommands() {
        ITCommand friendCommand = new ITCommandFriend(this, "friend", (short) 1);
        friendCommand.addSubCommand(new ITCommandFriendAdd(this, "add", (short)1));
        friendCommand.addSubCommand(new ITCommandFriendRemove(this, "remove", (short)1));
        friendCommand.addSubCommand(new ITCommandFriendList(this, "list", (short)1));
        friendCommand.addSubCommand(new ITCommandFriendSetRank(this, "setrank", (short)1));
        commands.add(friendCommand);


        ITCommand protectCommand = new ITCommandProtect(this, "protect", (short)1);
        protectCommand.addSubCommand(new ITCommandProtectFlags(this, "flags", (short)1));
        protectCommand.addSubCommand(new ITCommandProtectSetFlag(this, "setflag", (short)1));
        commands.add(protectCommand);

        ITCommand chunkCommand = new ITCommandChunk(this, "chunk", (short)1);
        chunkCommand.addSubCommand(new ITCommandChunkClaim(this, "claim", (short)1));
        chunkCommand.addSubCommand(new ITCommandChunkInfo(this, "info", (short)1));
        chunkCommand.addSubCommand(new ITCommandChunkUnclaim(this, "unclaim", (short)1));
        commands.add(chunkCommand);

        ITCommand permissionsCommand = new ITCommandPermissions(this, "permissions", (short)30000);
        permissionsCommand.addSubCommand(new ITCommandPermissionsAdd(this, "add", (short)30000));
        permissionsCommand.addSubCommand(new ITCommandPermissionsList(this, "list", (short)30000));
        permissionsCommand.addSubCommand(new ITCommandPermissionsEdit(this, "edit", (short)30000));
        permissionsCommand.addSubCommand(new ITCommandPermissionsDelete(this, "delete", (short)30000));
        commands.add(permissionsCommand);

        ITCommand rankCommand = new ITCommandRank(this, "rank", (short)1);
        rankCommand.addSubCommand(new ITCommandRankSet(this, "set", (short)30000));
        rankCommand.addSubCommand(new ITCommandRankView(this, "view", (short)1));
        commands.add(rankCommand);

        ITCommand groupCommand = new ITCommandGroup(this, "group", (short)1);
        groupCommand.addSubCommand(new ITCommandGroupAdd(this, "add", (short)1));
        groupCommand.addSubCommand(new ITCommandGroupCreate(this, "create", (short)1));
        groupCommand.addSubCommand(new ITCommandGroupInfo(this, "info", (short)1));
        groupCommand.addSubCommand(new ITCommandGroupLeave(this, "leave", (short)1));
        groupCommand.addSubCommand(new ITCommandGroupList(this, "list", (short)1));
        groupCommand.addSubCommand(new ITCommandGroupMembers(this, "members", (short)1));

        ITCommand groupPermissionsCommand = new ITCommandGroupPermissions(this, "permissions", (short)1);
        groupPermissionsCommand.addSubCommand(new ITCommandGroupPermissionsList(this, "list", (short)1));
        groupPermissionsCommand.addSubCommand(new ITCommandGroupPermissionsSet(this, "set", (short)1));
        groupCommand.addSubCommand(groupPermissionsCommand);

        groupCommand.addSubCommand(new ITCommandGroupRemove(this, "remove", (short)1));

        commands.add(groupCommand);

        //commands.add(new ITCommandForceReload(this, "forcereload", (short)30000));
        commands.add(new ITCommandAddDisabledItem(this, "adddisableditem", (short)30000));
        commands.add(new ITCommandStream(this, "stream", (short)32767));
        commands.add(new ITCommandCacheMP3(this, "cachemp3", (short)32767));
        commands.add(new ITCommandPlayCachedMP3(this, "playcachedmp3", (short)32767));

        ITCommand mp3CacheCommand = new ITCommandMp3Cache(this, "mp3cache", (short)32767);
        mp3CacheCommand.addSubCommand(new ITCommandMp3CacheAdd(this, "add", (short) 32767));
        mp3CacheCommand.addSubCommand(new ITCommandMp3CacheUpdate(this, "update", (short)32767));
        commands.add(mp3CacheCommand);


        commands.add(new ITCommandAddSaySound(this, "addsaysound", (short)32767));
        commands.add(new ITCommandSoundList(this, "soundlist", (short)1));

        addCommandsToHelp();
    }

    private void addCommandsToHelp() {
        //add each command help topic to the server
        for(ITCommand command : commands) {
            buildHelpTopic(plugin.getServer().getHelpMap(), command, "/");
        }
    }

    private HelpTopic buildHelpTopic(HelpMap map, ITCommand command, String prefix) {
        if(command.getSubCommandsLength() > 0) {
            //create the topics
            List<HelpTopic> topics = new ArrayList<HelpTopic>();
            for(ITCommand c : command.getSubCommands()) {
                HelpTopic topic = this.buildHelpTopic(map, c, prefix + command.name + " ");
                topics.add(topic);
                map.addTopic(topic);
            }

            HelpTopic index = new IndexHelpTopic("/" + command.name, command.arguments.get(0).help, "", topics);
            map.addTopic(index);
            return index;
        } else {
            return new ITHelpTopic(command, prefix);
        }
    }

    public String[] copyRange(String[] oldArray, int start, int end) {
        String[] array = new String[end-start];
        for(int i = start; i < end; i++) {
            array[i-start] = oldArray[i];
        }

        return array;
    }

    public boolean aSyncCommandCheck(ITPlayer player, String chat) {
        //make sure it's a command
        if(!chat.startsWith("/")) {
            return false;
        }

        //split the args
        String[] args = chat.split(" ");

        //Remove the slash
        args[0] = args[0].replaceFirst("/", "");

        //check all the commands
        for(ITCommand command : commands) {
            //see if they are a high enough rank for the command
            if(player.data.getRank() >= command.rankNeeded || player.getPlayer().isOp()) {
                //see if the name matches
                if(command.name.equals(args[0])) {

                    //check for subcommands
                    if(args.length > 1) {
                        if(subCommandCheck(command, player, copyRange(args, 1, args.length))) {
                        return true;
                        }
                    }

                    command.onPlayerUse(player, args);
                    //plugin.getServer().getScheduler().runTask(plugin, new ITCommandRunner(command, player, args));
                    return true;
                }
            }
        }

        //plugin.getServer().getScheduler().runTask(plugin, new CommandRunner(new ITCommandInvalidCommand(this), player, args));
        return false;
    }

    private boolean subCommandCheck(ITCommand command, ITPlayer player, String[] args) {
        for(ITCommand subCommand : command.getSubCommands()) {
            //check rank of subcommand, then name
            if(player.data.getRank() >= subCommand.rankNeeded) {
                if(args.length > 1) {
                    if(subCommandCheck(subCommand, player, copyRange(args, 1, args.length))) {
                        return true;
                    }
                }

                if(subCommand.name.equals(args[0])) {
                    //execute subcommand and return
                    subCommand.onPlayerUse(player, args);

                    //plugin.getServer().getScheduler().runTask(plugin, new ITCommandRunner(subCommand, player, copyRange(args, 1, args.length)));
                    return true;
                }
            }
        }
        return false;
    }
}
