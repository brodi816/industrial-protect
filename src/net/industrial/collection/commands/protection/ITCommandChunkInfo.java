package net.industrial.collection.commands.protection;

import net.industrial.collection.commands.ITCommand;
import net.industrial.collection.commands.ITCommands;
import net.industrial.collection.player.ITPlayer;
import net.industrial.collection.world.ITChunk;
import org.bukkit.ChatColor;

/**
 * Created with IntelliJ IDEA.
 * User: Brodi
 * Date: 6/24/13
 * Time: 11:38 AM
 * To change this template use File | Settings | File Templates.
 */
public class ITCommandChunkInfo extends ITCommand {
    public ITCommandChunkInfo(ITCommands commands, String name, short rankNeeded) {
        super(commands, name, rankNeeded);

        arguments.add(new Argument("help", "Returns the info of the chunk you're in"));
    }

    public void onPlayerUse(ITPlayer player, String[] args) {
        ITChunk chunk = ITChunk.getChunk(player.getPlayer().getLocation().getChunk());

        if(chunk == null || !chunk.isLoaded()) {
            player.getPlayer().sendMessage(ChatColor.DARK_RED + "Chunk data still loading");
            return;
        }

        StringBuffer stringBuffer = new StringBuffer();

        stringBuffer.append("Name: " + chunk.getName() + "\n");

        if(chunk.getOwner() != null) {
            stringBuffer.append("Owner: " + chunk.getOwner().name + "\n");
            stringBuffer.append("Protection date: " + chunk.getProtectionDate() + "\n");
        } else {
            stringBuffer.append("Owner: none\n");
            stringBuffer.append("Protection date: none\n");
        }

        player.getPlayer().sendMessage(stringBuffer.toString());
    }
}
