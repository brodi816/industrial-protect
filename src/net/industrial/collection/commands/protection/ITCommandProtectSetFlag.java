package net.industrial.collection.commands.protection;

import net.industrial.collection.ITTimedActionSetBlockFlag;
import net.industrial.collection.commands.ITCommand;
import net.industrial.collection.commands.ITCommands;
import net.industrial.collection.player.ITPlayer;
import net.industrial.collection.protection.ITProtectedBlock;
import org.bukkit.ChatColor;

/**
 * Created with IntelliJ IDEA.
 * User: brodi
 * Date: 6/17/13
 * Time: 9:26 AM
 * To change this template use File | Settings | File Templates.
 */
public class ITCommandProtectSetFlag extends ITCommand {
    public ITCommandProtectSetFlag(ITCommands commands, String name, short rankNeeded) {
        super(commands, name, rankNeeded);

        arguments.add(new Argument("help", "Set a flag of a clicked protected block"));
        arguments.add(new Argument("name", "The name of the flag to edit"));
        arguments.add(new Argument("canPublic", "Is it publicly accessible"));
        arguments.add(new Argument("groupRank", "The group rank required for the flag"));
        arguments.add(new Argument("friendRank", "The friend rank required for the flag"));
    }

    @Override
    public void onPlayerUse(ITPlayer player, String[] args) {
        if(player.timedAction != null && player.timedAction instanceof ITTimedActionSetBlockFlag) {
            player.timedAction.actionEnd();
            player.timedAction = null;
            return;
        }

        if(args.length > 4) {
            String flagName = args[1];
            boolean canPublic = false;
            short groupRank = 1;
            short friendRank = 1;

            //make sure the flag is valid
            if(!ITProtectedBlock.flagExists(flagName)) {
                player.getPlayer().sendMessage(ChatColor.DARK_RED + "Flag " + flagName + " doesn't exist");
                //send the player valid flag names
                String flags = ChatColor.GRAY + "Valid flags: ";
                flags += ITProtectedBlock.flagNames[0];
                for(int i = 1; i < ITProtectedBlock.flagNames.length; i++) {
                    flags += ", " + ITProtectedBlock.flagNames[i];
                }
                player.getPlayer().sendMessage(flags);
                return;
            }

            try {
                canPublic = Boolean.parseBoolean(args[2]);
            } catch (Exception ex) {
                player.getPlayer().sendMessage(ChatColor.DARK_RED + "Publicly accessible is true/false");
                return;
            }

            //try to get the rank
            try {
                groupRank = Short.valueOf(args[3]);
            } catch (Exception ex) {
                player.getPlayer().sendMessage(ChatColor.DARK_RED +
                        String.format("Invalid group rank(0-32767): %s", args[3]));
                return;
            }

            //make sure it's a valid rank
            if(groupRank < 0) {
                player.getPlayer().sendMessage(ChatColor.DARK_RED +
                        String.format("Invalid group rank(0-32767): %s", args[3]));
            }

            //try to get the rank
            try {
                friendRank = Short.valueOf(args[4]);
            } catch (Exception ex) {
                player.getPlayer().sendMessage(ChatColor.DARK_RED +
                        String.format("Invalid friend rank(0-32767): %s", args[4]));
                return;
            }

            //make sure it's a valid rank
            if(friendRank < 0) {
                player.getPlayer().sendMessage(ChatColor.DARK_RED +
                        String.format("Invalid friend rank(0-32767): %s", args[4]));
            }

            player.setTimedAction(
                    new ITTimedActionSetBlockFlag(plugin, player, flagName, canPublic, groupRank, friendRank));

            player.getPlayer().sendMessage(String.format("Setting flag to clicked blocks \n" +
                    "Name: %s \n" +
                    "Public accessible: %b \n" +
                    "Group rank required: %d \n" +
                    "Friend rank required: %d",
                    flagName, canPublic, groupRank, friendRank));
        } else {
            player.getPlayer().sendMessage(ChatColor.DARK_RED + "Not enough arguments");
            printArguments(player);
        }
    }
}
