package net.industrial.collection.commands.protection;

import net.industrial.collection.commands.ITCommand;
import net.industrial.collection.commands.ITCommands;
import net.industrial.collection.player.ITPlayer;
import net.industrial.collection.protection.ITProtectedBlock;
import org.bukkit.ChatColor;

/**
 * Created with IntelliJ IDEA.
 * User: Brodi
 * Date: 7/6/13
 * Time: 2:33 AM
 * To change this template use File | Settings | File Templates.
 */
public class ITCommandProtectFlags extends ITCommand {
    public ITCommandProtectFlags(ITCommands commands, String name, short rankNeeded) {
        super(commands, name, rankNeeded);
        arguments.add(new Argument("help", "Prints a list out of valid flags for protected blocks to edit"));
    }

    @Override
    public void onPlayerUse(ITPlayer player, String[] args) {
        StringBuilder flagsMessage = new StringBuilder();

        flagsMessage.append(ChatColor.GRAY);
        flagsMessage.append("Valid flags:");
        flagsMessage.append(ChatColor.WHITE);

        for(String flag : ITProtectedBlock.flagNames) {
            flagsMessage.append(" ");
            flagsMessage.append(flag);
        }
        player.getPlayer().sendMessage(flagsMessage.toString());
    }
}
