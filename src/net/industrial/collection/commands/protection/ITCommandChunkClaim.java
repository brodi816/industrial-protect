package net.industrial.collection.commands.protection;

import net.industrial.collection.commands.ITCommand;
import net.industrial.collection.commands.ITCommands;
import net.industrial.collection.player.ITPlayer;
import net.industrial.collection.world.ITChunk;
import org.bukkit.ChatColor;
import org.bukkit.inventory.ItemStack;

/**
 * Created with IntelliJ IDEA.
 * User: Brodi
 * Date: 6/22/13
 * Time: 10:19 PM
 * To change this template use File | Settings | File Templates.
 */
public class ITCommandChunkClaim extends ITCommand {
    public ITCommandChunkClaim(ITCommands commands, String name, short rankNeeded) {
        super(commands, name, rankNeeded);

        arguments.add(new Argument("help", "Attempts to claim the chunk you're in"));
    }

    @Override
    public void onPlayerUse(ITPlayer player, String[] args) {
        ITChunk chunk = ITChunk.getChunk(player.getPlayer().getLocation().getChunk());

        if(chunk == null || !chunk.isLoaded()) {
            player.getPlayer().sendMessage(ChatColor.DARK_RED + "Chunk data still loading");
            return;
        }

        if(chunk.getOwner() != null) {
            player.getPlayer().sendMessage(ChatColor.DARK_RED + "This chunk is already claimed by someone");
            return;
        }

        ItemStack itemInHand = player.getPlayer().getItemInHand();

        //return if item in hand is invalid
        if(itemInHand == null || itemInHand.getTypeId() == 0) {
            player.getPlayer().sendMessage(ChatColor.DARK_RED + "You have no item in your hand as a fee to claim the chunk");

            String materials = "";
            for(ItemStack cost : plugin.config.chunkCost) {
                materials += String.format("%d:%d*%d ",cost.getTypeId(), cost.getDurability(), cost.getAmount());
            }
            player.getPlayer().sendMessage(ChatColor.GRAY + "Valid materials: " + materials);
            return;
        }

        for(ItemStack cost : plugin.config.chunkCost) {
            if(itemInHand.isSimilar(cost)) {
                if(itemInHand.getAmount() >= cost.getAmount()) {
                    player.getPlayer().getInventory().removeItem(cost);
                    chunk.setOwner(player.data);
                    player.getPlayer().sendMessage(ChatColor.GREEN + "Claimed chunk named: " + chunk.getName());
                    return;
                } else {
                    player.getPlayer().sendMessage(ChatColor.DARK_RED + "You need at least " + cost.getAmount() + " of the item in your hand to claim the chunk");
                }
            } else {
                player.getPlayer().sendMessage(ChatColor.DARK_RED + "The item in your hand is not a valid fee to claim a chunk");

                String materials = "";
                for(ItemStack costPrint : plugin.config.chunkCost) {
                    materials += String.format("%d:%d*%d ",costPrint.getTypeId(), costPrint.getDurability(), costPrint.getAmount());
                }
                player.getPlayer().sendMessage(ChatColor.GRAY + "Valid materials: " + materials);
                return;
            }
        }
    }
}
