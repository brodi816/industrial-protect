package net.industrial.collection.commands.protection;

import net.industrial.collection.commands.ITCommand;
import net.industrial.collection.commands.ITCommands;
import net.industrial.collection.player.ITPlayer;
import net.industrial.collection.world.ITChunk;
import org.bukkit.ChatColor;

/**
 * Created with IntelliJ IDEA.
 * User: Brodi
 * Date: 6/27/13
 * Time: 2:19 PM
 * To change this template use File | Settings | File Templates.
 */
public class ITCommandChunkUnclaim extends ITCommand {
    public ITCommandChunkUnclaim(ITCommands commands, String name, short rankNeeded) {
        super(commands, name, rankNeeded);
        arguments.add(new Argument("help", "Unclaim the chunk your in if your the owner"));
    }


    @Override
    public void onPlayerUse(ITPlayer player, String[] args) {
        ITChunk chunk = ITChunk.getChunk(player.getPlayer().getLocation().getChunk());

        if(chunk == null || !chunk.isLoaded()) {
            player.getPlayer().sendMessage(ChatColor.DARK_RED + "Chunk data still loading");
            return;
        }

        if(chunk.getOwner() != player.data) {
            player.getPlayer().sendMessage(ChatColor.DARK_RED + "Chunk is owned by: " + chunk.getOwner().name);
            return;
        }

        chunk.setOwner(null);

        player.getPlayer().sendMessage(ChatColor.GREEN + "Unclaimed chunk: " + chunk.getName());
    }
}
