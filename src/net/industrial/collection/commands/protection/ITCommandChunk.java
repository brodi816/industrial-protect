package net.industrial.collection.commands.protection;

import net.industrial.collection.commands.ITCommand;
import net.industrial.collection.commands.ITCommands;

/**
 * Created with IntelliJ IDEA.
 * User: Brodi
 * Date: 6/22/13
 * Time: 10:18 PM
 * To change this template use File | Settings | File Templates.
 */
public class ITCommandChunk extends ITCommand {
    public ITCommandChunk(ITCommands commands, String name, short rankNeeded) {
        super(commands, name, rankNeeded);

        arguments.add(new Argument("help", "Base command for chunk protection"));
    }
}
