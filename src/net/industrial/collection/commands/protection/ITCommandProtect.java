package net.industrial.collection.commands.protection;

import net.industrial.collection.ITTimedActionProtect;
import net.industrial.collection.commands.ITCommand;
import net.industrial.collection.commands.ITCommands;
import net.industrial.collection.player.ITPlayer;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;


/**
 * Created with IntelliJ IDEA.
 * User: brodi
 * Date: 6/10/13
 * Time: 11:19 AM
 * To change this template use File | Settings | File Templates.
 */
public class ITCommandProtect extends ITCommand {

    public ITCommandProtect(ITCommands commands, String name, short rankNeeded) {
        super(commands, name, rankNeeded);

        arguments.add(new Argument("help","Allow for you to collection blocks"));
    }

    @Override
    public void onPlayerUse(ITPlayer player, String[] args) {

        //see if we should stop protecting
        if(player.protectionMaterial != null) {
            player.protectionMaterial = null;
            player.getPlayer().sendMessage(ChatColor.GREEN + "No longer protecting blocks");
            return;
        }

        ItemStack item = player.getPlayer().getItemInHand();

        if(item == null || item.getType() == Material.AIR || item.getAmount() == 0) {
            player.getPlayer().sendMessage(ChatColor.RED + "No material in your hand");
            String materials = "";
            for(ItemStack cost : plugin.config.protectionCost) {
                materials += String.format("%d:%d*%d ",cost.getTypeId(), cost.getDurability(), cost.getAmount());
            }

            player.getPlayer().sendMessage(ChatColor.GRAY + "Valid materials: " + materials);
            return;
        }

        for(ItemStack cost : plugin.config.protectionCost) {
            if(item.isSimilar(cost)) {
                player.protectionMaterial = cost;
                player.setTimedAction(new ITTimedActionProtect(plugin, player));
                player.getPlayer().sendMessage(ChatColor.GREEN + "Protection material set to " + cost.getAmount() + " of " + cost.getType().toString() + " per block");
                return;
            }
        }

        player.getPlayer().sendMessage(ChatColor.RED + "No valid material for protecting in your hand");

        String materials = "";
        for(ItemStack cost : plugin.config.protectionCost) {
            materials += String.format("%d:%d*%d ",cost.getTypeId(), cost.getDurability(), cost.getAmount());
        }

        player.getPlayer().sendMessage(ChatColor.GRAY + "Valid materials: " + materials);
    }
}
