package net.industrial.collection.world;

import net.industrial.collection.sql.ITMySQLQueue;
import net.industrial.collection.sql.ITMySQLQueueItem;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: Brodi
 * Date: 6/9/13
 * Time: 4:25 AM
 * To change this template use File | Settings | File Templates.
 */
public class ITChunkLoader implements Runnable {
    private ITChunk chunk;
    private Connection conn;

    public ITChunkLoader(ITChunk chunk) {
        this.chunk = chunk;
    }

    @Override
    public void run() {
        if(conn == null) {
            conn = chunk.plugin.createConnection();
        }

        try {
            if(!conn.isValid(5)) {
                conn = chunk.plugin.createConnection();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }


        //try to fetch it from mysql
        PreparedStatement stmt = null;
        try {
            stmt = conn.prepareStatement("SELECT * FROM protectedchunks WHERE worldId=? AND x=? AND z=?");

            stmt.setInt(1, chunk.world.getSQLid());
            stmt.setInt(2, chunk.getChunk().getX());
            stmt.setInt(3, chunk.getChunk().getZ());

            ResultSet rs = stmt.executeQuery();

            //load the chunk data
            if(rs.next()) {
                chunk.loadFromDatabase(stmt.getConnection(), rs);
            }

            rs.close();

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            //close the statement
            try {
                stmt.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        ///
        //create it
        if(!chunk.isLoaded()) {
            chunk.plugin.addQueueItem(new ITMySQLQueueItem(chunk, ITMySQLQueue.QueueOperation.UPDATE));
        }

        //GC
        chunk = null;

        try {
            conn.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
