package net.industrial.collection.world;

import net.industrial.collection.ITPlugin;
import net.industrial.collection.sql.ITMySQLQueue;
import net.industrial.collection.sql.ITMySQLQueueItem;
import net.industrial.collection.sql.ITSQL;
import net.industrial.collection.player.ITPlayerData;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;

import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ITWorld implements ITSQL {
    public final ITPlugin plugin;
	public final World world;

    private Date addDate;

    private List<DisabledItem> disabledItems = new ArrayList<DisabledItem>();

    private int SQLid = -1;
    private boolean invalidated = true;

    public class DisabledItem implements ITSQL {
        public final ITWorld world;
        private int SQLid = -1;
        private short type, data;
        private boolean invalidated = true;

        public DisabledItem(ITWorld world, short type, short data) {
            this.world = world;
            this.type = type;
            this.data = data;
        }

        public DisabledItem(ITWorld world, short type, short data, int SQLid) {
            this.world = world;
            this.type = type;
            this.data = data;
            this.SQLid = SQLid;
            invalidated = false;
        }

        public synchronized short getType() {
            return  type;
        }

        public synchronized short getData() {
            return data;
        }

        public synchronized void setType(short type) {
            this.type = type;
            invalidated = true;
        }

        public synchronized void setData(short data) {
            this.data = data;
            invalidated = true;
        }

        @Override
        public void loadFromDatabase(Connection conn, ResultSet rs) {
            try {
                type = rs.getShort("type");
                data = rs.getShort("data");
            } catch(Exception ex) {
                ex.printStackTrace();
            }
        }

        @Override
        public synchronized void deleteFromDatabase(Connection conn) {
            if(SQLid == -1) {
                return;
            }

            PreparedStatement stmt = null;
            try {
                stmt = conn.prepareStatement("DELETE FROM worldsdisableditems WHERE id=?", Statement.NO_GENERATED_KEYS);
                stmt.setInt(1, SQLid);

                stmt.executeUpdate();
            } catch(Exception ex) {
                ex.printStackTrace();
            } finally {
                //try to close the statement
                try {
                    stmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        @Override
        public synchronized void updateDatabase(Connection conn) {

            //insert
            if(SQLid == -1) {
                PreparedStatement stmt = null;
                try {
                    stmt = conn.prepareStatement("INSERT INTO worldsdisableditems(worldId,type,data) VALUES(?,?,?)", Statement.RETURN_GENERATED_KEYS);
                    stmt.setInt(1, world.getSQLid());
                    stmt.setShort(2, type);
                    stmt.setShort(3, data);

                    stmt.executeUpdate();

                    ResultSet rsKeys = stmt.getGeneratedKeys();
                    rsKeys.next();
                    SQLid = rsKeys.getInt(1);

                } catch(Exception ex) {
                    ex.printStackTrace();
                } finally {
                    //try to close the statement
                    try {
                        stmt.close();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }
            //update
            else if(invalidated) {
                PreparedStatement stmt = null;
                try {
                    stmt = conn.prepareStatement("UPDATE worldsdisableditems SET type=?,data=? WHERE id=?");

                    stmt.setShort(1, (short)type);
                    stmt.setShort(2, data);
                    stmt.setInt(3, SQLid);

                    stmt.executeUpdate();
                } catch (Exception ex) {
                    ex.printStackTrace();
                } finally {
                    //try to close the statement
                    try {
                        stmt.close();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            invalidated = false;
        }

        @Override
        public void invalidate() {
            invalidated = true;
        }

        @Override
        public boolean isInvalidated() {
            return invalidated;
        }

        @Override
        public int getSQLid() {
            return SQLid;
        }
    }

    public ITWorld(ITPlugin plugin, World world) {
        this.plugin = plugin;
        this.world = world;

        setWorld(this);
    }

    public boolean canInteract(ITPlayerData playerData, Block block) {
        ITChunk chunk = ITChunk.getChunk(block.getChunk());
        if(chunk != null) {
            return chunk.canInteract(playerData, block);
        }
        return false;
    }

    public boolean canEdit(ITPlayerData playerData, Block block) {
        ITChunk chunk = ITChunk.getChunk(block.getChunk());
        if(chunk != null) {
            return chunk.canEdit(playerData, block);
        }
        return false;
    }

    public synchronized void addDisabledItem(short type, short data) {
        DisabledItem item = new DisabledItem(this, type, data);
        this.disabledItems.add(item);
        plugin.addQueueItem(new ITMySQLQueueItem(item, ITMySQLQueue.QueueOperation.UPDATE));
    }

    public synchronized boolean itemDisabled(short type, short data) {
        for(DisabledItem item : disabledItems) {
            if(item.getType() == type && item.getData() == data) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void loadFromDatabase(Connection conn, ResultSet rs) {
        try {
            SQLid = rs.getInt("id");
            addDate = rs.getDate("addDate");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        invalidated = false;
    }

    @Override
    public synchronized void deleteFromDatabase(Connection conn) {
        //To change body of implemented methods use File | Settings | File Templates.
        ///TODO Implement deletion

        if(SQLid != -1) {
            PreparedStatement stmt = null;
            try {
                //delete the world
                stmt = conn.prepareStatement("DELETE FROM worlds WHERE id=?", Statement.NO_GENERATED_KEYS);

                stmt.setInt(1, SQLid);

                stmt.executeUpdate();
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                //try to close the statement
                try {
                    stmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        throw new RuntimeException("Not implemented");
    }

    @Override
    public synchronized void updateDatabase(Connection conn) {
        //insert
        if(SQLid == -1) {
            PreparedStatement stmt = null;
            try {
                addDate = new Date();
                stmt = conn.prepareStatement("INSERT INTO worlds(name, seed, addDate) VALUES(?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
                stmt.setString(1, world.getName());
                stmt.setLong(2, world.getSeed());
                stmt.setTimestamp(3, new Timestamp(addDate.getTime()));


                stmt.executeUpdate();

                ResultSet rsKeys = stmt.getGeneratedKeys();
                rsKeys.next();
                SQLid = rsKeys.getInt(1);

                plugin.logInfo("MySQL id for " + world.getName() + " is " + SQLid);

            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                //try to close the statement to free resources
                try {
                    stmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
        //update
        else if(invalidated) {
            PreparedStatement stmt = null;
            try {
                stmt = conn.prepareStatement("UPDATE worlds SET seed=? WHERE id=?", Statement.NO_GENERATED_KEYS);

                stmt.setLong(1, world.getSeed());
                stmt.setInt(2, SQLid);

                stmt.executeUpdate();
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                //try to close the statement to free resources
                try {
                    stmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        //update disabled items
        for(DisabledItem item : disabledItems) {
            item.updateDatabase(conn);
        }

        invalidated = false;
    }



    @Override
    public void invalidate() {
        invalidated = true;
    }

    @Override
    public boolean isInvalidated() {
        return invalidated;
    }

    @Override
    public int getSQLid() {
        return SQLid;
    }

    public void loadDisabledItems(Connection conn) {
        PreparedStatement stmt = null;
        try {
            stmt = conn.prepareStatement("SELECT id,type,data FROM worldsdisableditems WHERE worldId=?");

            stmt.setInt(1, SQLid);

            ResultSet rs = stmt.executeQuery();

            while(rs.next()) {
                DisabledItem disabledItem = new DisabledItem(this,
                        rs.getShort("type"), rs.getShort("data"), rs.getInt("id"));

                disabledItems.add(disabledItem);
            }

            rs.close();

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            //try to close the statement
            try {
                stmt.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public static boolean worldExists(ITPlugin plugin, String name) {
        boolean found = false;

        PreparedStatement stmt = null;
        //try to fetch the id from the db
        try {
            stmt = plugin.getConnection().prepareStatement("SELECT id FROM worlds WHERE name=?");
            stmt.setString(1, name);

            ResultSet rs = stmt.executeQuery();

            if(rs.next()) {
                found = true;
            }
            rs.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            //try to close the statement
            try {
                stmt.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return found;
    }

    public static ITWorld loadWorld(ITPlugin plugin, World world) {
        if(world.getMetadata("ITWorld").size() > 0) {
            return ITWorld.getWorld(plugin, world);
        }

        ITWorld itWorld = new ITWorld(plugin, world);

        if(worldExists(plugin, world.getName())) {
            PreparedStatement stmt = null;
            //try to fetch all information about the world from the db
            try {
                stmt = plugin.getConnection().prepareStatement("SELECT * FROM worlds WHERE name=?");
                stmt.setString(1, world.getName());

                ResultSet rs = stmt.executeQuery();
                rs.next();

                //load the world data
                itWorld.loadFromDatabase(stmt.getConnection(), rs);
                //load disabled items
                itWorld.loadDisabledItems(plugin.getConnection());

            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                //try to close the statement
                try {
                    stmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        } else {
            //update to create SQLid
            itWorld.updateDatabase(plugin.getConnection());
        }

        return itWorld;
    }

    public static ITWorld getWorld(ITPlugin plugin, World world) {
        List<MetadataValue> itWorlds = world.getMetadata("ITWorld");
        if(itWorlds.size() > 0) {
            return (ITWorld) itWorlds.get(0).value();
        }
        else {
            plugin.logInfo("Loaded world: " + world.getName() + " which was not initialised");
            return ITWorld.loadWorld(plugin, world);
        }
    }

    public static void setWorld(ITWorld world) {
        world.world.setMetadata("ITWorld", new FixedMetadataValue(world.plugin, world));
    }

    public static void removeWorld(ITWorld world) {
        world.world.removeMetadata("ITWorld", world.plugin);
    }
}
