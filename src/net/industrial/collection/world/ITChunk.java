package net.industrial.collection.world;

import net.industrial.collection.ITPlugin;
import net.industrial.collection.protection.ITProtectionChunkGroup;
import net.industrial.collection.protection.ITProtectionItem;
import net.industrial.collection.relationships.ITGroup;
import net.industrial.collection.relationships.ITPlayerFriend;
import net.industrial.collection.sql.ITMySQLQueue;
import net.industrial.collection.sql.ITMySQLQueueItem;
import net.industrial.collection.sql.ITSQL;
import net.industrial.collection.player.ITPlayerData;
import net.industrial.collection.protection.ITProtectedBlock;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;

import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Brodi
 * Date: 6/7/13
 * Time: 12:44 PM
 * To change this template use File | Settings | File Templates.
 */
public class ITChunk extends ITProtectionItem {

    public class Flag {
        public final String name;
        protected int SQLid = -1;
        boolean canPublic = false;
        short friendRank = 1;
        short groupRank = 1;

        public Flag(String name, boolean canPublic, short friendRank, short groupRank) {
            this.name = name;
            this.canPublic = canPublic;
            this.friendRank = friendRank;
            this.groupRank = groupRank;
        }

        public Flag(String name, boolean canPublic, short friendRank) {
            this.name = name;
            this.canPublic = canPublic;
            this.friendRank = friendRank;
        }

        public Flag(String name, boolean canPublic) {
            this.name = name;
            this.canPublic = canPublic;
        }

        public Flag(String name) {
            this.name = name;
        }
    }

    public static final String[] flagNames = new String[]{"binteract", "bedit"};

    public final ITPlugin plugin;
    public final ITWorld world;
    public final int chunkX, chunkZ;
    private Chunk chunk;
    private List<ITProtectedBlock> protectedBlocks = new ArrayList<>();
    private List<ITProtectionChunkGroup> groups = new ArrayList<>();
    private HashMap<String, Flag> flags = new HashMap<>();
    private boolean isLoaded = false;

    private String name;

    private ITPlayerData owner = null;
    private Date protectionDate = null;

    public ITChunk(ITWorld world, Chunk chunk) {
        this.plugin = world.plugin;
        this.world = world;
        this.chunk = chunk;

        this.chunkX = chunk.getX();
        this.chunkZ = chunk.getZ();

        this.name = "Chunk(" + chunkX + "," + chunkZ + ")";

        flags.put("binteract", new Flag("binteract"));
        flags.put("bedit", new Flag("bedit"));

        setChunk(this);
    }

    public boolean isLoaded() {
        return isLoaded;
    }

    public synchronized void addProtectedBlock(ITProtectedBlock block) {
        protectedBlocks.add(block);
        plugin.addQueueItem(new ITMySQLQueueItem(block, ITMySQLQueue.QueueOperation.UPDATE));
    }

    public synchronized void removeProtectedBlock(ITProtectedBlock block) {
        protectedBlocks.remove(block);
        plugin.addQueueItem(new ITMySQLQueueItem(block, ITMySQLQueue.QueueOperation.DELETE));
    }

    public ITProtectedBlock getProtectedBlock(int x, short y, int z) {
        for(ITProtectedBlock pb : protectedBlocks) {
            if(pb.x == x && pb.y == y && pb.z == z) {
                return pb;
            }
        }
        return null;
    }

    public boolean canInteract(ITPlayerData playerData, Block block) {
        if(isLoaded) {
            Flag flag = flags.get("binteract");
            if(!flag.canPublic) {
                //check the chunk
                if(owner != null && playerData != owner) {
                    ITPlayerFriend friendship = owner.getFriend(playerData);
                    if(friendship == null) {
                        return false;
                    }
                    else if(friendship.getRank() < flag.friendRank) {
                        return false;
                    }
                }

                //Check groups
                if(groups.size() > 0) {
                    boolean inGroup = false;
                    for(ITProtectionChunkGroup group : groups) {
                        if(group.group.getMember(playerData) != null) {
                            inGroup = true;
                            break;
                        }
                    }
                    if(!inGroup) {
                        return false;
                    }
                }
            }
            //check for protected block
            Location loc = block.getLocation();
            int x = loc.getBlockX(), y = loc.getBlockY(), z = loc.getBlockZ();

            for(ITProtectedBlock pb : protectedBlocks) {
                if(pb.x == x && pb.y == y && pb.z == z) {
                    return pb.canInteract(playerData, block);
                }
            }
        } else {
            return false;
        }
        return true;
    }

    public boolean canEdit(ITPlayerData playerData, Block block) {
        if(isLoaded) {
            //check the chunk
            Flag flag = flags.get("bedit");
            if(!flag.canPublic) {
                //check the chunk
                if(owner != null && playerData != owner) {
                    ITPlayerFriend friendship = owner.getFriend(playerData);
                    if(friendship == null) {
                        return false;
                    }
                    else if(friendship.getRank() < flag.friendRank) {
                        return false;
                    }
                }

                //Check groups
                if(groups.size() > 0) {
                    boolean inGroup = false;
                    for(ITProtectionChunkGroup group : groups) {
                        if(group.group.getMember(playerData) != null) {
                            inGroup = true;
                            break;
                        }
                    }
                    if(!inGroup) {
                        return false;
                    }
                }
            }
            //check for the block
            Location loc = block.getLocation();
            int x = loc.getBlockX(), y = loc.getBlockY(), z = loc.getBlockZ();

            for(ITProtectedBlock pb : protectedBlocks) {
                if(pb.x == x && pb.y == y && pb.z == z) {
                    return pb.canEdit(playerData, block);
                }
            }
        } else {
            return false;
        }
        return true;
    }

    public synchronized String getName() {
        return name;
    }

    public synchronized void setName(String name) {
        this.name = name;
        invalidate();
        plugin.addQueueItem(new ITMySQLQueueItem(this, ITMySQLQueue.QueueOperation.UPDATE));
    }

    public synchronized ITPlayerData getOwner() {
        return owner;
    }

    public synchronized void setOwner(ITPlayerData owner) {
        //If no owner defined set the protection date
        if(this.owner == null) {
            this.protectionDate = new Date();
        } else if(owner == null) {
            this.protectionDate = null;
        }

        this.owner = owner;
        invalidate();

        plugin.addQueueItem(new ITMySQLQueueItem(this, ITMySQLQueue.QueueOperation.UPDATE));
    }

    public synchronized Date getProtectionDate() {
        return protectionDate;
    }

    @Override
    public void loadFromDatabase(Connection conn, ResultSet rs) {
        try {
            this.SQLid = rs.getInt("id");
            this.name = rs.getString("name");

            this.owner = plugin.getPlayerData(rs.getInt("ownerId"));
            this.protectionDate = rs.getDate("protectedSince");

        } catch (Exception ex) {
            ex.printStackTrace();
            plugin.logSevere("ERROR LOADING CHUNK(" + chunkX + "," + chunkZ +") FROM MYSQL, VERY BAD!");
            return;
        }
        //load the flags
        loadFlags(conn);
        //load the groups
        loadGroups(conn);
        //load the blocks
        loadProtectedBlocks(conn);

        isLoaded = true;
        invalidated = false;
    }

    @Override
    public void deleteFromDatabase(Connection conn) {
        throw new RuntimeException("Delete not allowed on chunk");
    }

    @Override
    public synchronized void updateDatabase(Connection conn) {
        //insert a new entry
        if(SQLid == -1) {
            PreparedStatement stmt = null;
            try {
                stmt = conn.prepareStatement("INSERT INTO protectedchunks(name,ownerId,worldId,x,z,protectedSince) VALUES(?,?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);

                stmt.setString(1, name);
                stmt.setInt(3, world.getSQLid());
                stmt.setInt(4, chunkX);
                stmt.setInt(5, chunkZ);

                //see if this chunk is protected
                if(owner != null) {
                    stmt.setInt(2, owner.getSQLid());
                    stmt.setTimestamp(6, new Timestamp(protectionDate.getTime()));
                } else {
                    stmt.setNull(2, Types.NULL);
                    stmt.setNull(6, Types.NULL);
                }

                //execute the statement and fetch the id generated by it
                stmt.executeUpdate();

                ResultSet rs = stmt.getGeneratedKeys();
                rs.next();
                this.SQLid = rs.getInt(1);
                rs.close();

            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                //try to close the statement
                try {
                    stmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        } else if(invalidated) { //make sure it's invalidated
            //update the entry
            PreparedStatement stmt = null;
            try {
                stmt = conn.prepareStatement("UPDATE protectedchunks SET name=?, ownerId=?, protectedSince=? WHERE id=?");
                stmt.setString(1, name);
                //see if it's protected
                if(owner != null) {
                    stmt.setInt(2, owner.getSQLid());
                    stmt.setTimestamp(3, new Timestamp(protectionDate.getTime()));
                } else {
                    stmt.setNull(2, Types.NULL);
                    stmt.setNull(3, Types.NULL);
                }

                stmt.setInt(4, SQLid);

                stmt.executeUpdate();
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                //try to close the statement
                try  {
                    stmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }


        //update flags
        updateFlags(conn);

        //update groups
        for(ITProtectionChunkGroup group : groups) {
            group.updateDatabase(conn);
        }

        //update protected blocks
        for(ITProtectedBlock pb : protectedBlocks) {
           pb.updateDatabase(conn);
        }

        isLoaded = true;
        invalidated = false;
    }

    public void unload() {
        //plugin.logInfo("Unload Chunk");
        this.chunk = null;
        for(ITProtectedBlock pb : protectedBlocks) {
            pb.unload();
        }
    }

    public Chunk getChunk() {
        return this.chunk;
    }

    private void loadProtectedBlocks(Connection conn) {
        PreparedStatement stmt = null;
        try {
            stmt = conn.prepareStatement("SELECT * FROM protectedblocks WHERE chunkId=?");

            stmt.setInt(1, getSQLid());

            ResultSet rs = stmt.executeQuery();

            while(rs.next()) {
                ITProtectedBlock block = new ITProtectedBlock(this,
                        rs.getInt("x"), rs.getShort("y"), rs.getInt("z"), plugin.getPlayerData(rs.getInt("ownerId")));

                block.loadFromDatabase(conn, rs);

                //remove any duplicated protected blocks in the chunk
                while(true) {
                    ITProtectedBlock dp = this.getProtectedBlock(block.x, block.y, block.z);
                    if(dp != null) {
                        this.removeProtectedBlock(dp);
                    } else {
                        break;
                    }
                }

                protectedBlocks.add(block);
            }

            rs.close();

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            //close the statement
            try {
                stmt.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public void loadGroups(Connection conn) {
        PreparedStatement stmt = null;
        try {
            stmt = conn.prepareStatement("SELECT * FROM protectedchunksgroups WHERE protectedChunkId=?");

            stmt.setInt(1, SQLid);
            ResultSet rs = stmt.executeQuery();

            while(rs.next()) {
                ITProtectionChunkGroup group = new ITProtectionChunkGroup(this, plugin.getGroup(rs.getInt("groupId")));
                group.loadFromDatabase(conn, rs);
                groups.add(group);
            }

            rs.close();

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            //try to close the statement
            try {
                stmt.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public synchronized boolean setFlag(String flagName, boolean canPublic, short groupRank, short friendRank) {
        Flag flag = this.flags.get(flagName);
        if(flag == null) {
            return  false;
        }

        flag.canPublic = canPublic;
        flag.groupRank = groupRank;
        flag.friendRank = friendRank;

        plugin.addQueueItem(new ITMySQLQueueItem(this, ITMySQLQueue.QueueOperation.UPDATE));

        return true;
    }

    private void loadFlags(Connection conn) {
        PreparedStatement stmt = null;

        try {
            stmt = conn.prepareStatement("SELECT * FROM protectedchunksflags WHERE protectedChunkId=?");

            stmt.setInt(1, SQLid);

            ResultSet rs = stmt.executeQuery();

            while(rs.next()) {
                Flag flag = flags.get(rs.getString("flag"));

                flag.canPublic = rs.getBoolean("publicValue");
                flag.groupRank = rs.getShort("groupRank");
                flag.friendRank = rs.getShort("friendRank");
            }
            rs.close();

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            //try to close the statement
            try {
                stmt.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    private void updateFlags(Connection conn) {
        PreparedStatement insertStmt = null;
        PreparedStatement updateStmt = null;
        try {
            insertStmt = conn.prepareStatement("INSERT INTO protectedchunksflags(protectedChunkId,flag,publicValue,groupRank,friendRank) VALUES(?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
            updateStmt = conn.prepareStatement("UPDATE protectedchunksflags SET publicValue=?,groupRank=?,friendRank=? WHERE id=?", Statement.NO_GENERATED_KEYS);
            for(Flag f : flags.values()) {
                //insert
                if(f.SQLid == -1) {
                    insertStmt.setInt(1,SQLid);
                    insertStmt.setString(2,f.name);
                    insertStmt.setBoolean(3,f.canPublic);
                    insertStmt.setShort(4,f.groupRank);
                    insertStmt.setShort(5,f.friendRank);

                    insertStmt.executeUpdate();

                    //get the id generated by the DB
                    ResultSet rs = insertStmt.getGeneratedKeys();
                    rs.next();
                    f.SQLid = rs.getInt(1);
                    rs.close();
                }
                //update
                else {
                    updateStmt.setBoolean(1,f.canPublic);
                    updateStmt.setShort(2, f.groupRank);
                    updateStmt.setShort(3, f.friendRank);
                    updateStmt.setInt(4, f.SQLid);

                    updateStmt.executeUpdate();

                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            //try to close the open statements
            try {
                insertStmt.close();
                updateStmt.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public static ITChunk loadChunk(ITWorld world, Chunk chunk) {
        if(chunkExists(chunk))
            return getChunk(chunk);

        ITChunk iTchunk = new ITChunk(world, chunk);

        //load the chunk async
        world.plugin.getServer().getScheduler().runTaskAsynchronously(world.plugin, new ITChunkLoader(iTchunk));

        return iTchunk;
    }

    public static ITChunk getChunk(Chunk chunk) {
        List<MetadataValue> values = chunk.getBlock(0,0,0).getMetadata("ITChunk");
        if(values.size() == 0)
            return null;

        return (ITChunk) values.get(0).value();
    }

    public static void setChunk(ITChunk chunk) {
        chunk.chunk.getBlock(0, 0, 0).setMetadata("ITChunk", new FixedMetadataValue(chunk.plugin, chunk));
    }

    public static void removeChunk(ITChunk chunk) {
        chunk.chunk.getBlock(0,0,0).removeMetadata("ITChunk", chunk.plugin);
    }

    public static boolean chunkExists(Chunk chunk) {
        return chunk.getBlock(0,0,0).hasMetadata("ITChunk");
    }

}
