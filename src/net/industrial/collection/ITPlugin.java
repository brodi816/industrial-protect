package net.industrial.collection;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Connection;
import java.util.Collection;
import java.util.HashMap;
import java.util.logging.Logger;

import net.industrial.collection.commands.ITCommands;
import net.industrial.collection.listeners.ITBlockListener;
import net.industrial.collection.listeners.ITEntityListener;
import net.industrial.collection.listeners.ITPlayerListener;
import net.industrial.collection.listeners.ITWorldListener;
import net.industrial.collection.mp3.ITMp3Cache;
import net.industrial.collection.mp3.ITPlayedSaySound;
import net.industrial.collection.mp3.ITSaySound;
import net.industrial.collection.mp3.ITSaySounds;
import net.industrial.collection.permissions.ITPermissions;
import net.industrial.collection.relationships.ITGroup;
import net.industrial.collection.sql.ITMySQLKeepAlive;
import net.industrial.collection.sql.ITMySQLQueue;
import net.industrial.collection.sql.ITMySQLQueueItem;
import net.industrial.collection.world.ITChunk;
import net.industrial.collection.world.ITWorld;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.world.WorldInitEvent;
import org.bukkit.plugin.java.JavaPlugin;

import net.industrial.collection.player.*;

public class ITPlugin extends JavaPlugin {

	private Connection sqlCon;
    public ITPluginConfig config;
	
	private HashMap<String, ITPlayerData> playerData = null;
    private HashMap<String, ITGroup> groups = null;

    private ITServerTick itServerTick = new ITServerTick(this);
    private ITMySQLKeepAlive itMySQLKeepAlive = new ITMySQLKeepAlive(this);
    private ITMySQLQueue itMySQLQueue;

    private ITPlayerListener playerListener;
    private ITWorldListener worldListener;
    private ITEntityListener entityListener;
    private ITBlockListener blockListener;

    public final ITCommands commands = new ITCommands(this);
    public final ITPermissions permissions = new ITPermissions(this);
    private ITMp3Cache mp3Cache;
    public final ITSaySounds saySounds = new ITSaySounds(this);


    public String logHeader() { return "[IT(Early Dev)] "; }
    public void logInfo(String message) { getLogger().info(logHeader() + message); }
    public void logSevere(String message) { getLogger().severe(logHeader() + message); }
    public void logWarning(String message) { getLogger().warning(logHeader() + message); }
    public void logFine(String message) { getLogger().fine(logHeader() + message); }
    public void logFiner(String message) { getLogger().finer(logHeader() + message); }
    public void logFinest(String message) { getLogger().finest(logHeader() + message); }

    public ITPlayerListener getPlayerListener() {
        return playerListener;
    }

    public ITWorldListener getWorldListener() {
        return worldListener;
    }

    public ITEntityListener getEntityListener() {
        return entityListener;
    }

    public ITBlockListener getBlockListener() {
        return blockListener;
    }

    public void addQueueItem(ITMySQLQueueItem item) {
        itMySQLQueue.queue.add(item);
    }

    public int getServerTicks() {
        synchronized (itServerTick.ticks) {
            return itServerTick.ticks;
        }
    }

    public ITMp3Cache getMp3Cache() {
        return mp3Cache;
    }

    public void addSaySoundToPlay(ITPlayedSaySound sound) {
        itServerTick.soundsToPlay.add(sound);
    }

    public ITPlayerData getOrCreatePlayerData(String name) {

        //return the playerdata if it exists
        ITPlayerData player = playerData.get(name.toLowerCase());

        if(player != null)
            return player;

        //create a new playerdata for the player
        player = ITPlayerData.getPlayerFromMySql(this, name);

        playerData.put(name.toLowerCase(), player);

        return player;
    }

    public Collection<ITPlayerData> getPlayerDataValues() {
        return playerData.values();
    }


	public ITPlayerData getPlayerData(String name) {
		return playerData.get(name.toLowerCase());
	}

    public ITPlayerData getPlayerData(int SQLid) {
        for(ITPlayerData player : playerData.values()) {
            if(player.getSQLid() == SQLid)
                return player;
        }
        return null;
    }
	
	public ITPlayerData getPlayerData(Player player) {
		return playerData.get(player.getName());
	}

    public boolean groupExists(String name) {
        return groups.containsKey(name.toLowerCase());
    }

    public ITGroup getGroup(String name) {
        return groups.get(name.toLowerCase());
    }

    public ITGroup getGroup(int SQLid) {
        for(ITGroup group : groups.values()) {
            if(group.getSQLid() == SQLid) {
                return group;
            }
        }

        return null;
    }

    public void setGroup(String name, ITGroup group) {
        groups.put(name.toLowerCase(), group);
    }

    public Connection getConnection() {
        return sqlCon;
    }
	
	@Override
	public void onEnable() {

        //construct
        mp3Cache = new ITMp3Cache(this);

        logInfo(this.getDataFolder().getAbsolutePath());

        config = new ITPluginConfig(this);

        config.setDefaults();
        config.loadConfig();

        Bukkit.getMessenger().registerOutgoingPluginChannel(this, "Aud");

        //try and connect then create db if we can
        try {
            //sqlCon = DriverManager.getConnection("jdbc:mysql://" + config.dbIp + ":" + config.dbPort + "/", config.dbUser, config.dbPass);

            //Statement stmt = sqlCon.createStatement();
            //stmt.executeUpdate("CREATE DATABASE IF NOT EXISTS " + config.dbName);
            //stmt.close();

            //sqlCon.close();
            sqlCon = createConnection();
        } catch(Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();

            this.logSevere("Failure to connect to DB, commencing emergency shutdown");
            getServer().shutdown();

            return;
        }

        //create the queue after crreating the main db connection
        itMySQLQueue = new ITMySQLQueue(this);

        //load stuff from db
        //load permisions
        permissions.loadPermissionsFromMySQL(sqlCon);
        //load player data
        playerData = ITPlayerData.getAllPlayerData(this);

        //load friends
        for(ITPlayerData data : playerData.values()) {
            data.loadFriends(sqlCon);
        }

        //load groups
        groups = ITGroup.loadGroups(this, sqlCon);

        //load cached mp3s
        mp3Cache.loadMp3s(sqlCon);

        //load say sounds
        saySounds.loadSaySounds(sqlCon);

        //register commands
        this.commands.addCommands();

        //create listeners
        playerListener = new ITPlayerListener(this);
        worldListener = new ITWorldListener(this);
        entityListener = new ITEntityListener(this);
        blockListener = new ITBlockListener(this);

        //register listeners
        this.getServer().getPluginManager().registerEvents(playerListener, this);
        this.getServer().getPluginManager().registerEvents(worldListener, this);
        this.getServer().getPluginManager().registerEvents(entityListener, this);
        this.getServer().getPluginManager().registerEvents(blockListener, this);

        //register timers
        this.getServer().getScheduler().scheduleSyncRepeatingTask(this, itMySQLKeepAlive, 20, 20);
        this.getServer().getScheduler().runTaskTimerAsynchronously(this, itMySQLQueue, 10, 10);
        this.getServer().getScheduler().runTaskTimer(this, itServerTick, 1, 1);



        //fake player logins for players already in the game
        for(Player player : getServer().getOnlinePlayers()) {
            if(player != null && player.isOnline()) {
                playerListener.onPlayerJoin(new PlayerJoinEvent(player, ""));
            }
        }
        //fake world inits
        for(World world : getServer().getWorlds()) {
            worldListener.onWorldInit(new WorldInitEvent(world));
        }

        logInfo("Plugin loaded successfully");
	}

    @Override
    public void onDisable() {


        //queue finish
        this.getServer().getScheduler().cancelAllTasks();

        //check connection
        sqlCon = getConnection();

        //save all player data
        this.logInfo("Saving all player data");
        for(ITPlayerData playerData : this.playerData.values()) {
            playerData.updateDatabase(sqlCon);
        }

        this.logInfo("Removing player meta data");

        for(Player player : getServer().getOnlinePlayers()) {
            ITPlayer itPlayer = ITPlayer.getPlayer(this, player);
            if(itPlayer != null) {
                itPlayer.onQuit(new PlayerQuitEvent(player, "Plugin disable"));
            }
        }

        this.logInfo("Remove world and chunks meta data");
        for(World world : getServer().getWorlds()) {
            for(Chunk chunk : world.getLoadedChunks()) {
                ITChunk itChunk = ITChunk.getChunk(chunk);
                if(itChunk != null) {
                    //save the chunk, then remove
                    itChunk.updateDatabase(sqlCon);
                    ITChunk.removeChunk(itChunk);
                }
            }

            ITWorld itWorld = ITWorld.getWorld(this, world);
            if(itWorld != null) {
                //save the world, then remove
                itWorld.updateDatabase(sqlCon);
                ITWorld.removeWorld(itWorld);
            }
        }

        this.logInfo("Letting mysql queue finish");
        this.itMySQLQueue.run();
    }

    public Connection createConnection() {
        try {
            return DriverManager.getConnection("jdbc:mysql://" + config.dbIp + ":" + config.dbPort + "/" + config.dbName + "?user=" + config.dbUser + "&password=" + config.dbPass);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
