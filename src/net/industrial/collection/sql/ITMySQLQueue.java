package net.industrial.collection.sql;

import net.industrial.collection.ITPlugin;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created with IntelliJ IDEA.
 * User: brodi
 * Date: 6/15/13
 * Time: 9:18 AM
 * To change this template use File | Settings | File Templates.
 */
public class ITMySQLQueue implements Runnable {
    public enum QueueOperation {
        UPDATE,
        DELETE
    }

    public final ITPlugin plugin;
    private boolean running;
    private Connection conn;
    public ConcurrentLinkedQueue<ITMySQLQueueItem> queue = new ConcurrentLinkedQueue<ITMySQLQueueItem>();

    public ITMySQLQueue(ITPlugin plugin) {
        this.plugin = plugin;
        try {
            this.conn = plugin.createConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        //make sure it's not already running
        if(running) {
            return;
        }
        running = true;

        //check connection
        try {
            conn.prepareStatement("SELECT 1").execute();
        } catch (Exception e) {
            e.printStackTrace();
            if(conn != null) {
                try {
                    conn.close();
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
            conn = plugin.createConnection();
        }

        //run until queue is empty
        while(queue.size() > 0) {
            ITMySQLQueueItem item = queue.poll();
            if(item != null) {
                //perform the operation specified
                switch (item.operation) {
                    case UPDATE:
                        item.item.updateDatabase(conn);
                        break;
                    case DELETE:
                        item.item.deleteFromDatabase(conn);
                        break;
                }
            }
        }

        running = false;
    }
}

