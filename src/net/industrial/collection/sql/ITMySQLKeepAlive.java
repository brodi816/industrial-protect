package net.industrial.collection.sql;

import net.industrial.collection.ITPlugin;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created with IntelliJ IDEA.
 * User: Brodi
 * Date: 6/10/13
 * Time: 4:20 AM
 * To change this template use File | Settings | File Templates.
 */
public class ITMySQLKeepAlive implements Runnable {
    public final ITPlugin plugin;

    public ITMySQLKeepAlive(ITPlugin plugin) {
        this.plugin = plugin;
    }

    @Override
    public void run() {
        try {
            plugin.getConnection().prepareStatement("SELECT 1").execute();
        } catch (SQLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }
}
