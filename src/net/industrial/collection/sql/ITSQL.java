package net.industrial.collection.sql;

import java.sql.Connection;
import java.sql.ResultSet;

/**
 * Created with IntelliJ IDEA.
 * User: Brodi
 * Date: 6/10/13
 * Time: 12:28 AM
 * To change this template use File | Settings | File Templates.
 */
public interface ITSQL {

    public void loadFromDatabase(Connection conn, ResultSet rs);
    public void deleteFromDatabase(Connection conn);
    public void updateDatabase(Connection conn);

    public void invalidate();
    public boolean isInvalidated();

    public int getSQLid();


}
