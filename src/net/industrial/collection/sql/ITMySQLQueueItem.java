package net.industrial.collection.sql;

public class ITMySQLQueueItem {
    public final ITSQL item;
    public final ITMySQLQueue.QueueOperation operation;

    public ITMySQLQueueItem(ITSQL item, ITMySQLQueue.QueueOperation operation) {
        this.item = item;
        this.operation = operation;
    }
}
