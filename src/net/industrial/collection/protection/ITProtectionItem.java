package net.industrial.collection.protection;

import net.industrial.collection.player.ITPlayerData;
import net.industrial.collection.sql.ITSQL;
import org.bukkit.block.Block;

/**
 * Created with IntelliJ IDEA.
 * User: Brodi
 * Date: 6/30/13
 * Time: 2:44 PM
 * To change this template use File | Settings | File Templates.
 */
public abstract class ITProtectionItem implements ITSQL {
    protected boolean invalidated = true;
    protected int SQLid = -1;

    public abstract boolean canInteract(ITPlayerData player, Block block);
    public abstract boolean canEdit(ITPlayerData player, Block block);

    @Override
    public void invalidate() {
        invalidated = true;
    }

    @Override
    public boolean isInvalidated() {
        return invalidated;
    }

    @Override
    public int getSQLid() {
        return SQLid;
    }
}

