package net.industrial.collection.protection;

import net.industrial.collection.relationships.ITGroup;
import net.industrial.collection.world.ITChunk;

/**
 * Created with IntelliJ IDEA.
 * User: Brodi
 * Date: 6/30/13
 * Time: 2:54 PM
 * To change this template use File | Settings | File Templates.
 */
public class ITProtectionChunkGroup extends ITProtectionGroup {
    public ITProtectionChunkGroup(ITChunk chunk, ITGroup group) {
        super("protectedchunksgroups", "protectedChunkId", chunk, group);
    }
}
