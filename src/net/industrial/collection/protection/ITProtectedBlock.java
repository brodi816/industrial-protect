package net.industrial.collection.protection;

import net.industrial.collection.ITPlugin;
import net.industrial.collection.sql.ITMySQLQueue;
import net.industrial.collection.sql.ITMySQLQueueItem;
import net.industrial.collection.sql.ITSQL;
import net.industrial.collection.player.ITPlayerData;
import net.industrial.collection.relationships.ITPlayerFriend;
import net.industrial.collection.world.ITChunk;
import org.bukkit.block.Block;

import java.sql.*;
import java.util.Date;
import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: Brodi
 * Date: 6/8/13
 * Time: 11:46 PM
 * To change this template use File | Settings | File Templates.
 */
public class ITProtectedBlock extends ITProtectionItem {

    class Flag {
        public final String name;
        protected int SQLid = -1;
        boolean canPublic = false;
        short friendRank = 1;
        short groupRank = 1;

        public Flag(String name, boolean canPublic, short friendRank, short groupRank) {
            this.name = name;
            this.canPublic = canPublic;
            this.friendRank = friendRank;
            this.groupRank = groupRank;
        }

        public Flag(String name, boolean canPublic, short friendRank) {
            this.name = name;
            this.canPublic = canPublic;
            this.friendRank = friendRank;
        }

        public Flag(String name, boolean canPublic) {
            this.name = name;
            this.canPublic = canPublic;
        }

        public Flag(String name) {
            this.name = name;
        }
    }

    public static final String[] flagNames = new String[]{"binteract", "bedit"};
    public final ITPlugin plugin;
    private ITChunk chunk;
    public final int x, z;
    public final short y;

    private ITPlayerData owner;
    private HashMap<String, Flag> flags = new HashMap<String, Flag>();

    public ITProtectedBlock(ITChunk chunk, int x, short y, int z, ITPlayerData owner) {
        this.plugin = chunk.plugin;
        this.chunk = chunk;
        this.x = x;
        this.y = y;
        this.z = z;
        this.owner = owner;

        flags.put("binteract", new Flag("binteract"));
        flags.put("bedit", new Flag("bedit"));
    }

    @Override
    public boolean canInteract(ITPlayerData playerData, Block block) {
        if(playerData == owner)
            return true;

        //see if the block is open for public editing
        if(flags.get("binteract").canPublic)
            return true;

        //see if there is a friendship and make sure there rank is high enough
        ITPlayerFriend friendship = owner.getFriend(playerData);
        if(friendship != null) {
            if(friendship.getRank() >= flags.get("binteract").friendRank) {
                return true;
            }
        }

        return false;
    }

    @Override
    public boolean canEdit(ITPlayerData playerData, Block block) {
        if(playerData == owner)
            return true;

        //see if the block is open for public editing
        if(flags.get("bedit").canPublic)
            return true;

        //see if there is a friendship and make sure there rank is high enough
        ITPlayerFriend friendship = owner.getFriend(playerData);
        if(friendship != null) {
            if(friendship.getRank() >= flags.get("bedit").friendRank) {
                return true;
            }
        }

        return false;
    }

    public void loadFromDatabase(Connection conn, ResultSet rs) {
        try {
            SQLid = rs.getInt("id");
            owner = plugin.getPlayerData(rs.getInt("ownerId"));
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        loadFlags(conn);
    }

    @Override
    public synchronized void deleteFromDatabase(Connection conn) {
        if(SQLid == -1)
            return;

        PreparedStatement deleteFlagsStmt = null;
        PreparedStatement deleteGroupsStmt = null;
        PreparedStatement deleteFriendsStmt = null;
        PreparedStatement deleteStmt = null;
        try {
            deleteFlagsStmt = conn.prepareStatement("DELETE FROM protectedblocksflags WHERE protectedBlockId=?");
            deleteGroupsStmt = conn.prepareStatement("DELETE FROM protectedblocksgroups WHERE protectedBlockId=?");
            deleteFriendsStmt = conn.prepareStatement("DELETE FROM protectedblocksfriends WHERE protectedBlockId=?");
            deleteStmt = conn.prepareStatement("DELETE FROM protectedblocks WHERE id=?");

            deleteFlagsStmt.setInt(1, SQLid);
            deleteGroupsStmt.setInt(1, SQLid);
            deleteFriendsStmt.setInt(1, SQLid);
            deleteStmt.setInt(1, SQLid);

            deleteFlagsStmt.executeUpdate();
            deleteGroupsStmt.executeUpdate();
            deleteFriendsStmt.executeUpdate();
            deleteStmt.executeUpdate();

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                deleteFlagsStmt.close();
                deleteGroupsStmt.close();
                deleteFriendsStmt.close();
                deleteStmt.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public synchronized void updateDatabase(Connection conn) {

        //insert
        if(SQLid == -1) {
            PreparedStatement stmt = null;
            try {
                stmt = conn.prepareStatement(
                        "INSERT INTO protectedblocks(ownerId, worldId, blockId, blockData, x, y, z, chunkId, protectedSince)" +
                                " VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);

                stmt.setInt(1, owner.getSQLid());
                stmt.setInt(2, chunk.world.getSQLid());
                stmt.setShort(3, (short)chunk.world.world.getBlockAt(x,y,z).getTypeId());
                stmt.setShort(4, chunk.world.world.getBlockAt(x,y,z).getData());
                stmt.setInt(5, x);
                stmt.setShort(6, y);
                stmt.setInt(7, z);
                stmt.setInt(8,chunk.getSQLid());
                stmt.setTimestamp(9,new Timestamp((new Date()).getTime()));

                stmt.executeUpdate();

                //get the generated id from the DB
                ResultSet rs = stmt.getGeneratedKeys();

                rs.next();
                SQLid = rs.getInt(1);
                rs.close();

            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                //try to close the statement
                try {
                    stmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

        }
        //update
        else if(invalidated) {
            PreparedStatement stmt = null;
            try {
                stmt = conn.prepareStatement("UPDATE protectedblocks SET ownerId=? WHERE id=?");

                stmt.setInt(1, owner.getSQLid());
                stmt.setInt(2, SQLid);

                stmt.executeUpdate();

            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                //try to close the statement
                try {
                    stmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        updateFlags(conn);
    }

    public void unload() {
        this.chunk = null;
    }

    public synchronized void loadFlags(Connection conn) {
        PreparedStatement stmt = null;
        try {
            stmt = conn.prepareStatement("SELECT * from protectedblocksflags WHERE protectedBlockId=?");
            stmt.setInt(1, SQLid);

            //load the flags
            ResultSet rs = stmt.executeQuery();

            while(rs.next()) {
                Flag f = flags.get(rs.getString("flag"));
                f.SQLid = rs.getInt("id");
                f.canPublic = rs.getBoolean("publicValue");
                f.groupRank = rs.getShort("groupRank");
                f.friendRank = rs.getShort("friendRank");
            }

            rs.close();

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            //close the statement
            try {
                stmt.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public synchronized void updateFlags(Connection conn) {
        PreparedStatement insertStmt = null;
        PreparedStatement updateStmt = null;
        try {
            insertStmt = conn.prepareStatement("INSERT INTO protectedblocksflags(protectedBlockId,flag,publicValue,groupRank,friendRank) VALUES(?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
            updateStmt = conn.prepareStatement("UPDATE protectedblocksflags SET publicValue=?,groupRank=?,friendRank=? WHERE id=?", Statement.NO_GENERATED_KEYS);
            for(Flag f : flags.values()) {
                //insert
                if(f.SQLid == -1) {
                    insertStmt.setInt(1,SQLid);
                    insertStmt.setString(2,f.name);
                    insertStmt.setBoolean(3,f.canPublic);
                    insertStmt.setShort(4,f.groupRank);
                    insertStmt.setShort(5,f.friendRank);

                    insertStmt.executeUpdate();

                    //get the id generated by the DB
                    ResultSet rs = insertStmt.getGeneratedKeys();
                    rs.next();
                    f.SQLid = rs.getInt(1);
                    rs.close();
                }
                //update
                else {
                    updateStmt.setBoolean(1,f.canPublic);
                    updateStmt.setShort(2, f.groupRank);
                    updateStmt.setShort(3, f.friendRank);
                    updateStmt.setInt(4, f.SQLid);

                    updateStmt.executeUpdate();

                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            //try to close the open statements
            try {
                insertStmt.close();
                updateStmt.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public synchronized ITPlayerData getOwner() {
        return this.owner;
    }

    public synchronized boolean setFlag(String flagName, boolean canPublic, short groupRank, short friendRank) {
        Flag flag = this.flags.get(flagName);
        if(flag == null) {
            return  false;
        }

        flag.canPublic = canPublic;
        flag.groupRank = groupRank;
        flag.friendRank = friendRank;

        plugin.addQueueItem(new ITMySQLQueueItem(this, ITMySQLQueue.QueueOperation.UPDATE));

        return true;
    }

    public static boolean flagExists(String flagName) {
        for(String f : flagNames) {
            if(f.equals(flagName)) {
                return true;
            }
        }
        return  false;
    }

}
