package net.industrial.collection.protection;

import net.industrial.collection.relationships.ITGroup;
import net.industrial.collection.sql.ITSQL;

import java.sql.*;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Brodi
 * Date: 6/30/13
 * Time: 1:05 AM
 * To change this template use File | Settings | File Templates.
 */
public class ITProtectionGroup implements ITSQL {
    public final String tableName, keyName;
    public final ITProtectionItem parentProtection;
    public final ITGroup group;
    private short rank;
    private int SQLid = -1;
    private boolean invalidated = true;
    private Date addDate = new Date();

    public ITProtectionGroup(String tableName, String keyName, ITProtectionItem parentProtection, ITGroup group) {
        this.tableName = tableName;
        this.keyName = keyName;
        this.parentProtection = parentProtection;
        this.group = group;
    }

    @Override
    public void loadFromDatabase(Connection conn, ResultSet rs) {
        try {
            SQLid = rs.getInt("id");
            rank = rs.getShort("rank");
            addDate = rs.getDate("addDate");
            invalidated = false;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void deleteFromDatabase(Connection conn) {
        if(SQLid == -1)
            throw new RuntimeException("Tried to delete protection group with no SQLid");


        PreparedStatement stmt = null;
        try {
            stmt = conn.prepareStatement("DELETE FROM ? WHERE id=?", Statement.NO_GENERATED_KEYS);
            stmt.setString(1, tableName);
            stmt.setInt(2, SQLid);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            //try to close the statement
            try {
                stmt.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void updateDatabase(Connection conn) {
        PreparedStatement stmt = null;
        try {
            if(SQLid == -1) {
                stmt = conn.prepareStatement("INSERT INTO ?(?,groupId,rank,addDate) VALUES(?,?,?,?)",
                        Statement.RETURN_GENERATED_KEYS);
                stmt.setString(1, tableName);
                stmt.setString(2, keyName);

                stmt.setInt(3, parentProtection.getSQLid());
                stmt.setInt(4, group.getSQLid());
                stmt.setShort(5, rank);
                stmt.setTimestamp(6, new Timestamp(addDate.getTime()));

                ResultSet rs = stmt.getGeneratedKeys();
                rs.next();
                SQLid = rs.getInt(1);
                rs.close();
            } else if(invalidated) {
                stmt = conn.prepareStatement("UPDATE ? SET rank=? WHERE id=?",
                        Statement.NO_GENERATED_KEYS);

                stmt.setString(1, tableName);
                stmt.setShort(2, rank);
                stmt.setInt(3, SQLid);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            //try to close the statement
            try {
                stmt.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void invalidate() {
        invalidated = true;
    }

    @Override
    public boolean isInvalidated() {
        return invalidated;
    }

    @Override
    public int getSQLid() {
        return SQLid;
    }
}
