package net.industrial.collection;

import org.bukkit.inventory.ItemStack;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Brodi
 * Date: 5/19/13
 * Time: 1:22 AM
 * To change this template use File | Settings | File Templates.
 */
public class ITPluginConfig {
    public final ITPlugin plugin;

    public String dbIp, dbPort, dbUser, dbPass, dbName;

    public String chatFormat;

    public int chunkLoadDataRadius;

    public List<Integer> protectedBlocks = new ArrayList<Integer>();

    public int playersUpdatedPerTick;

    public int maxPlayerDataSavesPerTick;
    public int maxPlayerDataChecksPerTick;

    public boolean protectAnyBlock;
    public List<ItemStack> protectionCost;

    public int limitedBlockRange;
    public List<ItemStack> limitedBlocks;

    public List<ItemStack> chunkCost;
    public int saySoundsTicksInBetween;
    public boolean saySoundsEnabled;

    public ITPluginConfig(ITPlugin plugin) {
        this.plugin = plugin;
    }

    public void loadConfig() {
        this.dbIp = plugin.getConfig().getString("db.ip");
        this.dbPort = plugin.getConfig().getString("db.port");
        this.dbUser = plugin.getConfig().getString("db.user");
        this.dbPass = plugin.getConfig().getString("db.pass");
        this.dbName = plugin.getConfig().getString("db.name");

        this.chatFormat = plugin.getConfig().getString("chat.format");

        this.chunkLoadDataRadius = plugin.getConfig().getInt("protection.chunkLoadDataRadius");
        this.protectedBlocks = (List<Integer>) plugin.getConfig().getList("protection.protectedBlocks");
        this.protectAnyBlock = plugin.getConfig().getBoolean("protection.anyBlock");
        this.protectionCost = loadItemStack(plugin.getConfig().getString("protection.cost"));
        this.chunkCost = loadItemStack(plugin.getConfig().getString("protection.chunkCost"));

        this.playersUpdatedPerTick = plugin.getConfig().getInt("player.playersUpdatedPerTick");

        this.maxPlayerDataSavesPerTick = plugin.getConfig().getInt("playerData.maxPlayerDataSavesPerTick");
        this.maxPlayerDataChecksPerTick = plugin.getConfig().getInt("playerData.maxPlayerDataChecksPerTick");

        this.limitedBlockRange = plugin.getConfig().getInt("limitedRange.range");
        this.limitedBlocks = loadItemStack(plugin.getConfig().getString("limitedRange.blocks"));

        this.saySoundsEnabled = plugin.getConfig().getBoolean("saySounds.enabled");
        this.saySoundsTicksInBetween = plugin.getConfig().getInt("saySounds.ticksInBetween");
    }

    private List<ItemStack> loadItemStack(String raw) {
        List<ItemStack> items = new ArrayList<ItemStack>();

        String[] itemsRaw = raw.split(" ");

        for(String itemRaw : itemsRaw) {

            int id = 1;
            short data = 0;
            int amount = 1;

            //format id:data,amt
            String[] itemDataAmount = itemRaw.split(",");

            if(itemDataAmount.length > 1) {
                amount = Integer.valueOf(itemDataAmount[1]);
            }

            String[] idData = itemDataAmount[0].split(":");

            id = Integer.valueOf(idData[0]);
            if(idData.length > 1) {
                data = Short.valueOf(idData[1]);
            }

            ItemStack item = new ItemStack(id, amount, data);

            items.add(item);
        }

        return items;
    }


    public void setDefaults() {
        plugin.getConfig().addDefault("db.ip", "127.0.0.1");
        plugin.getConfig().addDefault("db.port", "3306");
        plugin.getConfig().addDefault("db.user", "root");
        plugin.getConfig().addDefault("db.pass", "");
        plugin.getConfig().addDefault("db.name", "ITPlugin");

        plugin.getConfig().addDefault("chat.format", "<%player%> %message%");

        plugin.getConfig().addDefault("protection.chunkLoadDataRadius", 1);

        plugin.getConfig().addDefault("protection.protectedBlocks", Arrays.asList(new Integer[] {}));

        plugin.getConfig().addDefault("protection.anyBlock", true);
        plugin.getConfig().addDefault("protection.cost", "1:0,64");

        plugin.getConfig().addDefault("protection.chunkCost", "264:0,1");

        plugin.getConfig().addDefault("limitedRange.range", 32);
        plugin.getConfig().addDefault("limitedRange.blocks", "95:0,1");

        plugin.getConfig().addDefault("player.playersUpdatedPerTick", 8);

        plugin.getConfig().addDefault("playerData.maxPlayerDataSavesPerTick", 8);
        plugin.getConfig().addDefault("playerData.maxPlayerDataChecksPerTick", 64);

        plugin.getConfig().addDefault("saySounds.enabled", true);
        plugin.getConfig().addDefault("saySounds.ticksInBetween", 300);


        plugin.getConfig().options().copyDefaults(true);
        plugin.saveConfig();
    }

}
