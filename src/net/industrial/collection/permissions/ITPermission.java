package net.industrial.collection.permissions;

import net.industrial.collection.ITPlugin;
import net.industrial.collection.sql.ITMySQLQueue;
import net.industrial.collection.sql.ITMySQLQueueItem;
import net.industrial.collection.sql.ITSQL;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Created with IntelliJ IDEA.
 * User: Brodi
 * Date: 6/24/13
 * Time: 4:04 PM
 * To change this template use File | Settings | File Templates.
 */
public class ITPermission implements ITSQL {
    public final ITPlugin plugin;

    private String name;
    private short minRank, maxRank;

    private int SQLid = -1;
    private boolean invalidated = true;

    public ITPermission(ITPlugin plugin, String name, short minRank, short maxRank) {
        this.plugin = plugin;
        this.name = name;
        this.minRank = minRank;
        this.maxRank = maxRank;
    }

    public ITPermission(ITPlugin plugin, String name, short minRank, short maxRank, int SQLid) {
        this.plugin = plugin;
        this.name = name;
        this.minRank = minRank;
        this.maxRank = maxRank;
        this.SQLid = SQLid;
        this.invalidated = false;
    }

    public synchronized String getName() {
        return name;
    }

    public synchronized void setName(String name) {
        this.name = name;
        invalidate();
        plugin.addQueueItem(new ITMySQLQueueItem(this, ITMySQLQueue.QueueOperation.UPDATE));
    }

    public synchronized short getMinRank() {
        return minRank;
    }

    public synchronized void setMinRank(short minRank) {
        this.minRank = minRank;
        invalidate();
        plugin.addQueueItem(new ITMySQLQueueItem(this, ITMySQLQueue.QueueOperation.UPDATE));
    }

    public synchronized short getMaxRank() {
        return maxRank;
    }

    public synchronized void setMaxRank(short maxRank) {
        this.maxRank = maxRank;
        invalidate();
        plugin.addQueueItem(new ITMySQLQueueItem(this, ITMySQLQueue.QueueOperation.UPDATE));
    }

    @Override
    public void loadFromDatabase(Connection conn, ResultSet rs) {
        try {
            this.name = rs.getString("name");
            this.minRank = rs.getShort("minRank");
            this.maxRank = rs.getShort("maxRank");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        invalidated = false;
    }

    @Override
    public synchronized void deleteFromDatabase(Connection conn) {

        if(SQLid == -1) {
            throw new RuntimeException("Tried to delete a permission from the database that had no SQL id");
        }

        PreparedStatement stmt = null;
        try {
            stmt = conn.prepareStatement("DELETE FROM permissions WHERE id = ?");

            stmt.setInt(1, SQLid);

            stmt.executeUpdate();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            //try to close the statement
            try {
                stmt.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public synchronized void updateDatabase(Connection conn) {
        PreparedStatement stmt = null;
        try {
            //insert
            if(SQLid == -1) {
                stmt = conn.prepareStatement("INSERT INTO permissions(name, minRank, maxRank) VALUES(?,?,?)", Statement.RETURN_GENERATED_KEYS);

                stmt.setString(1, name);
                stmt.setShort(2, minRank);
                stmt.setShort(3, maxRank);

                stmt.executeUpdate();

                //fetch the generated id
                ResultSet rs = stmt.getGeneratedKeys();

                rs.next();
                this.SQLid = rs.getInt(1);
                rs.close();

            } else if(invalidated) { //update if invalidated
                stmt = conn.prepareStatement("UPDATE permissions SET name=?, minRank=?, maxRank=? WHERE id=?", Statement.NO_GENERATED_KEYS);

                stmt.setString(1, name);
                stmt.setShort(2, minRank);
                stmt.setShort(3, maxRank);

                stmt.executeUpdate();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            //try to close the statement
            try {
                stmt.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        invalidated = false;
    }

    @Override
    public synchronized void invalidate() {
        invalidated = true;
    }

    @Override
    public synchronized boolean isInvalidated() {
        return invalidated;
    }

    @Override
    public synchronized int getSQLid() {
        return SQLid;
    }
}
