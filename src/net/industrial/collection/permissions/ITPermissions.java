package net.industrial.collection.permissions;

import net.industrial.collection.ITPlugin;
import net.industrial.collection.player.ITPlayer;
import net.industrial.collection.sql.ITMySQLQueue;
import net.industrial.collection.sql.ITMySQLQueueItem;
import org.bukkit.entity.Player;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: Brodi
 * Date: 6/24/13
 * Time: 4:59 PM
 * To change this template use File | Settings | File Templates.
 */
public class ITPermissions {
    public final ITPlugin plugin;
    private List<ITPermission> permissions = new ArrayList<>();

    public ITPermissions(ITPlugin plugin) {
        this.plugin = plugin;
    }

    public int permissionCount() {
        return permissions.size();
    }

    public ITPermission[] getPermissions() {
        return permissions.toArray(new ITPermission[]{});
    }

    public ITPermission getPermission(int index) {
        return permissions.get(index);
    }

    public void deletePermission(int index) {
        ITPermission permission = permissions.get(index);
        permissions.remove(index);
        plugin.addQueueItem(new ITMySQLQueueItem(permission, ITMySQLQueue.QueueOperation.DELETE));


        recalculateAllPlayersPermissions();
    }

    public void clearAllPermissions(ITPlayer player) {
        Map<String, Boolean> permissions = player.permissions.getPermissions();
        Set<String> keys = permissions.keySet();

        for(String permission : keys) {
            player.permissions.unsetPermission(permission);
        }
    }

    public void addPermissionsToPlayer(ITPlayer player) {
        for(ITPermission permission : permissions) {
            //check ranking
            if(player.data.getRank() >= permission.getMinRank() && player.data.getRank() <= permission.getMaxRank()) {
                player.permissions.setPermission(permission.getName(), true);
            }
        }

        player.getPlayer().recalculatePermissions();
    }

    public void recalculateAllPlayersPermissions() {
        for(Player player : plugin.getServer().getOnlinePlayers()) {
            ITPlayer itPlayer = ITPlayer.getPlayer(plugin, player);
            clearAllPermissions(itPlayer);

            addPermissionsToPlayer(itPlayer);
        }
    }

    private void addPermissionToOnlinePlayers(ITPermission permission) {
        for(Player player : plugin.getServer().getOnlinePlayers()) {
            ITPlayer itPlayer = ITPlayer.getPlayer(plugin, player);

            //check ranking
            if(itPlayer.data.getRank() >= permission.getMinRank() && itPlayer.data.getRank() <= permission.getMaxRank()) {
                itPlayer.permissions.setPermission(permission.getName(), true);
            }

            player.recalculatePermissions();
        }
    }

    public void addPermission(ITPermission permission) {
        permissions.add(permission);
        addPermissionToOnlinePlayers(permission);
    }

    public void loadPermissionsFromMySQL(Connection conn) {
        PreparedStatement stmt = null;

        try {
            stmt = conn.prepareStatement("SELECT * FROM permissions");

            ResultSet rs =stmt.executeQuery();

            while(rs.next()) {
                ITPermission permission = new ITPermission(plugin,
                        rs.getString("name"),
                        rs.getShort("minRank"),
                        rs.getShort("maxRank"),
                        rs.getInt("id"));
                permissions.add(permission);
            }

            rs.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            //try to close the statement
            try {
                stmt.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
}
