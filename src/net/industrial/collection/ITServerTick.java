package net.industrial.collection;

import net.industrial.collection.mp3.ITPlayedSaySound;
import net.industrial.collection.packets.mp3.PacketHelper;
import net.industrial.collection.player.ITPlayer;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created with IntelliJ IDEA.
 * User: brodi
 * Date: 6/16/13
 * Time: 5:46 PM
 * To change this template use File | Settings | File Templates.
 */
public class ITServerTick implements Runnable {
    public final ITPlugin plugin;

    public final ConcurrentLinkedQueue<ITPlayedSaySound> soundsToPlay = new ConcurrentLinkedQueue<>();

    public int currentIndex = 0;
    public Integer ticks = 0;

    public ITServerTick(ITPlugin plugin) {
        this.plugin = plugin;
    }

    @Override
    public void run() {
        synchronized (ticks) {
            ticks++;
        }

        //update the players for this tick
        Player players[] = plugin.getServer().getOnlinePlayers();
        for(int i = 0; i < plugin.config.playersUpdatedPerTick; i++) {
            if(currentIndex >= players.length) {
                currentIndex = 0;
            } else {
                ITPlayer.getPlayer(plugin, players[currentIndex]).update();
                currentIndex++;
            }
        }

        //check for sounds to play
        while(soundsToPlay.size() > 0) {
            ITPlayedSaySound sound = soundsToPlay.poll();
            plugin.getServer().broadcastMessage(sound.player.getDisplayName() + " played: " + ChatColor.GREEN + sound.sound.getName());
            //send message to all players
            for(Player player : players) {
                sound.sound.playSound(ITPlayer.getPlayer(plugin, player));
            }
        }
    }
}
