package net.industrial.collection;

import net.industrial.collection.player.ITPlayer;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEvent;

/**
 * Created with IntelliJ IDEA.
 * User: brodi
 * Date: 6/16/13
 * Time: 3:57 PM
 * To change this template use File | Settings | File Templates.
 */
public abstract class ITTimedAction {
    public final ITPlugin plugin;
    public final ITPlayer player;
    public int endTick;

    public ITTimedAction(ITPlugin plugin, ITPlayer player) {
        this.plugin = plugin;
        this.player = player;
    }

    public abstract void actionEnd();

    public abstract void onPlayerInteraction(PlayerInteractEvent event);
    public abstract void onBlockPlace(BlockPlaceEvent event);
    public abstract void onBlockBreak(BlockBreakEvent event);
}
