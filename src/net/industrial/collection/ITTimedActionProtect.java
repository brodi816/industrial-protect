package net.industrial.collection;

import net.industrial.collection.player.ITPlayer;
import net.industrial.collection.protection.ITProtectedBlock;
import net.industrial.collection.world.ITChunk;
import org.bukkit.ChatColor;
import org.bukkit.block.Block;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEvent;

/**
 * Created with IntelliJ IDEA.
 * User: brodi
 * Date: 6/16/13
 * Time: 6:57 PM
 * To change this template use File | Settings | File Templates.
 */
public class ITTimedActionProtect extends ITTimedAction {
    public static int length = 20 * 30;

    public ITTimedActionProtect(ITPlugin plugin, ITPlayer player) {
        super(plugin, player);
        this.endTick = plugin.getServerTicks() + length;
    }

    @Override
    public void actionEnd() {
        if(player.protectionMaterial != null) {
            player.getPlayer().sendMessage(ChatColor.GRAY + "No longer protecting blocks");
            player.protectionMaterial = null;
        }
    }

    @Override
    public void onPlayerInteraction(PlayerInteractEvent event) {
        if(player.protectionMaterial != null && event.getAction() == Action.LEFT_CLICK_BLOCK) {
            Block block = event.getClickedBlock();
            ITChunk chunk = ITChunk.getChunk(block.getLocation().getChunk());

            //make sure the block isn't already protected
            if(chunk.getProtectedBlock(block.getX(),(short)block.getY(),block.getZ()) == null) {
                int amount = InventoryHelper.getAmount(player.protectionMaterial, player.getPlayer().getInventory().getContents());

                if(amount >= player.protectionMaterial.getAmount()) {
                    //collection the block
                    chunk.addProtectedBlock(new ITProtectedBlock(chunk, block.getX(),
                            (short)block.getY(),
                            block.getZ(),
                            player.data));
                    //apply the cost
                    player.getPlayer().getInventory().removeItem(player.protectionMaterial);
                    player.getPlayer().sendMessage(ChatColor.GREEN + "Protected block");
                } else {
                    player.getPlayer().sendMessage(ChatColor.RED + "Not enough of item to collection");
                }
            } else {
                player.getPlayer().sendMessage(ChatColor.RED + "Block already protected");
            }

            this.endTick = plugin.getServerTicks() + length;
        }
    }

    @Override
    public void onBlockPlace(BlockPlaceEvent event) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void onBlockBreak(BlockBreakEvent event) {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
