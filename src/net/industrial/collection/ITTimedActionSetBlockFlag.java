package net.industrial.collection;

import net.industrial.collection.player.ITPlayer;
import net.industrial.collection.protection.ITProtectedBlock;
import net.industrial.collection.world.ITChunk;
import org.bukkit.ChatColor;
import org.bukkit.block.Block;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEvent;

/**
 * Created with IntelliJ IDEA.
 * User: brodi
 * Date: 6/17/13
 * Time: 9:02 AM
 * To change this template use File | Settings | File Templates.
 */
public class ITTimedActionSetBlockFlag extends ITTimedAction {
    public final String flagName;
    public boolean canPublic;
    public final short groupRank, friendRank;

    public ITTimedActionSetBlockFlag(ITPlugin plugin, ITPlayer player, String flagName, boolean canPublic, short groupRank, short friendRank) {
        super(plugin, player);

        this.flagName = flagName;
        this.canPublic = canPublic;
        this.groupRank = groupRank;
        this.friendRank = friendRank;

        this.endTick = plugin.getServerTicks() + 30 * 20;
    }

    @Override
    public void actionEnd() {
        player.getPlayer().sendMessage("No longer setting block flag: " + flagName + " to clicked blocks");
    }

    @Override
    public void onPlayerInteraction(PlayerInteractEvent event) {
        if(event.getAction() == Action.LEFT_CLICK_BLOCK) {
            Block block = event.getClickedBlock();
            ITChunk chunk = ITChunk.getChunk(block.getChunk());
            ITProtectedBlock protectedBlock = chunk.getProtectedBlock(block.getX(), (short)block.getY(), block.getZ());

            if(protectedBlock == null) {
                player.getPlayer().sendMessage(ChatColor.DARK_RED + "Block is not protected");
                return;
            }

            if(protectedBlock.getOwner() != player.data) {
                player.getPlayer().sendMessage(ChatColor.DARK_RED + "You are not the owner of this block");
                return;
            }

            if(protectedBlock.setFlag(flagName, canPublic, groupRank, friendRank)) {
                player.getPlayer().sendMessage(ChatColor.DARK_RED + "Changed block flag: " + flagName);

                this.endTick = plugin.getServerTicks() + 30 * 20;
            } else {
                player.getPlayer().sendMessage(ChatColor.DARK_RED + "Failed to set flag: " + flagName);
            }
        }
    }

    @Override
    public void onBlockPlace(BlockPlaceEvent event) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void onBlockBreak(BlockBreakEvent event) {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
