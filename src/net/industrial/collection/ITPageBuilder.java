package net.industrial.collection;

import net.industrial.collection.player.ITPlayer;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: brodi
 * Date: 6/13/13
 * Time: 2:54 AM
 * To change this template use File | Settings | File Templates.
 */
public class ITPageBuilder {
    public List<String> lines = new ArrayList<String>();

    public ITPageBuilder() {
    }

    public ITPageBuilder(String[] lines) {
        for(String s : lines) {
            this.lines.add(s);
        }
    }

    public String[] getPage(int page, int amountPerPage) {
        page = Math.max(page, 1);

        List<String> pageLines = new ArrayList<String>();

        //put the lines into the pageString
        for(int pageIndex = 0; pageIndex < amountPerPage; pageIndex++) {
            //get the index of the page in the global lines
            int linesIndex = pageIndex + ((page-1)*amountPerPage);
            if(linesIndex >= lines.size())
                break;

            pageLines.add(lines.get(linesIndex));
        }

        return pageLines.toArray(new String[]{});
    }

    public void printPage(Player player, int page, int amountPerPage) {
        String[] pageContent = getPage(page, amountPerPage);

        player.sendMessage("" + ChatColor.YELLOW  + ChatColor.UNDERLINE+ String.format("Page: %d/%d", page, pageCount(amountPerPage)));

        for(String line : pageContent) {
            player.sendMessage(line);
        }
    }

    public int pageCount(int amountPerPage) {
        return (int)Math.ceil((double)lines.size() / (double)amountPerPage);
    }

    public static void printStringsToPlayer(ITPlayer player, String[] strings, String prefix, String suffix) {
        //loop through all the string and send them to the player each with the prefix and suffix
        for(String s : strings) {
            player.getPlayer().sendMessage(prefix + s + suffix);
        }
    }
}
