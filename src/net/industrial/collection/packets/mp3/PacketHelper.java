package net.industrial.collection.packets.mp3;

import net.industrial.collection.player.ITPlayer;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;

/**
 * Created with IntelliJ IDEA.
 * User: Brodi
 * Date: 7/9/13
 * Time: 10:12 PM
 * To change this template use File | Settings | File Templates.
 */
public class PacketHelper {
    public static void sendMP3StreamToChannelMsg(ITPlayer player, String channel, String uri, float volume) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        DataOutputStream data = new DataOutputStream(bytes);

        try {
            //id
            data.writeByte(1);
            //channel
            data.writeUTF(channel);
            //uri
            data.writeUTF(uri);
            //volume
            data.writeFloat(volume);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        player.getPlayer().sendPluginMessage(player.plugin, "Aud", bytes.toByteArray());
    }

    public static void sendMP3StopChannelPlayingMsg(ITPlayer player, String channel) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        DataOutputStream data = new DataOutputStream(bytes);

        try {
            //id
            data.writeByte(2);
            //channel
            data.writeUTF(channel);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        player.getPlayer().sendPluginMessage(player.plugin, "Aud", bytes.toByteArray());
    }

    public static void sendMP3SetChannelVolumeMsg(ITPlayer player, String channel, float volume) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        DataOutputStream data = new DataOutputStream(bytes);

        try {
            //id
            data.writeByte(3);
            //channel
            data.writeUTF(channel);
            //volume
            data.writeFloat(volume);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        player.getPlayer().sendPluginMessage(player.plugin, "Aud", bytes.toByteArray());
    }

    public static void sendMP3CacheMP3msg(ITPlayer player, String name, String url, long hash) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        DataOutputStream data = new DataOutputStream(bytes);

        try {
            //id
            data.writeByte(4);
            //channel
            data.writeUTF(name);
            //url
            data.writeUTF(url);
            //hash
            data.writeLong(hash);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        player.getPlayer().sendPluginMessage(player.plugin, "Aud", bytes.toByteArray());
    }

    public static void sendMP3PlayCachedMP3msg(ITPlayer player, String channel, String name, float volume) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        DataOutputStream data = new DataOutputStream(bytes);

        try {
            //id
            data.writeByte(5);
            //channel
            data.writeUTF(channel);
            //name
            data.writeUTF(name);
            //volume
            data.writeFloat(volume);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        player.getPlayer().sendPluginMessage(player.plugin, "Aud", bytes.toByteArray());
    }
}
