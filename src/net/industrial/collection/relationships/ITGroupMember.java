package net.industrial.collection.relationships;

import net.industrial.collection.player.ITPlayerData;
import net.industrial.collection.sql.ITMySQLQueue;
import net.industrial.collection.sql.ITMySQLQueueItem;
import net.industrial.collection.sql.ITSQL;

import java.sql.*;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Brodi
 * Date: 6/8/13
 * Time: 10:02 AM
 * To change this template use File | Settings | File Templates.
 */
public class ITGroupMember implements ITSQL {
    public final ITGroup group;
    public final ITPlayerData player;

    private int SQLid = -1;
    private Date memberSince;
    private short rank = 1;
    private boolean invalidated = true;

    public ITGroupMember(ITGroup group, ITPlayerData player) {
        this.group = group;
        this.player = player;
        memberSince = new Date();
    }

    public ITGroupMember(ITGroup group, ITPlayerData player, short rank) {
        this.group = group;
        this.player = player;
        this.rank = rank;
        memberSince = new Date();
    }

    public Date getMemberSince() {
        return memberSince;
    }

    public synchronized short getRank() {
        return rank;
    }

    public synchronized void setRank(short rank) {
        this.rank = rank;
        invalidate();
        group.plugin.addQueueItem(new ITMySQLQueueItem(this, ITMySQLQueue.QueueOperation.UPDATE));
    }

    @Override
    public synchronized void loadFromDatabase(Connection conn, ResultSet rs) {
        try {
            SQLid = rs.getInt("id");
            rank = rs.getShort("rank");
            memberSince = rs.getDate("memberSince");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        invalidated = false;
    }

    @Override
    public synchronized void deleteFromDatabase(Connection conn) {
        if(SQLid == -1)
            throw new RuntimeException("Tried to delete group member that had no SQLid");

        PreparedStatement stmt = null;
        try {
            stmt = conn.prepareStatement("DELETE FROM groupmembers WHERE id=?", Statement.NO_GENERATED_KEYS);
            stmt.setInt(1, SQLid);
            stmt.executeUpdate();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            //try to close the statement
            try {
                stmt.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public synchronized void updateDatabase(Connection conn) {
        PreparedStatement stmt = null;
        try {
            if(SQLid == -1) {
                stmt = conn.prepareStatement("INSERT INTO groupmembers(playerId, groupId, rank, memberSince) VALUES(?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
                stmt.setInt(1, player.getSQLid());
                stmt.setInt(2, group.getSQLid());
                stmt.setShort(3, rank);
                stmt.setTimestamp(4, new Timestamp((new Date()).getTime()));

                stmt.executeUpdate();

                ResultSet rs = stmt.getGeneratedKeys();
                rs.next();
                this.SQLid = rs.getInt(1);
                rs.close();

            } else if(invalidated) {
                stmt = conn.prepareStatement("UPDATE groupmembers SET rank=? WHERE id=?", Statement.NO_GENERATED_KEYS);
                stmt.setShort(1, rank);
                stmt.setInt(2, SQLid);
                stmt.executeUpdate();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            //try to close the statement
            try {
                if(stmt != null) {
                    stmt.close();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public synchronized void invalidate() {
        invalidated = true;
    }

    @Override
    public synchronized boolean isInvalidated() {
        return invalidated;
    }

    @Override
    public synchronized int getSQLid() {
        return SQLid;
    }
}
