package net.industrial.collection.relationships;

import net.industrial.collection.ITPlugin;
import net.industrial.collection.player.ITPlayerData;
import net.industrial.collection.sql.ITMySQLQueue;
import net.industrial.collection.sql.ITMySQLQueueItem;
import net.industrial.collection.sql.ITSQL;

import java.sql.*;
import java.util.Date;
import java.util.HashMap;
import java.util.Vector;

/**
 * Created with IntelliJ IDEA.
 * User: Brodi
 * Date: 6/8/13
 * Time: 10:02 AM
 * To change this template use File | Settings | File Templates.
 */
public class ITGroup implements ITSQL {
    public class ITGroupPermission implements ITSQL {
        public final ITGroup group;
        public final String name;
        private short rankNeeded;
        private int SQLid;
        private boolean invalidated = true;

        public ITGroupPermission(ITGroup group, String name, short rankNeeded) {
            this.group = group;
            this.name = name;
            this.rankNeeded = rankNeeded;
        }

        public synchronized short getRankNeeded() {
            return rankNeeded;
        }

        public synchronized void setRankNeeded(short rank) {
            this.rankNeeded = rank;
            this.group.plugin.addQueueItem(new ITMySQLQueueItem(this, ITMySQLQueue.QueueOperation.UPDATE));
        }

        @Override
        public void loadFromDatabase(Connection conn, ResultSet rs) {
            try {
                SQLid = rs.getInt("id");
                rankNeeded = rs.getShort("rank");

                invalidated = false;
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        @Override
        public synchronized void deleteFromDatabase(Connection conn) {
            if(SQLid == -1) {
                throw new RuntimeException("Tried to delete group permission");
            }

            PreparedStatement stmt = null;
            try {
                stmt = conn.prepareStatement("DELETE FROM grouppermissions WHERE id=?", Statement.NO_GENERATED_KEYS);
                stmt.executeUpdate();
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                //try to close the statement
                try {
                    stmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        @Override
        public synchronized void updateDatabase(Connection conn) {
            PreparedStatement stmt = null;
            try {
                if(SQLid == -1) {
                    stmt = conn.prepareStatement("INSERT INTO grouppermissions(groupId, name, rank) VALUES(?,?,?)",
                            Statement.RETURN_GENERATED_KEYS);
                    stmt.setInt(1, group.getSQLid());
                    stmt.setString(2, name);
                    stmt.setShort(3, rankNeeded);

                    stmt.executeUpdate();

                    ResultSet rs = stmt.getGeneratedKeys();
                    rs.next();
                    SQLid = rs.getInt(1);

                } else if(invalidated) {
                    stmt = conn.prepareStatement("UPDATE grouppermissions SET rank=? WHERE id=?",
                            Statement.NO_GENERATED_KEYS);
                    stmt.setShort(1, rankNeeded);
                    stmt.setInt(2, SQLid);
                    stmt.executeUpdate();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                //try to close the statement
                try {
                    stmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        @Override
        public void invalidate() {
            invalidated = true;
        }

        @Override
        public boolean isInvalidated() {
            return invalidated;
        }

        @Override
        public int getSQLid() {
            return SQLid;
        }
    }


    private  int SQLid = -1;
    public final ITPlugin plugin;
    public final String name;
    private HashMap<String, ITGroupPermission> permissions = new HashMap<String, ITGroupPermission>();
    private Vector<ITGroupMember> members = new Vector<ITGroupMember>();
    private boolean invalidated = true;
    private Date creationDate = new Date();

    public ITGroup(ITPlugin plugin, String name) {
        this.plugin = plugin;
        this.name = name;

        permissions.put("member.add", new ITGroupPermission(this, "member.add", (short) 1));
        permissions.put("member.set", new ITGroupPermission(this, "member.set", (short) 1));
        permissions.put("member.remove", new ITGroupPermission(this, "member.remove", (short) 32767));
        permissions.put("permissions.set", new ITGroupPermission(this, "permissions.set", (short) 32767));
        permissions.put("allies.edit", new ITGroupPermission(this, "allies.edit", (short) 1));
        permissions.put("allies.interact", new ITGroupPermission(this, "allies.interact", (short) 1));
    }

    public synchronized void setRankNeeded(String permission, short rank) {
        permissions.get(permission).setRankNeeded(rank);
    }

    public synchronized ITGroupPermission[] getPermissions() {
        return permissions.values().toArray(new ITGroupPermission[]{});
    }

    public synchronized short rankNeeded(String permission) {
        return permissions.get(permission).rankNeeded;
    }

    public synchronized boolean permissionExists(String permission) {
        return permissions.containsKey(permission);
    }

    public synchronized boolean inGroup(ITPlayerData data) {
        for(ITGroupMember member : members) {
            if(member.player == data) {
                return true;
            }
        }
        return false;
    }

    public synchronized ITGroupMember getMember(ITPlayerData data) {
        for(ITGroupMember member : members) {
            if(member.player == data) {
                return member;
            }
        }
        return null;
    }

    public synchronized void addToGroup(ITGroupMember membership) {
        if(membership.group != this) {
            throw new RuntimeException("Membership doesn't belong to group");
        }
        //return if already in group
        if(inGroup(membership.player)) {
            return;
        }

        members.add(membership);

        //check player
        if(!membership.player.inGroup(this)) {
            membership.player.addToGroup(membership);
        }

        //update
        plugin.addQueueItem(new ITMySQLQueueItem(membership, ITMySQLQueue.QueueOperation.UPDATE));
    }

    public synchronized void removeFromGroup(ITGroupMember membership) {
        if(membership.group != this) {
            throw new RuntimeException("Membership doesn't belong to group");
        }


        members.remove(membership);

        //check player
        if(membership.player.inGroup(this)) {
            membership.player.removeFromGroup(membership);
        }

        //delete
        plugin.addQueueItem(new ITMySQLQueueItem(membership, ITMySQLQueue.QueueOperation.DELETE));
    }

    public synchronized int getMemberCount() {
        return members.size();
    }

    public synchronized ITGroupMember[] getMembers() {
        return members.toArray(new ITGroupMember[]{});
    }

    public synchronized Date getCreationDate() {
        return creationDate;
    }


    @Override
    public void loadFromDatabase(Connection conn, ResultSet rs) {
        try {
            SQLid = rs.getInt("id");
            creationDate = rs.getDate("creationDate");

            //try to load all group members
            PreparedStatement stmt = null;
            try {
                stmt = conn.prepareStatement("SELECT * FROM groupmembers WHERE groupId=?");
                stmt.setInt(1, SQLid);
                ResultSet memberRS = stmt.executeQuery();

                while(memberRS.next()) {
                    ITGroupMember member = new ITGroupMember(this, plugin.getPlayerData(memberRS.getInt("playerId")));
                    member.loadFromDatabase(conn, memberRS);

                    //add to group and player
                    addToGroup(member);

                }
                memberRS.close();

            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                //try to close the statement
                try {
                    stmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }


            invalidated = false;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public synchronized void deleteFromDatabase(Connection conn) {
        if(SQLid == -1)
            throw new RuntimeException("Tried to delete group that has no SQLid");

        //delete all members
        for(ITGroupMember member : members) {
            member.deleteFromDatabase(conn);
        }

        //delete all permissions
        for(ITGroupPermission permission : permissions.values()) {
            permission.deleteFromDatabase(conn);
        }

        PreparedStatement stmt = null;
        try {
            stmt = conn.prepareStatement("DELETE FROM groups WHERE id=?", Statement.NO_GENERATED_KEYS);
            stmt.setInt(1, SQLid);
            stmt.executeUpdate();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            //try to close the statement
            try {
                stmt.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public synchronized void updateDatabase(Connection conn) {
        PreparedStatement stmt = null;
        try {
            
            if(SQLid == -1) {
                stmt = conn.prepareStatement("INSERT INTO groups(name,creationDate) VALUES(?,?)", Statement.RETURN_GENERATED_KEYS);

                stmt.setString(1, name);
                stmt.setTimestamp(2, new Timestamp(creationDate.getTime()));

                stmt.executeUpdate();

                ResultSet rs = stmt.getGeneratedKeys();
                rs.next();
                SQLid = rs.getInt(1);
            } else if(invalidated) {
                stmt = conn.prepareStatement("UPDATE groups SET name=? WHERE id=?", Statement.NO_GENERATED_KEYS);
                stmt.setString(1, name);
                stmt.setInt(2, SQLid);
                stmt.executeUpdate();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            //try to close the statement
            try {
                stmt.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        //update permissions
        for(ITGroupPermission permission : permissions.values()) {
            permission.updateDatabase(conn);
        }
    }

    @Override
    public synchronized void invalidate() {
        invalidated = true;
    }

    @Override
    public synchronized boolean isInvalidated() {
        return invalidated;
    }

    @Override
    public synchronized int getSQLid() {
        return SQLid;
    }

    public static HashMap<String, ITGroup> loadGroups(ITPlugin plugin, Connection conn) {
        HashMap<String, ITGroup> groups = new HashMap<>();

        PreparedStatement stmt = null;
        try {
            stmt = conn.prepareStatement("SELECT * FROM groups");

            ResultSet rs = stmt.executeQuery();

            while(rs.next()) {
                ITGroup group = new ITGroup(plugin, rs.getString("name"));
                group.loadFromDatabase(conn, rs);
                groups.put(group.name.toLowerCase(), group);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            //try to close the statement
            try {
                stmt.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        return groups;
    }
}
