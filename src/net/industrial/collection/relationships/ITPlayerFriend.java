package net.industrial.collection.relationships;

import net.industrial.collection.ITPlugin;
import net.industrial.collection.sql.ITMySQLQueue;
import net.industrial.collection.sql.ITMySQLQueueItem;
import net.industrial.collection.sql.ITSQL;
import net.industrial.collection.player.ITPlayerData;

import java.sql.*;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Brodi
 * Date: 6/8/13
 * Time: 10:02 AM
 * To change this template use File | Settings | File Templates.
 */
public class ITPlayerFriend implements ITSQL {
    public final ITPlugin plugin;
    public final ITPlayerData ownerPlayer;
    public final ITPlayerData friendPlayer;

    private int SQLid = -1;
    private short rank = 1;
    private Date friendsSince;
    private boolean invalidated = true;

    public ITPlayerFriend(ITPlugin plugin, ITPlayerData ownerPlayer, ITPlayerData friendPlayer) {
        this.plugin = plugin;
        this.ownerPlayer = ownerPlayer;
        this.friendPlayer = friendPlayer;
        this.friendsSince = new Date();
    }

    public ITPlayerFriend(ITPlugin plugin, ITPlayerData ownerPlayer, ITPlayerData friendPlayer, short rank) {
        this.plugin = plugin;
        this.ownerPlayer = ownerPlayer;
        this.friendPlayer = friendPlayer;
        this.rank = rank;
        this.friendsSince = new Date();
    }

    public ITPlayerFriend(ITPlugin plugin, ITPlayerData ownerPlayer, ITPlayerData friendPlayer, short rank, Date friendsSince, int SQLid) {
        this.plugin = plugin;
        this.ownerPlayer = ownerPlayer;
        this.friendPlayer = friendPlayer;
        this.rank = rank;
        this.friendsSince = friendsSince;
        this.SQLid = SQLid;
        invalidated = false;
    }

    public synchronized void setRank(short rank) {
        this.rank = rank;
        this.plugin.addQueueItem(new ITMySQLQueueItem(this, ITMySQLQueue.QueueOperation.UPDATE));
    }

    public synchronized short getRank() {
        return this.rank;
    }

    @Override
    public void loadFromDatabase(Connection connection, ResultSet rs) {
        throw new RuntimeException("Loading player directly not implemented");
    }

    @Override
    public void deleteFromDatabase(Connection conn) {
        if(SQLid == -1) {
            return;
        }

        PreparedStatement stmt = null;
        //try to remove the harmony of their friendship
        try {
            stmt = conn.prepareStatement("DELETE FROM friends WHERE id=?");
            stmt.setInt(1, SQLid);

            stmt.executeUpdate();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            //try to close the statement
            try {
                stmt.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        invalidated = false;
    }

    @Override
    public synchronized void updateDatabase(Connection conn) {
        //Insert
        if(SQLid == -1) {
            PreparedStatement stmt = null;
            try {
                stmt = conn.prepareStatement("INSERT INTO friends(ownerId,playerId,rank,friendsSince) VALUES(?,?,?,?);", Statement.RETURN_GENERATED_KEYS);

                stmt.setInt(1, ownerPlayer.getSQLid());
                stmt.setInt(2, friendPlayer.getSQLid());
                stmt.setShort(3, rank);
                stmt.setTimestamp(4, new Timestamp(friendsSince.getTime()));

                stmt.executeUpdate();

                ResultSet rs = stmt.getGeneratedKeys();

                rs.next();

                SQLid = rs.getInt(1);

                rs.close();

            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                //close the statement
                try {
                    stmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
        //update rank
        else if(invalidated) {
            PreparedStatement stmt = null;

            try {
                stmt = conn.prepareStatement("UPDATE friends SET rank=? WHERE id=?");

                stmt.setShort(1, rank);
                stmt.setInt(2, SQLid);

                stmt.executeUpdate();

            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                //close the statement
                try {
                    stmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        invalidated = false;
    }

    @Override
    public synchronized void invalidate() {
        invalidated = true;
    }

    @Override
    public synchronized boolean isInvalidated() {
        return invalidated;
    }

    @Override
    public int getSQLid() {
        return SQLid;
    }
}
