package net.industrial.collection;

import org.bukkit.inventory.ItemStack;

/**
 * Created with IntelliJ IDEA.
 * User: brodi
 * Date: 6/10/13
 * Time: 11:46 AM
 * To change this template use File | Settings | File Templates.
 */
public class InventoryHelper {
    public static int getAmount(ItemStack check, ItemStack[] contents) {
        int amount = 0;

        //loop through all the items and match items that share ids and data values and add up there amounts
        for(ItemStack item : contents) {
            if(item != null && check.isSimilar(item)) {
                amount += item.getAmount();
            }
        }

        return amount;
    }
}
