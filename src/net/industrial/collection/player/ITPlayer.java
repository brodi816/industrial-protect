package net.industrial.collection.player;

import net.industrial.collection.ITPlugin;
import net.industrial.collection.ITTimedAction;
import net.industrial.collection.permissions.ITPermissions;
import net.industrial.collection.sql.ITMySQLQueue;
import net.industrial.collection.sql.ITMySQLQueueItem;
import net.industrial.collection.world.ITChunk;
import net.industrial.collection.world.ITWorld;
import org.bukkit.Chunk;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.permissions.PermissionAttachment;

import java.util.Date;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;

public class ITPlayer {
	public final ITPlugin plugin;
	public final ITPlayerData data;
    public final PermissionAttachment permissions;

    public Chunk lastChunk;
	private Player player;
    private int joinTick;

    private boolean sendMp3CacheInfo = true;

    public ITTimedAction timedAction = null;
    public ItemStack protectionMaterial = null;
	
	//constructor always called when connected
	public ITPlayer(ITPlugin plugin, ITPlayerData data, Player player) {
		this.plugin = plugin;
		this.player = player;
		this.data = data;
        this.permissions = player.addAttachment(plugin);

        this.data.setLastSeen(new Date());

        joinTick = plugin.getServerTicks();

        //calculate permission
        plugin.permissions.addPermissionsToPlayer(this);
	}
	
	public void onQuit(PlayerQuitEvent event) {
        //save the player data
        this.plugin.addQueueItem(new ITMySQLQueueItem(data, ITMySQLQueue.QueueOperation.UPDATE));
        //remove this ITPlayer class from metadata
		ITPlayer.removePlayer(this);
	}

    public void onMove(PlayerMoveEvent event) {
        //see if the chunk the player is on is different
        if(lastChunk == null) {
            lastChunk = event.getPlayer().getLocation().getChunk();
            loadChunksAroundPlayer();
        } else if(lastChunk != event.getPlayer().getLocation().getChunk()) {
            lastChunk = event.getPlayer().getLocation().getChunk();
            loadChunksAroundPlayer();
        }
    }

    public void loadChunksAroundPlayer() {
        Chunk chunk = getPlayer().getLocation().getChunk();
        //load the chunks in a radius around the chunk the player is on
        for(int x = -plugin.config.chunkLoadDataRadius; x <= plugin.config.chunkLoadDataRadius; x++) {
            for(int z = -plugin.config.chunkLoadDataRadius; z <= plugin.config.chunkLoadDataRadius; z++) {
                ITChunk.loadChunk(ITWorld.getWorld(plugin, player.getWorld()),
                getPlayer().getWorld().getChunkAt(chunk.getX()+x,chunk.getZ()+z));
            }
        }
    }

    public void update() {
        //see if the timed action should end
        if(timedAction != null) {
            if(plugin.getServerTicks() >= timedAction.endTick) {
                timedAction.actionEnd();
                timedAction = null;
            }
        }

        //mp3 send
        if(sendMp3CacheInfo && plugin.getServerTicks() - joinTick > 100) {
            plugin.getMp3Cache().sendMp3CacheInfoToPlayer(this);
            sendMp3CacheInfo = false;
        }
    }

    public void setTimedAction(ITTimedAction action) {
        if(this.timedAction != null) {
            this.timedAction.actionEnd();
        }
        this.timedAction = action;
    }
	
	public void cacheLatestPlayer() {
		player = plugin.getServer().getPlayer(data.name);
	}
	
	public Player getPlayer() {
		if(player == null || player.isDead())
			cacheLatestPlayer();
		return player;
	}

    public static ITPlayer getPlayer(ITPlugin plugin, String name) {
        return ITPlayer.getPlayer(plugin, plugin.getServer().getPlayer(name));
    }

    public static ITPlayer getPlayer(ITPlugin plugin, Player player) {
        List<MetadataValue> itPlayer = player.getMetadata("ITPlayer");

        if(itPlayer.size() > 0) {
            return (ITPlayer) (itPlayer.get(0).value());
        } else {
            //fake a player join event
            plugin.logInfo("Logged " + player.getName() + " in, who had no ITPlayer data");
            plugin.getPlayerListener().onPlayerJoin(new PlayerJoinEvent(player, ""));
            return (ITPlayer) (player.getMetadata("ITPlayer").get(0).value());
        }
    }

    public static void setPlayer(ITPlayer itPlayer) {
        itPlayer.getPlayer().setMetadata("ITPlayer", new FixedMetadataValue(itPlayer.plugin, itPlayer));
    }

    public static void removePlayer(ITPlayer player) {
        player.getPlayer().removeMetadata("ITPlayer", player.plugin);
    }
}
