package net.industrial.collection.player;

import java.sql.*;
import java.util.*;
import java.util.Date;

import net.industrial.collection.ITPlugin;
import net.industrial.collection.relationships.ITGroup;
import net.industrial.collection.relationships.ITGroupMember;
import net.industrial.collection.sql.ITMySQLQueue;
import net.industrial.collection.sql.ITMySQLQueueItem;
import net.industrial.collection.sql.ITSQL;
import net.industrial.collection.relationships.ITPlayerFriend;


public class ITPlayerData implements ITSQL {
	public final ITPlugin plugin;
	public final String name;

    private Date joinDate, lastSeen;

    private Vector<ITPlayerFriend> friends = new Vector<ITPlayerFriend>();
    private Vector<ITGroupMember> groups = new Vector<ITGroupMember>();
    private short rank;
    private int SQLid = -1;
	private boolean invalidated;
	
	ITPlayerData(ITPlugin plugin, String name) {
		this.plugin = plugin;
		this.name = name;
	}

    public void loadFromDatabase(Connection conn, ResultSet rs) {
        try {
            this.SQLid = rs.getInt("id");
            this.joinDate = rs.getDate("joinDate");
            this.lastSeen = rs.getDate("lastSeen");
            this.rank = rs.getShort("rank");
        } catch(Exception e) {
            e.printStackTrace();
        }

        invalidated = false;
	}

    @Override
    public void deleteFromDatabase(Connection con) {
        throw new RuntimeException("Trying to delete player from DB, NOT ALLOWED");
    }

    @Override
    public synchronized void updateDatabase(Connection conn) {
	    if(SQLid == -1) {
            throw new RuntimeException("NO SQL ID for playerData(" + name + ")");
        }

        if(invalidated) {
            PreparedStatement stmt = null;

            try {
                stmt = conn.prepareStatement("UPDATE players SET 'rank'=?, 'lastSeen'=? WHERE 'id'=?", Statement.NO_GENERATED_KEYS);

                stmt.setShort(1, rank);
                stmt.setTimestamp(2, new Timestamp(lastSeen.getTime()));
                stmt.setInt(3, SQLid);

            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                //try to close the statement
                try {
                    stmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        //update friends
        for(ITPlayerFriend friendship : friends) {
            friendship.updateDatabase(conn);
        }

        invalidated = false;
    }

    @Override
    public synchronized void invalidate() {
        invalidated = true;
    }

    @Override
    public synchronized boolean isInvalidated() {
        return invalidated;
    }

    public int getSQLid() {
        return this.SQLid;
    }

    public void loadFriends(Connection conn) {
        if(SQLid == -1)
            return;

        PreparedStatement stmt = null;
        try {
            stmt = conn.prepareStatement("SELECT * FROM friends WHERE ownerId=?");
            stmt.setInt(1, SQLid);

            ResultSet rs = stmt.executeQuery();

            while(rs.next()) {
                friends.add(new ITPlayerFriend(plugin, this, plugin.getPlayerData(rs.getInt("playerId")), rs.getShort("rank"), rs.getDate("friendsSince"), rs.getInt("id")));
            }

            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //try and close the statement
            try {
                stmt.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public synchronized void addFriend(ITPlayerData player) {
        ITPlayerFriend friendship = new ITPlayerFriend(plugin, this, player);
        friends.add(friendship);
        this.plugin.addQueueItem(new ITMySQLQueueItem(friendship, ITMySQLQueue.QueueOperation.UPDATE));
    }

    public synchronized void addFriend(ITPlayerData player, short rank) {
        ITPlayerFriend friendship = new ITPlayerFriend(plugin, this, player, rank);
        friends.add(friendship);
        this.plugin.addQueueItem(new ITMySQLQueueItem(friendship, ITMySQLQueue.QueueOperation.UPDATE));
    }

    public synchronized ITPlayerFriend getFriend(ITPlayerData player) {
        for(ITPlayerFriend friendship : friends) {
            if(friendship.friendPlayer == player)
                return friendship;
        }
        return null;
    }

    public synchronized Object[] getFriends() {
        return friends.toArray();
    }

    public synchronized void removeFriend(ITPlayerData player) {
        ITPlayerFriend friendship = this.getFriend(player);
        if(friendship != null) {
            plugin.addQueueItem(new ITMySQLQueueItem(friendship, ITMySQLQueue.QueueOperation.DELETE));
            friends.remove(friendship);
        }
        invalidate();
    }

    public synchronized void setLastSeen(Date date) {
        this.lastSeen = date;
        invalidate();
        this.plugin.addQueueItem(new ITMySQLQueueItem(this, ITMySQLQueue.QueueOperation.UPDATE));
    }

    public synchronized short getRank() {
        return rank;
    }

    public synchronized void setRank(short rank) {
        this.rank = rank;
        invalidate();
        this.plugin.addQueueItem(new ITMySQLQueueItem(this, ITMySQLQueue.QueueOperation.UPDATE));
    }

    public void addToGroup(ITGroupMember membership) {
        if(membership.player != this) {
            throw new RuntimeException("Membership doesn't belong to player");
        }
        //return if already in group
        if(inGroup(membership.group)) {
            return;
        }

        groups.add(membership);

        //check group
        if(!membership.group.inGroup(this)) {
            membership.group.addToGroup(membership);
        }
    }

    public ITGroupMember getGroupMembership(ITGroup group) {
        for(ITGroupMember member : groups) {
            if(member.group == group) {
                return member;
            }
        }
        return null;
    }

    public ITGroupMember[] getGroupMemberships() {
        return groups.toArray(new ITGroupMember[]{});
    }

    public boolean inGroup(ITGroup group) {
        for(ITGroupMember member : groups) {
            if(member.group == group) {
                return true;
            }
        }
        return false;
    }

    public void removeFromGroup(ITGroupMember membership) {
        if(membership.player != this) {
            throw new RuntimeException("Tried to remove membership of wrong player");
        }

        groups.remove(membership);

        //check group
        if(membership.group.inGroup(this)) {
            membership.group.removeFromGroup(membership);
        }
    }

    public static boolean checkForPlayerData(ITPlugin plugin, String name) {
		PreparedStatement stmt = null;
		boolean found = false;
		
		try {
			stmt = plugin.getConnection().prepareStatement("SELECT 1 FROM players WHERE name=? LIMIT 1");
            stmt.setString(1, name);

            ResultSet rs = stmt.executeQuery();

            if(rs.next()) {
                found = true;
            }

            rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		
		//try to close the statement
		if(stmt != null) {
			try {
				stmt.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		return found;
	}

    public static ITPlayerData getPlayerFromMySql(ITPlugin plugin, String name) {
        ITPlayerData playerData = null;

        //see if the player exists
        if(checkForPlayerData(plugin, name)) {
            PreparedStatement stmt = null;

            //fetch the playerdata from the database
            try {
                stmt = plugin.getConnection().prepareStatement("SELECT * FROM players WHERE name = ? LIMIT 1");
                stmt.setString(1, name);
                ResultSet rs = stmt.executeQuery();

                if(rs.next()) {
                    playerData = new ITPlayerData(plugin, name);

                    playerData.loadFromDatabase(stmt.getConnection(), rs);
                }

                rs.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            finally {
                //Close the statement
                try {
                    stmt.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else {
            PreparedStatement stmt = null;

            //insert new player data since one doesn't exist
            try {
                stmt = plugin.getConnection().prepareStatement("INSERT INTO players(name, rank, joinDate, lastSeen) VALUES(?,?,?,?); ");

                stmt.setString(1, name);
                stmt.setShort(2, (short) 1);
                stmt.setTimestamp(3, new Timestamp(new Date().getTime()));
                stmt.setTimestamp(4, new Timestamp(new Date().getTime()));

                stmt.executeUpdate();

                playerData = getPlayerFromMySql(plugin, name);


            } catch(Exception e) {
                e.printStackTrace();
            } finally {
                //close the statement
                try {
                    stmt.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }


		return playerData;
	}

    public static HashMap<String, ITPlayerData> getAllPlayerData(ITPlugin plugin) {
        HashMap<String, ITPlayerData> allPlayerData = new HashMap<String, ITPlayerData>();


        //load all the player data from the db
        PreparedStatement stmt = null;

        try {
            stmt = plugin.getConnection().prepareStatement("SELECT * FROM players");

            ResultSet rs = stmt.executeQuery();

            //load each data
            while(rs.next()) {
                //Create the player data, load, and then add
                ITPlayerData data = new ITPlayerData(plugin, rs.getString("name"));
                data.loadFromDatabase(stmt.getConnection(), rs);
                allPlayerData.put(data.name.toLowerCase(), data);
            }
            rs.close();

        } catch(Exception ex) {
            ex.printStackTrace();
        } finally {
            //try to close the statement
            try {
                stmt.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        return allPlayerData;
    }


}
