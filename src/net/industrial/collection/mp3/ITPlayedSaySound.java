package net.industrial.collection.mp3;

import net.industrial.collection.player.ITPlayer;
import org.bukkit.entity.Player;

/**
 * Created with IntelliJ IDEA.
 * User: Brodi
 * Date: 7/12/13
 * Time: 5:11 PM
 * To change this template use File | Settings | File Templates.
 */
public class ITPlayedSaySound {
    public final Player player;
    public final ITSaySound sound;

    public ITPlayedSaySound(Player player, ITSaySound sound) {
        this.player = player;
        this.sound = sound;
    }
}
