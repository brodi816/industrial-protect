package net.industrial.collection.mp3;

import net.industrial.collection.ITPlugin;
import net.industrial.collection.packets.mp3.PacketHelper;
import net.industrial.collection.player.ITPlayer;
import net.industrial.collection.sql.ITMySQLQueue;
import net.industrial.collection.sql.ITMySQLQueueItem;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Brodi
 * Date: 7/12/13
 * Time: 12:53 AM
 * To change this template use File | Settings | File Templates.
 */
public class ITMp3Cache {
    public final ITPlugin plugin;
    public final File cacheFolder;
    private List<ITCachedMp3> cachedMP3s = new ArrayList<ITCachedMp3>();

    public ITMp3Cache(ITPlugin plugin) {
        this.plugin = plugin;
        this.cacheFolder = new File(plugin.getDataFolder(), "mp3cache");
        if(!this.cacheFolder.exists()) {
            plugin.logInfo("Creating folder: " + cacheFolder.getAbsolutePath());
            this.cacheFolder.mkdir();
        }
    }

    public boolean mp3Exists(String name) {
        for(ITCachedMp3 mp3 : cachedMP3s) {
            if(mp3.getName().equals(name)) {
                return true;
            }
        }
        return false;
    }

    public ITCachedMp3 getMp3(String name) {
        for(ITCachedMp3 mp3 : cachedMP3s) {
            if(mp3.getName().equals(name)) {
                return mp3;
            }
        }
        return null;
    }

    public void addMp3(ITCachedMp3 mp3) {
        cachedMP3s.add(mp3);
        plugin.addQueueItem(new ITMySQLQueueItem(mp3, ITMySQLQueue.QueueOperation.UPDATE));
    }

    public void addAndDownloadMp3(ITCachedMp3 mp3) {
        addMp3(mp3);
        downloadMp3(mp3);
    }

    public void downloadMp3(ITCachedMp3 mp3) {
        mp3.threadedDownload(new File(cacheFolder, mp3.getName()));
    }

    public void deleteMp3(ITCachedMp3 mp3) {
        cachedMP3s.remove(mp3);
        plugin.addQueueItem(new ITMySQLQueueItem(mp3, ITMySQLQueue.QueueOperation.DELETE));
        //delete the file
        (new File(cacheFolder, mp3.getName())).delete();
    }

    public void loadMp3s(Connection conn) {
        PreparedStatement stmt = null;

        try {
            stmt = conn.prepareStatement("SELECT * FROM cachedmp3s");
            ResultSet rs = stmt.executeQuery();

            while(rs.next()) {
                ITCachedMp3 mp3 = new ITCachedMp3(this);
                mp3.loadFromDatabase(conn, rs);
                cachedMP3s.add(mp3);
            }
            //close result
            rs.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            //try to close the statement
            try {
                stmt.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public void sendMp3CacheInfoToPlayer(ITPlayer player) {
        for(ITCachedMp3 mp3 : cachedMP3s) {
            PacketHelper.sendMP3CacheMP3msg(player, mp3.getName(), mp3.getUrl(), mp3.getHash());
        }
    }
}
