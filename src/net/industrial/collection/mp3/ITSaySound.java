package net.industrial.collection.mp3;

import net.industrial.collection.packets.mp3.PacketHelper;
import net.industrial.collection.player.ITPlayer;
import net.industrial.collection.sql.ITMySQLQueue;
import net.industrial.collection.sql.ITMySQLQueueItem;
import net.industrial.collection.sql.ITSQL;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Created with IntelliJ IDEA.
 * User: Brodi
 * Date: 7/12/13
 * Time: 3:47 PM
 * To change this template use File | Settings | File Templates.
 */
public class ITSaySound implements ITSQL {
    public final ITSaySounds sounds;
    private String name, file;
    private int SQLid = -1;
    private boolean invalidated = true;

    public ITSaySound(ITSaySounds sounds) {
        this.sounds = sounds;
    }

    public ITSaySound(ITSaySounds sounds, String saySoundTrigger, String file) {
        this.sounds = sounds;
        this.name = saySoundTrigger;
        this.file = file;
    }

    public synchronized String getName() {
        return name;
    }

    public synchronized String getFile() {
        return file;
    }

    public synchronized void setName(String name) {
        this.name = name;
        invalidate();
        sounds.plugin.addQueueItem(new ITMySQLQueueItem(this, ITMySQLQueue.QueueOperation.UPDATE));
    }

    public synchronized void setFile(String file) {
        this.file = file;
        invalidate();
        sounds.plugin.addQueueItem(new ITMySQLQueueItem(this, ITMySQLQueue.QueueOperation.UPDATE));
    }

    public synchronized void playSound(ITPlayer player) {
        try {
            (new URL(file)).toURI();
            PacketHelper.sendMP3StreamToChannelMsg(player, "saysound", file, 100);
        } catch (Exception ex) {
            PacketHelper.sendMP3PlayCachedMP3msg(player, "saysound", file, 100);
        }
    }

    @Override
    public synchronized void loadFromDatabase(Connection conn, ResultSet rs) {
        try {
            SQLid = rs.getInt("id");
            name = rs.getString("name");
            file = rs.getString("file");
            invalidated = false;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public synchronized void deleteFromDatabase(Connection conn) {
        if(SQLid == -1) {
            return;
        }

        PreparedStatement stmt = null;
        try {
            stmt = conn.prepareStatement("DELETE FROM saysounds WHERE id=?");
            stmt.setInt(1, SQLid);
            stmt.executeUpdate();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            //try to close the statement
            try {
                stmt.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public synchronized void updateDatabase(Connection conn) {
        PreparedStatement stmt = null;
        try {
            if(SQLid == -1) {
                stmt = conn.prepareStatement("INSERT INTO saysounds(name,file) VALUES(?,?)", Statement.RETURN_GENERATED_KEYS);
                stmt.setString(1, name);
                stmt.setString(2, file);
                stmt.executeUpdate();

                //get generated key
                ResultSet rs = stmt.getGeneratedKeys();
                rs.next();
                SQLid = rs.getInt(1);
            } else if(invalidated) {
                stmt = conn.prepareStatement("UPDATE saysounds SET name=?, file=? WHERE id=?", Statement.NO_GENERATED_KEYS);
                stmt.setString(1, name);
                stmt.setString(2, file);
                stmt.setInt(3, SQLid);
                stmt.executeUpdate();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            //try to close the statement
            try {
                if(stmt != null) {
                    stmt.close();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        invalidated = false;
    }

    @Override
    public synchronized void invalidate() {
        invalidated = true;
    }

    @Override
    public synchronized boolean isInvalidated() {
        return invalidated;
    }

    @Override
    public synchronized int getSQLid() {
        return SQLid;
    }
}
