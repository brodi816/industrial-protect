package net.industrial.collection.mp3;

import net.industrial.collection.ITPlugin;
import net.industrial.collection.packets.mp3.PacketHelper;
import net.industrial.collection.player.ITPlayer;
import net.industrial.collection.sql.ITMySQLQueue;
import net.industrial.collection.sql.ITMySQLQueueItem;
import net.industrial.collection.sql.ITSQL;

import java.io.*;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.Files;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.zip.CRC32;

/**
 * Created with IntelliJ IDEA.
 * User: Brodi
 * Date: 7/11/13
 * Time: 12:13 AM
 * To change this template use File | Settings | File Templates.
 */
public class ITCachedMp3 implements ITSQL {
    public final ITMp3Cache mp3Cache;
    private String name, url;
    private long hash;
    private int SQLid = -1;
    private boolean invalidated = true;

    public ITCachedMp3(ITMp3Cache mp3Cache) {
        this.mp3Cache = mp3Cache;
    }

    public ITCachedMp3(ITMp3Cache mp3Cache, String name, String url) {
        this.mp3Cache = mp3Cache;
        this.name = name;
        this.url = url;
    }


    public synchronized void sendToPlayer(ITPlayer player) {
        PacketHelper.sendMP3CacheMP3msg(player, name, url, hash);
    }

    public synchronized String getName() {
        return name;
    }

    public synchronized String getUrl() {
        return url;
    }

    public synchronized long getHash() {
        return hash;
    }

    public synchronized void setName(String name) {
        this.name = name;
        invalidate();
        mp3Cache.plugin.addQueueItem(new ITMySQLQueueItem(this, ITMySQLQueue.QueueOperation.UPDATE));
    }

    public synchronized void setUrl(String url) {
        this.url = url;
        invalidate();
        mp3Cache.plugin.addQueueItem(new ITMySQLQueueItem(this, ITMySQLQueue.QueueOperation.UPDATE));
    }

    public synchronized void setHash(Long hash) {
        this.hash = hash;
        invalidate();
        mp3Cache.plugin.addQueueItem(new ITMySQLQueueItem(this, ITMySQLQueue.QueueOperation.UPDATE));
    }

    @Override
    public synchronized void loadFromDatabase(Connection conn, ResultSet rs) {
        try {
            SQLid = rs.getInt("id");
            name = rs.getString("name");
            url = rs.getString("url");
            hash = rs.getLong("hash");
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        invalidated = false;
    }

    @Override
    public synchronized void deleteFromDatabase(Connection conn) {
        if(SQLid == -1)
            return;

        PreparedStatement stmt = null;

        try {
            stmt = conn.prepareStatement("DELETE FROM cachedmp3s WHERE id=?", Statement.NO_GENERATED_KEYS);
            stmt.setInt(1, getSQLid());
            stmt.executeUpdate();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            //try to close the statement
            try {
                stmt.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public synchronized void updateDatabase(Connection conn) {
        PreparedStatement stmt = null;

        try {
            //insert
            if(SQLid == -1) {
                stmt = conn.prepareStatement("INSERT INTO cachedmp3s(name,url,hash) VALUES(?,?,?)", Statement.RETURN_GENERATED_KEYS);

                stmt.setString(1, name);
                stmt.setString(2, url);
                stmt.setLong(3, hash);

                stmt.executeUpdate();

                //attempt to get the SQLid generated
                ResultSet rs = stmt.getGeneratedKeys();
                rs.next();
                SQLid = rs.getInt(1);
            } else if(invalidated) {
                stmt = conn.prepareStatement("UPDATE cachedmp3s SET name=?, URL=?, hash=? WHERE id=?");

                stmt.setString(1, name);
                stmt.setString(2, url);
                stmt.setLong(3, hash);
                stmt.setInt(4, SQLid);

                stmt.executeUpdate();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            //try to close the statement
            try {
                if(stmt != null) {
                    stmt.close();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        invalidated = false;
    }

    @Override
    public synchronized void invalidate() {
        invalidated = true;
    }

    @Override
    public synchronized boolean isInvalidated() {
        return invalidated;
    }

    @Override
    public synchronized int getSQLid() {
        return SQLid;
    }

    public void threadedDownload(File destFile) {
        (new downloadCachedMP3(this, destFile)).start();
    }

    public static long calculateHash(File file) {
        //attempt to get an input stream of the file
        FileInputStream is = null;
        try {
            is = new FileInputStream(file);
        } catch (FileNotFoundException ex) {
            return 0;
        }

        CRC32 crc32 = new CRC32();
        byte[] buffer = new byte[8192];
        int bytesRead;

        //read the bytes and pass them to crc32 generator
        try {
            while((bytesRead = is.read(buffer)) != -1) {
                crc32.update(buffer, 0, bytesRead);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }

        //try to close the open file input stream
        try {
            is.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return crc32.getValue();
    }

    public static void downloadFile(File destFile, String uri) {
        if(destFile.exists()) {
            if(!destFile.delete()) {
                //try to delete file another way
                try {
                    Files.delete(destFile.toPath());
                } catch (IOException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
        }

        try {
            if(!destFile.createNewFile()) {
                System.out.println("Unable to create file: " + destFile.getName());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        //try to form a url from the uri given
        URL url = null;
        try {
            url = new URL(uri);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //Open a channel from the url stream
        ReadableByteChannel rbc = null;
        try {
            rbc = Channels.newChannel(url.openStream());
        } catch (Exception e) {
            e.printStackTrace();
        }

        //Open the file to write
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(destFile);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //write
        try {
            fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //close
        try {
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class downloadCachedMP3 extends Thread {
        public final ITCachedMp3 mp3;
        public final File destFile;

        private downloadCachedMP3(ITCachedMp3 mp3, File destFile) {
            this.mp3 = mp3;
            this.destFile = destFile;
        }

        @Override
        public void run() {
            downloadFile(destFile, mp3.url);
            mp3.setHash(calculateHash(destFile));
            mp3.mp3Cache.plugin.logInfo("Finished mp3 download: " + mp3.name + " from: " + mp3.url);
        }
    }
}
