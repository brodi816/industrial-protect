package net.industrial.collection.mp3;

import net.industrial.collection.ITPlugin;
import net.industrial.collection.sql.ITMySQLQueue;
import net.industrial.collection.sql.ITMySQLQueueItem;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;

/**
 * Created with IntelliJ IDEA.
 * User: Brodi
 * Date: 7/12/13
 * Time: 5:02 PM
 * To change this template use File | Settings | File Templates.
 */
public class ITSaySounds {
    public final ITPlugin plugin;
    private Vector<ITSaySound> sounds = new Vector<>();

    public ITSaySounds(ITPlugin plugin) {
        this.plugin = plugin;
    }

    public ITSaySound getSound(String string) {
        String lowercase = string.toLowerCase();

        for(ITSaySound sound : sounds) {
            if(lowercase.contains(sound.getName())) {
                return sound;
            }
        }
        return null;
    }

    public ITSaySound[] getSounds() {
        return sounds.toArray(new ITSaySound[]{});
    }

    public void addSound(ITSaySound sound) {
        sounds.add(sound);
        plugin.addQueueItem(new ITMySQLQueueItem(sound, ITMySQLQueue.QueueOperation.UPDATE));
    }

    public void loadSaySounds(Connection conn) {
        PreparedStatement stmt = null;
        try {
            stmt = conn.prepareStatement("SELECT * FROM saysounds");
            ResultSet rs = stmt.executeQuery();
            //load all the sounds from the result
            while(rs.next()) {
                ITSaySound sound = new ITSaySound(this);
                sound.loadFromDatabase(conn, rs);
                sounds.add(sound);
            }
            rs.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            //try to close the statement
            try {
                stmt.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
}
